// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  pusher: {
    key: 'ABCDEFG@_',
    cluster: 'mt1',
  }
};
// PUSHER_APP_ID=secure_ssl_2022@
// PUSHER_APP_KEY=ABCDEFG@_
// PUSHER_APP_SECRET=HIJKLMNOP@_
// PUSHER_APP_CLUSTER=mt1

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
