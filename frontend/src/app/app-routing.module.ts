import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//===================================BACK-OFFICE===============================================
import { AboutComponent } from './views/Back-office/about/about.component';
import { DashboardComponent } from './views/Back-office/dashboard/dashboard.component';
import { LoginComponent } from './views/Back-office/login/login.component';
import { ProfileComponent } from './views/Back-office/profile/profile.component';
import { StockComponent } from './views/Back-office/stock/stock.component';
import { CreateComponent } from './views/Back-office/users/create/create.component';
import { EditComponent } from './views/Back-office/users/edit/edit.component';
import { IndexComponent } from './views/Back-office/users/index/index.component';
import { StatsComponent } from './views/Back-office/orders/stats/stats.component';
import { CashiersComponent } from './views/Back-office/cashiers/cashiers.component';
import { PreparatorsComponent } from './views/Back-office/preparators/preparators.component';
//=======================================================================================================
//================================website ecommerce components import====================================
//=======================================================================================================

import { ProductsingleComponent } from './views/website_ecommerce/productsingle/productsingle.component';
import { HomeComponent } from './views/website_ecommerce/home/home.component';
import { CartComponent } from './views/website_ecommerce/cart/cart.component';
import { CheckoutComponent } from './views/website_ecommerce/checkout/checkout.component';
import { ShopComponent } from './views/website_ecommerce/shop/shop.component';
import { DashboardEcommComponent } from './views/website_ecommerce/dashboard-ecomm/dashboard-ecomm.component';
import { OrdersComponent } from './views/website_ecommerce/orders/orders.component';
import { LoginEcommComponent } from './views/website_ecommerce/login-ecomm/login-ecomm.component';
import { SignupComponent } from './views/website_ecommerce/signup/signup.component';
import { ForgotPasswordComponent } from './views/website_ecommerce/forgot-password/forgot-password.component';
import { ProfileDetailsComponent } from './views/website_ecommerce/profile-details/profile-details.component';
import { EditAddressComponent } from './views/website_ecommerce/edit-address/edit-address.component';
import { AuthGuard } from './services/auth.guard';
import { ProductsComponent } from './views/Back-office/products/products.component';
import { CategoryProductComponent } from './views/Back-office/category-product/category-product.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';

import { BeforeLoginService } from './services/login_back_office/before-login.service';
import { AfterLoginService } from './services/login_back_office/after-login-service';
import { AboutUsComponent } from './views/website_ecommerce/about-us/about-us.component';
import { ContactUsComponent } from './views/website_ecommerce/contact-us/contact-us.component';
import { LoginAdminComponent } from './views/admin_pos/login-admin/login-admin.component';
import { DashboardAdminComponent } from './views/admin_pos/dashboard-admin/dashboard-admin.component';
import { ShopSettingsComponent } from './views/Back-office/shop-settings/shop-settings.component';
import { StatisticsComponent } from './views/Back-office/statistics/statistics.component';
import { PromotionsComponent } from './views/Back-office/promotions/promotions.component';
import { NotificationsComponent } from './views/Back-office/notifications/notifications.component';
import { ScreenNotifComponent } from './views/Back-office/screen-notif/screen-notif.component';
import { CustomerGuardService } from './services/login_back_office/customer-guard.service';

const routes: Routes = [
  { path:"", component:HomeComponent },
  { path:"product-single/:id", component:ProductsingleComponent },
  { path:"cart", component:CartComponent },
  { path: "checkout", component: CheckoutComponent},
  { path: "shop", component: ShopComponent},
  { path: 'login', component: LoginComponent,
    canActivate: [BeforeLoginService], },
  { path: 'dashboard_ecomm', component: DashboardEcommComponent,
  canActivate: [CustomerGuardService]},
  { path: 'order', component: OrdersComponent,
  canActivate: [CustomerGuardService]},
  { path: 'login_pos', component: LoginEcommComponent,
    canActivate: [BeforeLoginService] },
  { path: 'signup', component: SignupComponent,
  canActivate: [BeforeLoginService] },
  { path: 'forgot-password', component: ForgotPasswordComponent},
  { path: 'profile-details', component: ProfileDetailsComponent,
  canActivate: [CustomerGuardService]},
  { path: 'edit-address', component: EditAddressComponent,
  canActivate: [CustomerGuardService]},
  {path: 'aboutUs', component: AboutUsComponent },
  {path: 'ContactUs', component: ContactUsComponent },
  {path: 'shop/category/:id', component: ShopComponent},
  // { path: 'register', component: SignupComponent },
  { path: 'profile', component: ProfileComponent,
  canActivate: [AfterLoginService] },
  {path: 'dashboard', component: DashboardComponent,
    canActivate: [AfterLoginService]},
  {path: 'about', component: AboutComponent,
  canActivate: [AfterLoginService]},
  {path: 'shopSettings', component: ShopSettingsComponent,
  canActivate: [AfterLoginService]},
  {path: 'manageStock', component: StockComponent,
  canActivate: [AfterLoginService]},
  {path: 'users/index', component: IndexComponent,
  canActivate: [AfterLoginService]},
  {path: 'users/create', component: CreateComponent,
  canActivate: [AfterLoginService]},
  {path: 'users/edit/:idUser', component: EditComponent,
  canActivate: [AfterLoginService]},
  {path: 'orders/stats', component: StatsComponent,
  canActivate: [AfterLoginService]},
  {path: 'orders/cashiers_list', component: CashiersComponent,
  canActivate: [AfterLoginService]},
  {path: 'orders/prepa_list', component: PreparatorsComponent,
  canActivate: [AfterLoginService]},
  {path: 'products', component: ProductsComponent,
  canActivate: [AfterLoginService]},
  {path: 'categoryProduct', component: CategoryProductComponent,
  canActivate: [AfterLoginService]},
  {path: 'statistics', component: StatisticsComponent,
  canActivate: [AfterLoginService]},
  {path: 'coupons', component: PromotionsComponent,
  canActivate: [AfterLoginService]},
  {path: 'all-notifs', component: NotificationsComponent,
  canActivate: [AfterLoginService]},
  {path: 'screen-orders-notif', component: ScreenNotifComponent,
  canActivate: [AfterLoginService]},
  {path: 'lposAdmin', component: LoginAdminComponent,
  canActivate: [BeforeLoginService] },
  {path: 'dashAdmin', component: DashboardAdminComponent,
  canActivate: [AfterLoginService] },
  {path: '**', component: PageNotFoundComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AfterLoginService, BeforeLoginService]
})
export class AppRoutingModule {
}
