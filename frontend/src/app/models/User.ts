export interface User{
  id: number;
  username: string;
  email: string;
  firstname: string;
  lastname : string;
  avatar: string;
  address1: string;
  address2: string;
  type: string;
  phone_number: string;
  city: string;
  role: number;
  created_at: string;
  email_verified_at: string;
  updated_at: string;
}
