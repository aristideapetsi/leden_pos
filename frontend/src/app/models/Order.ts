import { CurrencyPipe } from "@angular/common";
import { OrderProduct } from "./OrderProduct";
import { User } from "./User";

export interface Order
{
  id: number;
  prod_qty: number;
  total_price: number ;
  status: string;
  customer_id: number;
  shipping_total:number;
  payment_channel: number;
  created_at: Date,
  updated_at: Date,
  order_products: OrderProduct[],
  firstname: string;
  lastname: string;
  address1: string;
  address2: string;
  city: string;
  notes: string;
  split_payment_infos: string;
  tracking_no: string;
  cooked_at: String;
  user : User;
}
