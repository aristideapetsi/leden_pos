import { Product } from "src/app/models/product";

export interface OrderProduct
{
  id: number;
  order_id : number;
  variation_id: number;
  customer_id: number;
  product_qty: number;
  product: Product
}
