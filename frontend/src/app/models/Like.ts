import { Product } from "./product";
import { User } from "./User";

export interface Like
{
  id: number;
  status: number;
  user: User;
  product: Product;
}
