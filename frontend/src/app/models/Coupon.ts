export interface Coupon
{
  id: number;
  code: string;
  type: string;
  value: string;
  cart_value: string;

}
