import { Product } from "./product";

export interface Cart
{
  id: number;
  // product_name: string;
  prod_qty: number;
  product_id: number;
  // price: number
  // feature_image : string,
  products: Product
}
