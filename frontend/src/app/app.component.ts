import { Component, OnDestroy, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { TokenService } from './Back-office/services/token.service';
import { AuthStateService } from './services/auth/auth-state.service';
import { AuthService } from './services/auth/auth.service';
import { User } from './models/User';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
// import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  // public isSignedIn: boolean =  false;
  // public UserProfile: User;
  onlineEvent: Observable<Event>;
  offlineEvent: Observable<Event>;

  subscriptions: Subscription[] = [];

  connectionStatusMessage: string = "";
  connectionStatus: string = "";

  constructor(
    private auth: AuthStateService,
    // public router: Router,
    // public token: TokenService,
    private authService: AuthService,
    private toastr: ToastrService
  ) {
    // this.authService.profileUser().subscribe((data: any) =>{
    //   this.UserProfile = data;
    // })
  }
  ngOnInit() {
        /**
    * Get the online/offline status from browser window
    */
         this.onlineEvent = fromEvent(window, 'online');
         this.offlineEvent = fromEvent(window, 'offline');

         this.subscriptions.push(this.onlineEvent.subscribe(e => {
           this.connectionStatusMessage = 'Back to online';
           this.connectionStatus = 'online';
           this.toastr.success('Connexion statut', 'Connection Internet Rétabli');
          //  console.log('Online...');
         }));

         this.subscriptions.push(this.offlineEvent.subscribe(e => {
           this.connectionStatusMessage = 'Connection lost! You are not connected to internet';
           this.connectionStatus = 'offline';
           this.toastr.error('Connexion statut', "Impossible de se connecter à Internet!");
          //  console.log('Offline...');
         }));

          // this.auth.authStatus.subscribe((val) => {
          //   this.isSignedIn = val;
          // });

  }
  ngOnDestroy(): void {
    /**
    * Unsubscribe all subscriptions to avoid memory leak
    */
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  // Signout
  // signOut() {
  //   this.auth.setAuthState(false);
  //   this.token.removeToken();
  //   this.router.navigate(['login']);
  // }
}
