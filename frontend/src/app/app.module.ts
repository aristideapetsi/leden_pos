import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { DataTablesModule } from 'angular-datatables';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {MatBadgeModule} from '@angular/material/badge';
import {NgxPrintModule} from 'ngx-print';

// Import NgxHideOnScrollModule
import { NgxHideOnScrollModule } from 'ngx-hide-on-scroll';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxMaskModule, IConfig } from 'ngx-mask'

import { AuthInterceptor } from './services/auth.interceptor';
//=============================================BACK OFFICE =====================================================================
import { ProfileComponent } from './views/Back-office/profile/profile.component';
import { LoginComponent } from './views/Back-office/login/login.component';
import { TopBarComponent } from './views/Back-office/shared/top-bar/top-bar.component';
import { LeftBarComponent } from './views/Back-office/shared/left-bar/left-bar.component';
import { DashboardComponent } from './views/Back-office/dashboard/dashboard.component';
import { AboutComponent } from './views/Back-office/about/about.component';
import { StockComponent } from './views/Back-office/stock/stock.component';
import { IndexComponent } from './views/Back-office/users/index/index.component';
import { CreateComponent } from './views/Back-office/users/create/create.component';
import { EditComponent } from './views/Back-office/users/edit/edit.component';
import { FooterComponent } from './views/Back-office/shared/footer/footer.component';
import { StatsComponent } from './views/Back-office/orders/stats/stats.component';
import { ProductListComponent } from './views/Back-office/stock/productList.component';
import { CashiersComponent } from './views/Back-office/cashiers/cashiers.component';
import { PreparatorsComponent } from './views/Back-office/preparators/preparators.component';
import { OrderCashierDatatableComponent } from './views/Back-office/cashiers/order-cashier-datatable/order-cashier-datatable.component';
import { OrderPreparatorDatatableComponent } from './views/Back-office/preparators/order-preparator-datatable/order-preparator-datatable.component';
import { ProductsComponent } from './views/Back-office/products/products.component';



//=============================================================================================================
//=================================FRONT OFFICE ====================================================================
//================================================================================================================
import { HeaderComponent } from './views/website_ecommerce/layouts/shared/header/header.component';
import { HomeComponent } from './views/website_ecommerce/home/home.component';
import { ProductsingleComponent } from './views/website_ecommerce/productsingle/productsingle.component';
import { CartComponent } from './views/website_ecommerce/cart/cart.component';
import { EditAddressComponent } from './views/website_ecommerce/edit-address/edit-address.component';
import { CheckoutComponent } from './views/website_ecommerce/checkout/checkout.component';
import { ShopComponent } from './views/website_ecommerce/shop/shop.component';
import { OrdersComponent } from './views/website_ecommerce/orders/orders.component';
import { LoginEcommComponent } from './views/website_ecommerce/login-ecomm/login-ecomm.component';
import { SignupComponent } from './views/website_ecommerce/signup/signup.component';
import { ForgotPasswordComponent } from './views/website_ecommerce/forgot-password/forgot-password.component';
import { ProfileDetailsComponent } from './views/website_ecommerce/profile-details/profile-details.component';
import { AddressComponent } from './views/website_ecommerce/address/address.component';
import { DashboardEcommComponent } from './views/website_ecommerce/dashboard-ecomm/dashboard-ecomm.component';
import { MenuComponent } from './views/website_ecommerce/layouts/shared/menu/menu.component';
import { FooterComponentEcom } from './views/website_ecommerce/layouts/shared/footer/footer.component';
import { ProductDetailModalComponent } from './views/website_ecommerce/shop/product-detail-modal/product-detail-modal.component';
import { ContactFormComponent } from './views/website_ecommerce/checkout/contact-form/contact-form.component';
import { CategoryProductComponent } from './views/Back-office/category-product/category-product.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { SkeletonCategoryComponent } from './components/skeleton-category/skeleton-category.component';
import { SkeletonProductComponent } from './components/skeleton-product/skeleton-product.component';
import { SkeletonProductPopularComponent } from './components/skeleton-product-popular/skeleton-product-popular.component';
import { SkeletonProductTableComponent } from './components/skeleton-product-table/skeleton-product-table.component';
import { AboutUsComponent } from './views/website_ecommerce/about-us/about-us.component';
import { ContactUsComponent } from './views/website_ecommerce/contact-us/contact-us.component';


//=============================================================================================================
//================================= BACK OFFICE ADMIN POS | POS MANAGER====================================================================
//================================================================================================================
import { LoginAdminComponent } from './views/admin_pos/login-admin/login-admin.component';
import { DashboardAdminComponent } from './views/admin_pos/dashboard-admin/dashboard-admin.component';
import { NgChartjsModule } from 'ng-chartjs';
import { ShopSettingsComponent } from './views/Back-office/shop-settings/shop-settings.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { StatisticsComponent } from './views/Back-office/statistics/statistics.component';
import { PromotionsComponent } from './views/Back-office/promotions/promotions.component';
import { NotificationsComponent } from './views/Back-office/notifications/notifications.component';
import { ScreenNotifComponent } from './views/Back-office/screen-notif/screen-notif.component';
// export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LoginComponent,
    TopBarComponent,
    LeftBarComponent,
    DashboardComponent,
    AboutComponent,
    StockComponent,
    IndexComponent,
    CreateComponent,
    EditComponent,
    FooterComponent,
    StatsComponent,
    ProductListComponent,
    CashiersComponent,
    PreparatorsComponent,
    OrderCashierDatatableComponent,
    OrderPreparatorDatatableComponent,
    HeaderComponent,
    HomeComponent,
    ProductsingleComponent,
    CartComponent,
    EditAddressComponent,
    CheckoutComponent,
    ShopComponent,
    OrdersComponent,
    LoginEcommComponent,
    SignupComponent,
    ForgotPasswordComponent,
    ProfileDetailsComponent,
    AddressComponent,
    DashboardEcommComponent,
    MenuComponent,
    FooterComponentEcom,
    ProductDetailModalComponent,
    ContactFormComponent,
    ProductsComponent,
    CategoryProductComponent,
    PageNotFoundComponent,
    SkeletonCategoryComponent,
    SkeletonProductComponent,
    SkeletonProductPopularComponent,
    SkeletonProductTableComponent,
    AboutUsComponent,
    ContactUsComponent,
    LoginAdminComponent,
    DashboardAdminComponent,
    ShopSettingsComponent,
    StatisticsComponent,
    PromotionsComponent,
    NotificationsComponent,
    ScreenNotifComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    SlickCarouselModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatSelectCountryModule.forRoot('fr'),
    NgxPaginationModule,
    NgxSkeletonLoaderModule.forRoot({ animation: 'pulse', loadingText: 'This item is actually loading...' }),
    MatBadgeModule,
    NgxHideOnScrollModule,
    NgxPrintModule,
    NgxSpinnerModule,
    NgChartjsModule,
    NgxMaskModule.forRoot(maskConfig),
    NgxSliderModule,

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'XOF' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
