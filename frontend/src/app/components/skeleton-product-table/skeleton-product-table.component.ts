import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-product-table',
  templateUrl: './skeleton-product-table.component.html',
  styleUrls: ['./skeleton-product-table.component.css']
})
export class SkeletonProductTableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
