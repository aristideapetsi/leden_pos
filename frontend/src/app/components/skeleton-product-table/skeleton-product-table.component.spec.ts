import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletonProductTableComponent } from './skeleton-product-table.component';

describe('SkeletonProductTableComponent', () => {
  let component: SkeletonProductTableComponent;
  let fixture: ComponentFixture<SkeletonProductTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkeletonProductTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletonProductTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
