import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletonCategoryComponent } from './skeleton-category.component';

describe('SkeletonCategoryComponent', () => {
  let component: SkeletonCategoryComponent;
  let fixture: ComponentFixture<SkeletonCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkeletonCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletonCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
