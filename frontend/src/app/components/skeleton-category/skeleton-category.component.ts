import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-category',
  templateUrl: './skeleton-category.component.html',
  styleUrls: ['./skeleton-category.component.css']
})
export class SkeletonCategoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
