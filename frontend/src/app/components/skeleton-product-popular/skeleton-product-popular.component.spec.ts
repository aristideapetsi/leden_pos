import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletonProductPopularComponent } from './skeleton-product-popular.component';

describe('SkeletonProductPopularComponent', () => {
  let component: SkeletonProductPopularComponent;
  let fixture: ComponentFixture<SkeletonProductPopularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkeletonProductPopularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletonProductPopularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
