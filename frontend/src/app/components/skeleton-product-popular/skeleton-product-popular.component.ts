import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-product-popular',
  templateUrl: './skeleton-product-popular.component.html',
  styleUrls: ['./skeleton-product-popular.component.css']
})
export class SkeletonProductPopularComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
