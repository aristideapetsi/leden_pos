export class ArrayHelper{

  constructor() {
  }
    /**
     * Search Item and remove element in array
     * @param array
     * @param item
     */
       searchItemAndRemoveInArray(array: number[], item: any){
        array.forEach((element,index)=>{
          if(element == item.id)
            array.splice(index,1);
       })
      }

      /**
       * DISTINCT ARRAY ELEMENTS
       * @param value
       * @param index
       * @param self
       * @returns
       */
      distinct = (value: any, index: any, self: any) =>{
        return self.indexOf(value) === index;
      }

      
}
