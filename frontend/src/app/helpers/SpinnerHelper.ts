import { Injectable } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";


@Injectable({
  providedIn: 'root'
 })


export class SpinnerHelper{

  constructor(
    private spinner: NgxSpinnerService
  ){}

  loader(){
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
  }

  get spin(){
    return this.spinner
  }

}




