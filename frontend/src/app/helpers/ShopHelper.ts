import { Injectable } from "@angular/core";
import { Cart } from "../models/cart";
import { CartService } from "../services/cart/cart.service";
import { ToastHelper } from "./ToastHelper";

@Injectable({
  providedIn: 'root'
 })

export class ShopHelper{

  cartItemList: Cart[] = [];
  addProdToCartIdProd: number;

  constructor(
    private toastHelper : ToastHelper,
    private cartService: CartService
  ) {
  }

      /**
     * Get cart info
     */
       getCartInfo(){
        this.cartService.cartItemList$.subscribe((response)=>{
          this.cartItemList = response;
        })

        this.cartService.getCartInfo().subscribe((response)=>{
           this.cartService.setCart(response.cart, response.subTotal);
        })
      }
    /**
   * Add product to cart
   */
     addToCart(id: number, quantity: any, stock_prod: number){
      // console.log(quantity.value)

      if(quantity.value == 0){
         this.toastHelper.toast_error('Ajout au panier', 'Ajouter au moin un produit!!!');
      }else if(quantity.value > 0){
         if(stock_prod >= quantity.value){
           this.cartService.addToCart(id, quantity.value).subscribe((response: any) => {
             // console.log(response)
             if(response.code == 200){
               // console.log('product added to cart');
               // this.cartItemList = response.cart;
               this.toastHelper.toast_success('Ajout au panier', response.message);
               this.getCartInfo();
             }else if(response.code == 204){
               this.toastHelper.toast_error('Ajout au panier', response.message);
             }else if(response.code == 401){
               this.toastHelper.toast_info('Ajout au panier', response.message);
             }

           })
         }else{
           this.toastHelper.toast_warning('Ajout au panier', "Stock Disponible : " + stock_prod );
         }

      }else{
       this.toastHelper.toast_error('Ajout au panier', 'Erreur Inattendu !!!');
      }

    }

}
