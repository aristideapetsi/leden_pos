import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { User } from "../models/User";
import { AuthStateService } from "../services/auth/auth-state.service";
import { AuthService } from "../services/auth/auth.service";
import { TokenService } from "../services/token/token.service";
import { ToastHelper } from "./ToastHelper";

@Injectable({
  providedIn: 'root'
 })

export class AccountHelper{
  UserProfile: User;

  constructor(
    private authState: AuthStateService,
    private tokenService: TokenService,
    private router: Router,
    private toast: ToastHelper,
    private authService: AuthService
  ) {
  }

  logout_pos() {
    this.authState.changeAuthStatus(false);
    this.tokenService.removeToken();
    this.router.navigate(['login_pos']);
    this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');
  }

  logout_admin() {
    this.authState.changeAuthStatus(false);
    this.tokenService.removeToken();
    this.router.navigate(['login']);
    this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');
  }

  getProfile(){
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
    })
    return this.UserProfile;
   }
}
