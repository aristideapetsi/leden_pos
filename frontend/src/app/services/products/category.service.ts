import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';
import { Category } from 'src/app/models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  env = new ConfigEnv()
  apiUrl = this.env.getApiUrl() + "categories/";
  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(
    private http: HttpClient,
  ) {

   }

   addCategory(formData: any): Observable<any>{
    const headers = new HttpHeaders();
    return this.http.post<any>(this.apiUrl  + "addCategoryProd" , formData, {
      headers: headers
    })
    .pipe(
      catchError(this.errorHandler)
    )
   }

      /**
     * Get product categories list
     */
       getProductCategories(): Observable<any>{
        return this.http.get<any>(this.apiUrl + "getAllCategories")
        .pipe(
          catchError(this.errorHandler)
        )
      }
      /**
       * Remove category
       * @param id
       * @returns
       */
      removeCategory(id: number):Observable<any>{
        return this.http.get<any>(this.apiUrl + "removeCategoryProd/" + id)
        .pipe(
          catchError(this.errorHandler)
        )
      }
      /**
       * Update Category info
       * @param formData
       * @returns
       */
      updateCategory(catId:number, formData : any): Observable<any>{
        const headers = new HttpHeaders();
        return this.http.post<any>(this.apiUrl + "updateCategoryProd/" + catId , formData, {
          headers: headers
        })
        .pipe(
          catchError(this.errorHandler)
        )
      }

      /**
     *
     * @param error
     * @returns
     */
    errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }

}
