import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigEnv } from 'src/app/configs/env';
import { Product } from 'src/app/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  env = new ConfigEnv()
  private apiUrl = this.env.getApiUrl() + "products/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    }),

  }

 paramProds = new HttpParams();

  constructor(
    private http: HttpClient
  ) {
   }

  /**
   *
   * @returns list users
   */
   getAll(): Observable<any>{
    return this.http.get<any>(this.apiUrl + "getAllProducts")
    .pipe(
      catchError(this.errorHandler),
      map((res:any) => {
        return res;
      })
    )
  }
  /**
   * Add product
   * @param formValues
   * @returns
   */
  addProduct(product: any): Observable<any>{
    const body = {
      product,
    }
    // const body = {
    //   product_name: product.product_name,
    //   description : product.description,
    //   feature_image: product.feature_image,
    //   stock_qty: product.stock_qty,
    //   category_id: product.category_id,
    //   price: product.price
    // }
    console.log(product)
    const headers = new HttpHeaders();
    return this.http.post<any>(this.apiUrl + "addProduct/" , product, {
      headers: headers
    })
    .pipe(
      catchError(this.errorHandler)
    )
  }
/**
 * Remove product
 * @param id
 * @returns
 */
  deleteProduct(id: number){
    return this.http.get<any>(this.apiUrl + "removeProduct/" + id )
    .pipe(
      catchError(this.errorHandler)
    )
  }
  /**
   * Update product info
   */
   updateProd(prodId: number, product: Product): Observable<any>{
    const body = {
      product
    }
      return this.http.post<any>(this.apiUrl + "updateProduct?id=" + prodId, body, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
   }
  /**
   *
   * @param prods
   * @returns
   */
  emptyStockProduct(prods : any): Observable<any>{
      console.log('stocks = ' + prods)

    return this.http.post<any>(this.apiUrl + "emptyProductStock?prods=" + prods , this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  updateStockProd(prodId:number, stock:number): Observable<any>{
    return this.http.post<any>(this.apiUrl + "updateStockProd/" + prodId + "/stock/" + stock, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  errorHandler(error: any){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
      errorMessage = error.error.message;
    else
      errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

  return throwError(errorMessage);
  }

}
