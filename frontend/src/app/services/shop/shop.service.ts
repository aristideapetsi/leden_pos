import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  env = new ConfigEnv();

  apiUrl = this.env.getApiUrl() + "shop/";
  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(
    private http: HttpClient
  ) { }

    /**
   *
   * @returns list users
   */
     getAll(): Observable<any>{
      return this.http.get<any>(this.apiUrl + "products")
      .pipe(
        catchError(this.errorHandler)
      )
    }
  /***
   * Get 5 products more purchase
   */
   getMostPopularProducts(): Observable<any>{
      return this.http.get<any>(this.apiUrl + "getMostPopularProducts")
      .pipe(
        catchError(this.errorHandler)
      )
    }
      /***
   * Get products more purchase
   */
       getPopularProducts(): Observable<any>{
        return this.http.get<any>(this.apiUrl + "getPopularProducts")
        .pipe(
          catchError(this.errorHandler)
        )
      }
    /**
     * get product from low price to high price
     */
    getProductsFromLowToCost():Observable<any>{
      return this.http.get<any>(this.apiUrl + "getProductsFromLowToCost")
      .pipe(
        catchError(this.errorHandler)
      )
    }
    /**
     * Get product filter by price range
     */
     getProductByPriceRange(minPrice: number, maxPrice: number):Observable<any>{
      const body = {
        minPrice,
        maxPrice
      }
      return this.http.post<any>(this.apiUrl + "filterProductByPriceRange", JSON.stringify(body), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
     }
    /**
     * get product from COST price to LOW price
     */
     getProductsFromCostToLow():Observable<any>{
      return this.http.get<any>(this.apiUrl + "getProductsFromCostToLow")
      .pipe(
        catchError(this.errorHandler)
      )
    }


      /**
       * Filter Product by category
       */

      FilterProductByCategory(categories: number[]): Observable<any>{
        const body = {
          categories
        }
        return this.http.post<any>(this.apiUrl + "filterProductByCategory", JSON.stringify(body), this.httpOptions)
        .pipe(
          catchError(this.errorHandler)
        )
      }

      /**
       *Search product
       * @param products
       * @returns
       */
      searchProduct(products: number[]): Observable<any>{
        const body = {
          products
        }
        return this.http.post<any>(this.apiUrl + "searchProducts", JSON.stringify(body), this.httpOptions)
        .pipe(
          catchError(this.errorHandler)
        )
      }

  /**
   * Get category info
   */
  getCategoryInfo(id:number){
    return this.http.get<any>(this.apiUrl + "getCategoryInfo/" + id)
    .pipe(
      catchError(this.errorHandler)
    );
  }
    /**
     *
     * @param error
     * @returns
     */
    errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }

}
