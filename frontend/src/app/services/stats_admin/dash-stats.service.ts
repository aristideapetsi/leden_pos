import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class DashStatsService {

  env = new ConfigEnv();

  private apiUrl = this.env.getApiUrl() + "statistics/";
  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(
    private http: HttpClient
    ) { }

  /**
   *
   * @returns list users
   */
   getAll(): Observable<any>{
    return this.http.get<any>(this.apiUrl + "dashboard")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error: any){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
      errorMessage = error.error.message;
    else
      errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

  return throwError(errorMessage);
  }

  /**
   * get orders by month
   * @returns
   */
  getOrdersStats():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/products_by_order")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getUsersStats():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/users_chart")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getProductsStats():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/products_chart")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getcompareOrdersStats():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/order_compare_chart")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * Get order yearly
   */
  getOrdersStatsByYear(year: any):Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/products_by_order?year=" + year)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  /**
   * get product by categories data
   * @returns
   */
  getProductsByCategoryStats():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/product_by_category")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   *  get yearly incomes
   * @returns
   */
  getIncomes():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/incomes")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getIncomesByYear(year: any):Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/monthly_incomes?year=" + year)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  /**
   * Get monthly income stats
   */
  getMonthlyIncomes():Observable<any>{
    return this.http.get<any>(this.env.getApiUrl() + "chart/monthly_incomes")
    .pipe(
      catchError(this.errorHandler)
    )
  }


  /**
   * get overview stats
   * @returns
   */
  getOverviewStat(): Observable<any>{
    return this.http.get<any>(this.apiUrl + 'getOverviewStats/' )
    .pipe(
      catchError(this.errorHandler)
    )

  }
}
