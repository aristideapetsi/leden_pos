import { TestBed } from '@angular/core/testing';

import { DashStatsService } from './dash-stats.service';

describe('DashStatsService', () => {
  let service: DashStatsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DashStatsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
