import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class OrderNotificationService {

  env = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "notifications/";

  public notif$  = new BehaviorSubject<any>(0);
  notifObs$ = this.notif$.asObservable();

  constructor(
    private httpClient: HttpClient
  ) { }




  /**
   * get orders notifs
   * @returns
   */
   getUnreadOrdersNotifications(): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "getOrderUnreadNotifications")
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * Get all notifications
   * @returns
   */
  getAllOrderNotifications(): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "getAllOrderNotifications")
    .pipe(
      catchError(this.errorHandler)
    );
  }

   /**
   * Get orders update notifications
   * @returns
   */
    getOrderUpdateNotifications(): Observable<any>{
      return this.httpClient.get<any>(this.apiUrl + "getOrdersUpdateNotifications")
      .pipe(
        catchError(this.errorHandler)
      );
    }

  /**
   * mark notif as read
   * @returns
   */
   markNotifAsRead(notif_id: any): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "markNotifAsRead/" + notif_id)
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * get unread notif by customer
   */
  getCustomerUnreadNotif(customer_id:any): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "getCustomerUnreadNotif/" + customer_id)
    .pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * get all notifs for a customer
   */
   getAllNotifForCustomer(customer_id:any): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "getAllNotifForCustomer/" + customer_id)
    .pipe(
      catchError(this.errorHandler)
    );
  }


    /**
   *
   * @param error
   * @returns
   */

     errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }


}
