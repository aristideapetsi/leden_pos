import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  env : ConfigEnv = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "users/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * Update Customer address
   * @param formData
   * @returns
   */
  updateAddressCustomer(formData : any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.apiUrl + "updateAddressCustomer", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );;
  }

  /**
   * Register customer
   * @param user
   * @returns
   */
  signup(user: any){
    return this.httpClient.post<any>(this.env.getApiUrl() + "signup", JSON.stringify(user), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * Update customer info
   * @param formData
   * @returns
   */
  updateAccountInfo(formData: any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.apiUrl + "updateAccountInfo", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );;
  }

  /**
   * Update profile image
   * @param formData
   * @returns
   */
  updateProfileAvatar(formData: any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.apiUrl + "updateProfileAvatar", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );;
  }
  /**
   * update Password | Admin
   * @param formData
   * @returns
   */
  updateAdminPassword(formData: any): Observable<any>{
    const headers = new HttpHeaders();
    return this.httpClient.post<any>(this.apiUrl + "updateAdminPass", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );
  }

  /**
   * Update Admin info
   * @param formData
   * @returns
   */
  updateAdminAccountInfo(formData: any): Observable<any>{
    const headers = new HttpHeaders();
    return this.httpClient.post<any>(this.apiUrl + "updateAdminAccountInfo", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );;
  }

  /**
   * Reset forget password by mail
   * @param formData
   * @returns
   */
  resetPassword(formData: any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.env.getApiUrl() + "sendPasswordResetLink", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );
  }
      /**
     *
     * @param error
     * @returns
     */
       errorHandler(error: any){
        let errorMessage = '';
        if(error.error instanceof ErrorEvent)
          errorMessage = error.error.message;
        else
          errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

      return throwError(errorMessage);
      }
}
