import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  env = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "orders/";
  constructor(
    private httpClient: HttpClient
  ) { }

    /**
   *
   * @returns list orders | super admin
   */
     getAllOrders(): Observable<any>{
      return this.httpClient.get<any>(this.apiUrl + "getAllOrders")
      .pipe(
        catchError(this.errorHandler)
      )
    }

  /**
   * get orders by customer
   * @returns
   */
  getOrderByCustomer(): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "orderByCustomer")
    .pipe(
      catchError(this.errorHandler)
    );
  }

  getOrderDetail(orderid: number): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + 'getorderDetail/' + orderid )
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   *
   * @param error
   * @returns
   */

  errorHandler(error: any){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
      errorMessage = error.error.message;
    else
      errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

  return throwError(errorMessage);
  }
}
