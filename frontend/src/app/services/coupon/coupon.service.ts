import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class CouponService {

  env : ConfigEnv = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "coupons/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(
    private httpClient: HttpClient
  ) { }



  /**
   *Add coupon
   * @param coupon
   * @returns
   */
  addCoupon(coupon: any): Observable<any>{
    const headers = new HttpHeaders();
      return this.httpClient.post<any>(this.apiUrl + "addCoupon", coupon, {
        headers: headers
      })
      .pipe(
        catchError(this.errorHandler)
      )
  }
  /**
   * get all coupons
   * @returns
   */
  getAllCoupons(): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "getAllCoupons")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * remove coupon
   * @param idCoupon
   * @returns
   */
  deleteCoupon(idCoupon: number): Observable<any>{
    return this.httpClient.get<any>(this.apiUrl + "deleteCoupon/" + idCoupon)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * update coupon
   * @param idCoupon
   * @param coupon
   * @returns
   */
  updateCoupon(idCoupon: number, coupon: any): Observable<any>{
    const body = {
      coupon
    }
      return this.httpClient.post<any>(this.apiUrl + "updateCoupon?id=" + idCoupon, body, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
  }

    /**
     *
     * @param error
     * @returns
     */
       errorHandler(error: any){
        let errorMessage = '';
        if(error.error instanceof ErrorEvent)
          errorMessage = error.error.message;
        else
          errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

      return throwError(errorMessage);
      }

}
