import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigEnv } from 'src/app/configs/env';
import { Cart } from 'src/app/models/cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  env = new ConfigEnv()
  apiUrl = this.env.getApiUrl() + "cart/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  // public products = new BehaviorSubject<Cart[]>([]);
  public totalPrice$ = new BehaviorSubject<any>(0);
  private cartItem$  = new BehaviorSubject<any>({});
  cartItemList$ = this.cartItem$.asObservable();
  constructor(
    private http: HttpClient,
  ) { }


  setCart(items: any, totalPrice: any){
     this.cartItem$.next(items);
     this.totalPrice$.next(totalPrice);
  }

  /**
   *add product to cart
    */
    addToCart(prodId: number, prod_qty: number): Observable<any>{
    // this.productList.next(this.cartItemList);
    // this.getTotalPrice();
    return this.http.post<any>(this.apiUrl + "addToCart/" + prodId + "/prod_qty/" + prod_qty, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   * get cart items
   * @returns
   */
  getCartInfo(): Observable<any>{
    return this.http.get<any>(this.apiUrl + "getCartContent")
      .pipe(
        catchError(this.errorHandler),
        map((res:any) => {
          this.cartItem$.next(res.cart)
          // console.log( this.cartItemList)
          return res;
        })
      );
  }

  /**
   * remove item in cart
   */

  removeItemToCart(prod_id: number): Observable<any>{
    const body = {
      prod_id
    }
    return this.http.post<any>(this.apiUrl + "removeItemToCart", JSON.stringify(body), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    );
  }

    /**
     *
     * @param error
     * @returns
     */
       errorHandler(error: any){
        let errorMessage = '';
        if(error.error instanceof ErrorEvent)
          errorMessage = error.error.message;
        else
          errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

      return throwError(errorMessage);
      }


}
