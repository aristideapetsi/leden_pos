import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  pusher: any = "";

  constructor() {
    this.pusher = new this.pusher("",{
      cluster : "eu",
      auth:{
        params : {foo: "bar"},
        headers: {bar : "boo"}
      }
    })
  }

  listenChannel(names:any){
    for(let i = 0; i < names.length; i++){
      //subscribe schannels
      this.pusher.subscribe("private-" + names[i]);

      this.pusher.allChannels().forEach( (channel: any)=> {
        console.log("Subscribe: " + channel.name )
      })
    }
  }

}
