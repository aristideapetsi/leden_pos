import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class LikeService {
  env = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "shop/";
  constructor(
    private http: HttpClient
  ) { }

  getLikedProds(): Observable<any>{
    return this.http.get(this.apiUrl + "likedProducts")
    .pipe(
      catchError(this.errorHandler)
    );
  }
    /**
     *Like product
     * @param idProd
     * @returns
     */
  likeProduct(idProd: number): Observable<any>{
    return this.http.get(this.apiUrl + "likeProduct/" + idProd)
    .pipe(
      catchError(this.errorHandler)
    );
  }

    /**
     *
     * @param error
     * @returns
     */
     errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }
}
