import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';
import { Order } from 'src/app/models/Order';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  env: ConfigEnv = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "checkout/";
  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    }),

  }
  constructor(
    private http: HttpClient
  ) { }

    placeOrder(order: any): Observable<any>{
      const headers = new HttpHeaders();
      
      return this.http.post<any>(this.apiUrl + "place-order", order , {
        headers: headers
      })
      .pipe(
        catchError(this.errorHandler)
      )
    }

    errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;
  
    return throwError(errorMessage);
    }
}
