import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PreparatorService {

  apiUrl = "http://localhost:8000/api/orders/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(
    private http: HttpClient
  ) { }

    /**
   *
   * @returns list users
   */
     getAll(): Observable<any>{
      return this.http.get<any>(this.apiUrl + "getOrdersPrepa")
      .pipe(
        catchError(this.errorHandler)
      )
    }

    getAllOrdersDataForPrepa():Observable<any>{
      return this.http.get<any>(this.apiUrl + "getOrdersPrepaData")
      .pipe(
        catchError(this.errorHandler)
      )
    }

    /**
     *
     * @param order_id
     */
    validateOrderPrepare(order_id: number): Observable<any> {
      console.log('url prepa: ' + this.apiUrl + "validateOrderByPrepa/" + order_id)
      return this.http.post<any>(this.apiUrl + "validateOrderByPrepa/" + order_id, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }
}
