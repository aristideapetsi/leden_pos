import { TestBed } from '@angular/core/testing';

import { PreparatorService } from './preparator.service';

describe('PreparatorService', () => {
  let service: PreparatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreparatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
