import { TestBed } from '@angular/core/testing';

import { BeforeLoginService } from './before-login.service';

describe('BeforeLoginServiceService', () => {
  let service: BeforeLoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BeforeLoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
