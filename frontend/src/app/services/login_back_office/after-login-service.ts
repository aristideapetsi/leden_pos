import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/User';
import { AuthService } from '../auth/auth.service';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService implements CanActivate{

  UserProfile! : User;
  role : number ;

  constructor(
		private tokenService: TokenService,
		private router: Router,
    private authService: AuthService,
    ) {


     }

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

      // provides the route configuration options.
      const { routeConfig } = route;

      // provides the path of the route.
      const { path } = routeConfig as Route;


          console.log(this.role)
      if(this.tokenService.isLoggedIn() ){
        this.authService.profileUser().subscribe((response)=>{
          this.UserProfile = response
          console.log(response.role)

          if ((path?.includes('dashboard') || path?.includes('users/index') ||  path?.includes('orders/stats') || path?.includes('coupons') || path?.includes('statistics')
            || path?.includes('categoryProduct') || path?.includes('products') || path?.includes('shopSettings')) && (response.role == 0 || response.role == 1)) {
                return true;
            }else if((path?.includes('manageStock') ||  path?.includes('orders/prepa_list')) && (response.role == 3 || response.role == 0 || response.role == 1)){
              return true
            }else if( path?.includes('orders/cashiers_list') && (response.role == 2 || response.role == 0 || response.role == 1 ) ){
              return true;
            }else if(( path?.includes('about') || path?.includes('profile') || path?.includes('all-notifs')  ) && (response.role == 3 || response.role == 0 || response.role == 1 || response.role == 2)  ){
              return true;
            }else if(path?.includes('screen-orders-notif') && response.role == 5){
              return true;
            }
            else{
              return this.router.navigate(['**']);
            }
        })

      }else{
          return false;
      }

        // if ((path?.includes('dashboard') || path?.includes('dashboard_ecomm')) && (role === 0 || role === 4)) {
        //   // if a logged in user goes to Guest or Home, navigate to their respective dashboard.

        //       this.router.navigateByUrl(role === 0 ? '/dashboard' : '/dashboard_ecomm');
        //       return false;
        //   }

      if(!this.tokenService.isLoggedIn()){
        return this.router.navigate(['/login_pos']);
      }

    // console.log(this.tokenService.isLoggedIn());
    // this.router.navigate(['/login_pos'], { queryParams: { returnUrl: state.url } });
		return this.tokenService.isLoggedIn();
	}
}
