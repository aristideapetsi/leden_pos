import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/User';
import { AuthService } from '../auth/auth.service';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerGuardService {

  UserProfile! : User;

  constructor(
		private tokenService: TokenService,
		private router: Router,
    private authService: AuthService,
    ) {
     }
     canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

        if(!this.tokenService.isLoggedIn()){
          return this.router.navigate(['/login_pos']);
        }

      // console.log(this.tokenService.isLoggedIn());
      // this.router.navigate(['/login_pos'], { queryParams: { returnUrl: state.url } });
      return this.tokenService.isLoggedIn();
    }

}
