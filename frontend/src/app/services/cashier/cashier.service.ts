import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CashierService {

  apiUrl = "http://localhost:8000/api/orders/";
  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(
    private http: HttpClient
  ) { }

    /**
   *
   * @returns list users
   */
     getAll(): Observable<any>{
      return this.http.get<any>(this.apiUrl + "getOrdersCashier")
      .pipe(
        catchError(this.errorHandler)
      )
    }

    getAllOrdersDataForCashier(): Observable<any>{
      return this.http.get<any>(this.apiUrl + "getOrdersCashierData")
      .pipe(
        catchError(this.errorHandler)
      )
    }

    /**
     * CancelOrder
     * @param orderId
     * @returns
     */
    cancelOrder(orderId: number){
      return this.http.get<any>(this.apiUrl + "cancelOrder/" + orderId)
      .pipe(
        catchError(this.errorHandler)
      )

    }
    /**
     *
     * @param order_id
     */
      cashMoneyByCashier(order_id: number): Observable<any> {
          return this.http.post<any>(this.apiUrl + "cashMoneyByCashier/" + order_id, this.httpOptions)
          .pipe(
            catchError(this.errorHandler)
          )
      }

    /**
     *
     * @param order_id
     */
     deliveryOrderByCashier(order_id: number): Observable<any> {
          return this.http.post<any>(this.apiUrl + "deliveryOrderByCashier/" + order_id, this.httpOptions)
          .pipe(
            catchError(this.errorHandler)
          )
      }

    errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }
}
