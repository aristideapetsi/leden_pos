import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css',
  '../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../assets_leden/css/app.min.css'
]
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
