import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { User } from 'src/app/models/User';
import { AccountService } from 'src/app/services/account/account.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {
  UserProfile! : User;
  user: User ;
  UserForm = this.formBuilder.group({
    city: new FormControl(),
    address1:new FormControl(),
    address2: new FormControl(),
    phone_number: new FormControl(),
  });

  constructor(
    private authService: AuthService,
    private accountService: AccountService,
    private toast : ToastHelper,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.authService.profileUser().subscribe((data: any) =>{
        this.UserProfile = data;
    })


  }

  updateUserAddress(){

    const formData = new FormData();
    var city =  this.UserForm.get('city')?.value;
    var address1 = this.UserForm.get('address1')?.value;
    var address2 = this.UserForm.get('address2')?.value
    var phone_number = this.UserForm.get('phone_number')?.value;

    if(city == null){
      city = this.UserProfile.city;
    }
    if(address1 == null){
      address1 = this.UserProfile.address1;
    }
    if(address2 == null){
      address2 = this.UserProfile.address2;
    }

    if(phone_number == null){
      phone_number = this.UserProfile.phone_number;
    }
    formData.append('city', city);
    formData.append('address1', address1);
    formData.append('address2', address2);
    formData.append('phone_number', phone_number );
    // console.log(formData);
    this.accountService.updateAddressCustomer(formData).subscribe((Response) => {
      if(Response.code == 401){
        Swal.fire({
          title: 'Vous n\'etes pas connecté!!!',
          text: "Connectez vous pour mettre à jour vos infos!!!",
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'OK, Se connecter!',
          cancelButtonText: 'ANNULER'
        }).then((result) => {
          if (result.isConfirmed) {
            //ReDIRECTION VERS LA PAGE DE CONNEXION
            this.router.navigate(['/loin_pos']);
          }
        })
      }else if(Response.code == 200){
          this.toast.toast_success('Mise à jour adresse', 'Votre adresse a été mise à jour avec succcès!!!');
      }else{
        this.toast.toast_error('Mise à jour adresse', 'Un problème inattendu est survenu!!!');
        return;
      }
    })
  }
}
