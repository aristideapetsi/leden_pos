import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountHelper } from 'src/app/helpers/AccountHelper';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-dashboard-ecomm',
  templateUrl: './dashboard-ecomm.component.html',
  styleUrls: ['./dashboard-ecomm.component.css']
})
export class DashboardEcommComponent implements OnInit {
  UserProfile! : User;
  constructor(
    private authService: AuthService,
    private accountHelper: AccountHelper
  ) { }

  ngOnInit(): void {
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
  })
  }

  logout() {
    this.accountHelper.logout_pos();
  }
}
