import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEcommComponent } from './dashboard-ecomm.component';

describe('DashboardEcommComponent', () => {
  let component: DashboardEcommComponent;
  let fixture: ComponentFixture<DashboardEcommComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardEcommComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEcommComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
