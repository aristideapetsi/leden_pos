import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/models/User';
import { AccountService } from 'src/app/services/account/account.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css']
})
export class ProfileDetailsComponent implements OnInit {

  UserProfile! : User;
  UserForm : FormGroup = this.formBuilder.group({
    firstname: new FormControl(),
    lastname: new FormControl(),
    username: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    verification_password: new FormControl(),
  });
  loading: boolean = false;
  errors:any = null;
  submitted: boolean = false;

  constructor(
    private authService:  AuthService,
    private accountService: AccountService,
    private toast: ToastrService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
  })
  }

  get f(){
    return this.UserForm.controls;
  }

  updateAccountInfo(){
    this.loading = true;
    this.errors = false;
    this.submitted = true;

    const formData = new FormData();

    var firstname = this.UserForm.get('firstname')?.value;
    var lastname = this.UserForm.get('lastname')?.value;
    var username = this.UserForm.get('username')?.value;
    var email = this.UserForm.get('email')?.value;
    var password = this.UserForm.get('password')?.value;
    var verification_password = this.UserForm.get('verification_password')?.value;
    console.log("first,ame: " + firstname)
    if(firstname == null){
      firstname = this.UserProfile.firstname;
    }
    if(lastname == null){
      lastname = this.UserProfile.lastname;
    }
    if(username == null){
      username = this.UserProfile.username;
    }

    if(email == null){
      email = this.UserProfile.email;
    }
    if(password == null){
      password = '';
    }
    formData.append('firstname', firstname);
    formData.append('lastname',  lastname);
    formData.append('username', username);
    formData.append('email',  email);
    formData.append('password', password);
    formData.append('verification_password', verification_password);

    this.accountService.updateAccountInfo(formData).subscribe((response) => {
      this.loading = false;
      this.errors = response.message;
      if(response.code == 200){
        this.toast.success('Mise à jour compte',response.message);
      }else if(response.code == 401){
        this.toast.info('Mise à jour compte',response.message);
      }else{
        this.toast.error('Mise à jour compte', 'Erreur Inattendue !!!');
      }
    })
  }
}
