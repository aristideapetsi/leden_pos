import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Cart } from 'src/app/models/cart';
import { CartService } from 'src/app/services/cart/cart.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  cartItemList : Cart[];
  totalPrice: number;
  totalItems : number = 0;

  formContact: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    subject: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required),
  })

  constructor(
    private cartService : CartService,
  ) { }

  ngOnInit(): void {
    this.getCartInfo();
  }
  onSubmit(){
    const formData = new FormData();

    formData.append('name', this.formContact.get('name')?.value);
    formData.append('email', this.formContact.get('email')?.value);
    formData.append('subject', this.formContact.get('subject')?.value);
    formData.append('message', this.formContact.get('message')?.value);


  }


  getCartInfo(){

    this.cartService.cartItemList$.subscribe((response)=>{
      this.cartItemList = response;
      this.totalItems = response.length;
      // console.log(this.cartItemList)
   })
   this.cartService.totalPrice$.subscribe((totalPrice)=>{
      this.totalPrice = totalPrice;
      // console.log(this.totalPrice )
   })
   this.cartService.getCartInfo().subscribe((response)=>{
    this.cartService.setCart(response.cart, response.subTotal);
 })
    // console.log(this.cartItemList)

    // console.log("Total: " + this.totalPrice)
  }
}
