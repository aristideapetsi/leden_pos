import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  ResetPasswordForm = this.formBuilder.group({
    email: new FormControl('', [Validators.email, Validators.required])
  })
  error : boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private toast: ToastHelper
  ) { }

  ngOnInit(): void {
  }

  get f(){
    return this.ResetPasswordForm.controls;
  }

  ResetPassword(){
    this.toast.toast_info('Réinitialisation', 'Patientez un moment...');
    const formData = new FormData();
    formData.append('email', this.ResetPasswordForm.get('email')?.value);
    this.accountService.resetPassword(formData).subscribe((response)=>{
      if(response.code == '204'){
        this.toast.toast_error('Réinitialisation', response.message);
      }else if(response.code == '200'){
        this.toast.toast_success('Réinitialisation', response.message);
        this.ResetPasswordForm.reset();
      }else{
        this.toast.toast_error('Réinitialisation', 'Erreur Inattendue !!!');
      }
    })
  }
}
