import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ArrayHelper } from 'src/app/helpers/ArrayHelper';
import { ShopHelper } from 'src/app/helpers/ShopHelper';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { Cart } from 'src/app/models/cart';
import { CartService } from 'src/app/services/cart/cart.service';
import { LikeService } from 'src/app/services/like/like.service';
import Swal from 'sweetalert2';
import { Product } from '../../../models/product';
import { ShopComponent } from '../shop/shop.component';
import { ProductService } from './product.service';
@Component({
  selector: 'app-productsingle',
  templateUrl: './productsingle.component.html',
  styleUrls: ['./productsingle.component.css']
})
export class ProductsingleComponent implements OnInit {
  productId : number;
  products : Product[] = [];
  product : Product;
  catId: number;
  relatedProducts : Product[];
  productDetail : Product ;
  cartItemList: Cart[] = [];
  popularProducts : Product[] = [];
  addProdToCartStatus : boolean = false;
  addProdToCartIdProd: number;

  likeStatus: boolean = false;
  likedProdsId : number[] = [];
  arrayHelper: ArrayHelper = new ArrayHelper();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productDetailService : ProductService,
    private toastHelper: ToastHelper,
    private cartService: CartService,
    private likeService: LikeService,
    private shopHelper: ShopHelper
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit(): void {

    this.productId = Number(this.route.snapshot.paramMap.get('id'));
    this.productDetailService.getProductDetail(this.productId).subscribe((response: any)=>{
      this.product = response.product;
      this.catId = response.product.category_id;
      // console.log(this.catId)
      this.productDetailService.getRelatedProduct(this.catId).subscribe((response: any)=>{
        this.relatedProducts = response.products;
        console.log(this.relatedProducts)
      });
    });

  }

  /**
   * get Liked Prods by user
   */
   getLikedProdsId(){
    this.likeService.getLikedProds().subscribe((response)=>{
      if(response.code == 200){
        this.likedProdsId = response.likedProducts;
      }else{
        this.likedProdsId = [];
      }
      // console.log(this.likedProdsId)
    });
  }

  /**
   * Check popular product in shop
   * @param idProd
   * @returns
   */
  checkIfPopularProd(idProd: number){
    return this.popularProducts.some(elem => elem.id === idProd)
  }

  /**
   * check if a product is in cart
   * @param idProd
   * @returns
   */
  checkIfProdInCart(idProd: number){
    return this.cartItemList.some(elem => elem.product_id === idProd);
  }
  /**
   * Get prod quantity in cart
   * @param idProd
   * @returns
   */
  getCartProdQty(idProd: number){
    return this.cartItemList.find(element => element.product_id == idProd)?.prod_qty  ;
  }
  /**
   * Like product function
   */
  tapLike(idProd:number){
    this.likeService.likeProduct(idProd).subscribe((response)=>{
      if(response.code == "401"){
        this.toastHelper.toast_info('Aimer ce produit', response.message);
        Swal.fire({
          title: 'Bienvenue sur LEDEN POS',
          text: "Vous devez vous connecter avant de continuer!",
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ok, Se connecter!',
          cancelButtonText: 'ANNULER'
        }).then((result) => {
          if (result.isConfirmed) {
            //REdirection foward connexion page
            this.router.navigateByUrl('/login_pos');
          }
        })
        return;
      }else if(response.code = "200"){

        if(response.like_status == 1){
          this.likeStatus = true;
        }else if(response.like_status == 0){
          this.likeStatus = false;
        }
        this.toastHelper.toast_success('Aimer ce produit', response.message);
        this.getLikedProdsId();
      }
      // console.log("aime: " + this.likeStatus)
    })
  }
/**
 * Add TO CART
 * @param id
 * @param quantity
 */
   /**
   * Add product to cart
   */
    addToCart(id: number, quantity: any, stock_prod: any){
      this.shopHelper.addToCart(id, quantity, stock_prod);
      this.addProdToCartIdProd = -1;
     }

  removeAddCartBtn(){
    // this.addProdToCartStatus = false;
    this.addProdToCartIdProd = -1;
  }
  addBtn(id:any){
    // console.log(event)
    // this.addProdToCartStatus = true;
    this.addProdToCartIdProd = id;
  }

  public showDetail(id:number){
    this.productDetail = this.products.filter(item => item.id == id)[0];

   }

}
