import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  apiUrl = "http://localhost:8000/api/shop/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }


  constructor(
    private http: HttpClient
  ) { }



      /**
     * Get Product detail
     * @param id
     * @returns
     */
       getProductDetail(id: number): Observable<any>{
        return this.http.get<any>(this.apiUrl + "productDetail/" + id)
        .pipe(
          catchError(this.errorHandler)
        )
      }
      /**
       * Get related products to specific category
       * @param idCat
       */
      getRelatedProduct(idCat: number): Observable<any>{
        return this.http.get<any>(this.apiUrl + "relatedProduct/" + idCat)
        .pipe(
          catchError(this.errorHandler)
        )
      }

    /**
     *
     * @param error
     * @returns
     */
    errorHandler(error: any){
      let errorMessage = '';
      if(error.error instanceof ErrorEvent)
        errorMessage = error.error.message;
      else
        errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

    return throwError(errorMessage);
    }
}
