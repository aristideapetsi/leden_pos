import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginEcommComponent } from './login-ecomm.component';

describe('LoginEcommComponent', () => {
  let component: LoginEcommComponent;
  let fixture: ComponentFixture<LoginEcommComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginEcommComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginEcommComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
