import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login-ecomm',
  templateUrl: './login-ecomm.component.html',
  styleUrls: ['./login-ecomm.component.css']
})
export class LoginEcommComponent implements OnInit {

  // Variables
  form: FormGroup;
  loading: boolean = false;
  errors:any = null;
  UserProfile: User;
  role : number;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private token: TokenService,
    private authState: AuthStateService,
    private toast: ToastHelper

  ) {
    this.form = fb.group({
      email: [
        '',
        [Validators.required, Validators.email]
      ],
      password: [
        '',
        Validators.required
      ]
    });
  }
  ngOnInit(): void {

  }

    /**
   * Login the user based on the form values
   */
     login(): void {
      this.loading = true;
      this.errors = false;

      this.authService.signin(this.form.value).subscribe(
        (result) => {
          this.responseHandler(result);
          this.loading = false;
        },
        (error) => {
          this.errors = error.error;
          this.loading = false;
        },
        () => {
          this.authState.changeAuthStatus(true);
          // this.form.reset();
          this.loading = false;
          var role = this.UserProfile?.role;
          this.authService.profileUser().subscribe((data: any) =>{
            this.UserProfile = data;
            if(this.UserProfile.role == 0 || this.UserProfile.role == 1 ){
                this.router.navigate(['/dashboard']);
            }
            if(this.UserProfile.role == 2 ){
                this.router.navigate(['/orders/cashiers_list']);
            }
            if(this.UserProfile.role == 3 ){
              this.router.navigate(['/orders/prepa_list']);
            }
            if(this.UserProfile.role == 4 ){
              this.router.navigate(['/dashboard_ecomm']);
            }
          });
        });
    }

    /**
     * Getter for the form controls
     */
    get controls() {
      return this.form.controls;
    }

    //handle response
   responseHandler(data: any){
     console.log("expires data: " + data.expires_in)
    this.token.handleData(data.access_token);
    this.autoLogout(data.expires_in)

  }

  logout() {
    this.authState.changeAuthStatus(false);
    this.token.removeToken();
    this.router.navigate(['login_pos']);
    this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');
  }

  autoLogout(expirationTime: number){
    // console.log(expirationTime);
    setTimeout(()=>{
      this.logout();
      Swal.fire({
        title: 'Session Expiré',
        text: "Vous etes déconnecté suite à l'expiration de votre session!",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Me reconnecter!',
        cancelButtonText: 'ANNULER'
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigate(['/login_pos']);
        }
      })

    }, expirationTime * 1000)
  }
}
