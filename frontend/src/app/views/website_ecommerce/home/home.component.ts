import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { Cart } from 'src/app/models/cart';
import { Category } from 'src/app/models/category';
import { CartService } from 'src/app/services/cart/cart.service';
import { LikeService } from 'src/app/services/like/like.service';
import { CategoryService } from 'src/app/services/products/category.service';
import { Product } from '../../../models/product';
import { ProductService } from '../productsingle/product.service';
import { ShopService } from 'src/app/services/shop/shop.service';
import { ShopHelper } from 'src/app/helpers/ShopHelper';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'
]
})

export class HomeComponent implements OnInit {
  @ViewChild('widgetsContent') widgetsContent: ElementRef;

  //Slider settings
  slideConfig = {"slidesToShow": 1, "slidesToScroll": 1} ;
  products : Product[] = [];
  categories: Category[] = [];
  popularProducts: Product[] = [];
  p:number;
  productDetail : Product ;

  cartItemList : Cart[] = [];
  addProdToCartStatus : boolean = false;
  addProdToCartIdProd: number;

  likeStatus: boolean = false;
  likedProdsId : number[] = [];
  contentLoaded: boolean = true;
  constructor(
    private shopService: ShopService,
    private toastHelper: ToastHelper,
    private productDetailService : ProductService,
    private likeService: LikeService,
    private cartService: CartService,
    private categoryProdService: CategoryService,
    private shopHelper : ShopHelper
  ) { }

  ngOnInit(): void {
    this.shopService.getAll().subscribe((response: any) =>{
      this.products = response.products;
      this.contentLoaded = false;
    });
    this.shopService.getMostPopularProducts().subscribe((response: any)=>{
      this.popularProducts = response.products;
    });

    //get liked prods
    this.getLikedProdsId();
    //get list category products
    this.categoryProdService.getProductCategories().subscribe((response)=>{
      this.categories = response.categories;
    });
  }

  /* Check popular product in shop
  * @param idProd
  * @returns
  */
 checkIfPopularProd(idProd: number){
   return this.popularProducts.some(elem => elem.id === idProd)
 }

   /**
   * check if a product is in cart
   * @param idProd
   * @returns
   */
    checkIfProdInCart(idProd: number){
      return this.cartItemList.some(elem => elem.product_id === idProd);
    }
    /**
     * Get prod quantity in cart
     * @param idProd
     * @returns
     */
    getCartProdQty(idProd: number){
      return this.cartItemList.find(element => element.product_id == idProd)?.prod_qty  ;
    }
    /**
   * get Liked Prods by user
   */
     getLikedProdsId(){
      this.likeService.getLikedProds().subscribe((response)=>{
        if(response.code == 200){
          this.likedProdsId = response.likedProducts;
        }else{
          this.likedProdsId = [];
        }
        // console.log(this.likedProdsId)
      });
    }

    /**
     * Like product function
     */
    tapLike(idProd:number){
      this.likeService.likeProduct(idProd).subscribe((response)=>{
        if(response.code == "401"){
          this.toastHelper.toast_info('Aimer ce produit', response.message);
          return;
        }else if(response.code = "200"){

          if(response.like_status == 1){
            this.likeStatus = true;
          }else if(response.like_status == 0){
            this.likeStatus = false;
          }
          this.toastHelper.toast_success('Aimer ce produit', response.message);
          this.getLikedProdsId();
        }
        // console.log("aime: " + this.likeStatus)
      })
    }

    scrollLeft(){
      this.widgetsContent.nativeElement.scrollLeft -= 150;
    }

    scrollRight(){
      this.widgetsContent.nativeElement.scrollLeft += 150;
    }
    removeAddCartBtn(){
      // this.addProdToCartStatus = false;
      this.addProdToCartIdProd = -1;
    }
    addBtn(id:any){
      // console.log(event)
      // this.addProdToCartStatus = true;
      this.addProdToCartIdProd = id;
    }

    /**
     * Add product to cart
     */
     addToCart(id: number, quantity: any, stock_prod: number){
      this.shopHelper.addToCart(id, quantity, stock_prod);
      this.addProdToCartIdProd = -1;
     }

    /**
     * Get cart info
     */
    getCartInfo(){
      this.cartService.cartItemList$.subscribe((response)=>{
        this.cartItemList = response;
      })

      this.cartService.getCartInfo().subscribe((response)=>{
         this.cartService.setCart(response.cart, response.subTotal);
      })
    }


  public showDetail(id:number){
    this.productDetail = this.products.filter(item => item.id == id)[0];
   }


}
