import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Cart } from '../../../models/cart';
import { CartService } from 'src/app/services/cart/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartItemList : Cart[];
  totalPrice: number;
  totalItems : number = 0;

  constructor(
    private cartService : CartService,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
    this.getCartInfo();
  }
  getCartInfo(){

    this.cartService.cartItemList$.subscribe((response)=>{
      this.cartItemList = response;
      this.totalItems = response.length;
      // console.log(this.cartItemList)
   })
   this.cartService.totalPrice$.subscribe((totalPrice)=>{
      this.totalPrice = totalPrice;
      // console.log(this.totalPrice )
   })
   this.cartService.getCartInfo().subscribe((response)=>{
    this.cartService.setCart(response.cart, response.subTotal);
 })
    // console.log(this.cartItemList)

    // console.log("Total: " + this.totalPrice)
  }

  removeProduct(cartIemId: number, product_name: string){
    // console.log(cartIemId)
    this.cartService.removeItemToCart(cartIemId).subscribe((response) =>{
      // console.log(response)
      if(response.status == 401){

        this.toast.info('Retirer produit', 'Veuillez-vous connecter pour continuer !!!');

      }else if(response.status == 200){
        this.cartService.getCartInfo().subscribe((response)=>{
          this.cartItemList = response.cart;
          this.totalPrice = response.subTotal;
          this.cartService.setCart(this.cartItemList, this.totalPrice);
       })
        this.toast.success('Retirer produit', product_name + ' vient d\'etre retirer du panier');
        // this.getCartInfo();
      }
    })
  }
}
