import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import { Cart } from '../../../../../models/cart';
import { User } from '../../../../../models/User';
import { CartService } from 'src/app/services/cart/cart.service';
import { OrderNotificationService } from 'src/app/services/notifications/order-notification.service';
import Echo from 'laravel-echo';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { Order } from 'src/app/models/Order';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css',
  '../../../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css'
]
})
export class HeaderComponent implements OnInit {
  UserProfile! : User;
  isSignedIn: boolean = false;
  cartItemList : Cart[] = [];
  cartPrices: number[] = [];
  totalPrice: number = 0;
  totalItems : number = 0;

  ordersNotifs: any = [];
  orderCount = 0;
  constructor(
    public authService: AuthService,
    private auth: AuthStateService,
    public router: Router,
    public token: TokenService,
    private cartService : CartService,
    private toast: ToastHelper,
    private orderNotifService: OrderNotificationService
  ) {
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
      this.getUnreadorderNotifsForCustomer(data.id);

    })
  }

  ngOnInit(): void {
    this.auth.authStatus.subscribe((val) => {
      this.isSignedIn = val;
    });
    this.getCartInfo();
    this.websockets();
  }
  getCartInfo(){

    this.cartService.cartItemList$.subscribe((response)=>{
      this.cartItemList = response;
      this.totalItems = response.length;
      // console.log(this.cartItemList)
   })
   this.cartService.totalPrice$.subscribe((totalPrice)=>{
      this.totalPrice = totalPrice;
      // console.log(this.totalPrice )
   })
    // this.cartService.getCart(this.cartItemList, this.totalPrice);
  }

  removeProduct(cartIemId: number, product_name: string){
    this.cartService.removeItemToCart(cartIemId).subscribe((response) =>{
      if(response.status == 401){
        this.toast.toast_info('Retirer produit', 'Veuillez-vous connecter pour continuer !!!');
      }else if(response.status == 200){
        this.cartService.getCartInfo().subscribe((response)=>{
          this.cartItemList = response.cart;
          this.totalPrice = response.subTotal;
          this.cartService.setCart(this.cartItemList, this.totalPrice);
       })
        this.toast.toast_success('Retirer produit', product_name + ' vient d\'etre retirer du panier');

      }
    })
  }

  playSound() {
    //   let audio: HTMLAudioElement = new Audio('/assets_leden/notif.wav');
    //  audio.play();
      var aSound = document.createElement('audio');
       aSound.setAttribute('src', '/assets_leden/notif.wav');
       aSound.play();
    }
    websockets(){
      const echo = new Echo({
        broadcaster: 'pusher',
        cluster: 'mt1',
        key: 'ABCDEFG@_',
        wsHost: window.location.hostname,
        wsPort: 6001,
        forceTLS: false,
        disableStats: true,
          });

      echo.channel('order-update')
      .listen('OrderUpdate', (resp: any)=>{
        // console.log(resp);
        this.toast.toast_success('Commande', resp.order);
        this.playSound();
        this.getUnreadorderNotifsForCustomer(this.UserProfile.id);
        this.orderCount = this.ordersNotifs.length ;
      });
    }

      /**
       * get unread orders notifications
       */
      getUnreadorderNotifsForCustomer(customer_id: any){
        this.orderNotifService.getCustomerUnreadNotif(customer_id).subscribe((response: any)=>{
          this.ordersNotifs = response.notifications;
        });
      }

      /**
       * mark notif as read
       * @param notif_id
       */
      markNotifAsRead(notif_id:any){
        this.orderNotifService.markNotifAsRead(notif_id).subscribe((response: any)=>{
          // console.log(response.message);
          if(response.code == 200) {
            this.getUnreadorderNotifsForCustomer(this.UserProfile.id);
          }
        });
      }

}
