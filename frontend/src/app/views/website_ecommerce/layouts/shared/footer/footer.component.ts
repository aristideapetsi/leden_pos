import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Cart } from 'src/app/models/cart';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-footer-ecom',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css',
  '../../../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css'
]
})
export class FooterComponentEcom implements OnInit {

  UserProfile! : User;
  isSignedIn: boolean = false;
  cartItemList : Cart[] = [];
  cartPrices: number[] = [];
  totalPrice: number = 0;
  totalItems : number = 0;
  cartShow : boolean = false;

  constructor(
    public authService: AuthService,
    private auth: AuthStateService,
    public router: Router,
    public token: TokenService,
    private cartService : CartService,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
      this.auth.authStatus.subscribe((val) => {
        this.isSignedIn = val;
      });
      this.getCartInfo();

    }

    showCartDetails(){
       !this.cartShow;
    }

    getCartInfo(){

      this.cartService.cartItemList$.subscribe((response)=>{
        this.cartItemList = response;
        this.totalItems = response.length;
        // console.log(this.cartItemList)
     })
     this.cartService.totalPrice$.subscribe((totalPrice)=>{
        this.totalPrice = totalPrice;
        // console.log(this.totalPrice )
     })
      // this.cartService.getCart(this.cartItemList, this.totalPrice);

    }

    removeProduct(cartIemId: number, product_name: string){
      this.cartService.removeItemToCart(cartIemId).subscribe((response) =>{
        if(response.status == 401){
          this.toast.info('Retirer produit', 'Veuillez-vous connecter pour continuer !!!');
        }else if(response.status == 200){
          this.cartService.getCartInfo().subscribe((response)=>{
            this.cartItemList = response.cart;
            this.totalPrice = response.subTotal;
            this.cartService.setCart(this.cartItemList, this.totalPrice);
         })
          this.toast.success('Retirer produit', product_name + ' vient d\'etre retirer du panier');

        }
      })
    }
}
