import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(
    private auth :AuthStateService,
    private token : TokenService,
    private router : Router,
    private toast: ToastHelper
  ) { }

  ngOnInit(): void {
  }

      // Signout
      logout() {
        this.auth.changeAuthStatus(false);
        this.token.removeToken();
        this.router.navigate(['login_pos']);
        this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');
      }
}
