import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Order } from 'src/app/models/Order';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  orders : Order[] = [];

  orderDetails : Order ;
  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.getOrders();
  }


  /**
   * Get all order for connected user
   */
  getOrders(){
    this.orderService.getOrderByCustomer().subscribe((response)=>{
      this.orders = response.orders;
      // console.log(this.orders);
      this.dtTrigger.next(this.orders)
    })
  }

  /**
   * Get specific order by id
   * @param orderId
   */
  showOrder(orderId: number){
    this.orderService.getOrderDetail(orderId).subscribe((response) => {
      if(response.code == 200){
        this.orderDetails = response.order;
        // console.log(this.orderDetails)
      }else{
        // print('Erreur Inttendue Commande !!!');
      }
    })
  }

}
