import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  loading: boolean = false;
  errors:any = null;
  submitted: boolean = false;
  form = new FormGroup({
    username:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
    firstname:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
    lastname:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
    email: new FormControl('', [ Validators.required, Validators.email ]),
    phone_number: new FormControl('', [ Validators.required, ]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    verification_password: new FormControl('', [Validators.required]),
  },);

  constructor(
   private accountService : AccountService,
   private toast: ToastHelper,
   private router: Router
  ) { }

  ngOnInit(): void {
  }

  get f(){
    return this.form.controls;
  }

  signup(){
    this.loading = true;
    this.errors = false;
    this.submitted = true;
    this.accountService.signup(this.form.value).subscribe((response: any) => {
      if(response.code == 204){
        this.errors = response.message;
        console.log(response.message)
        return;

      }else if(response.code == 200){
        this.loading = false;
        this.form.reset();
        // console.log('User created successfully');
        // this.router.navigate(['/dashboard_ecomm']);
        this.toast.toast_success("Inscription", "Inscription effectuée avec succès. Vous pouvez-vous connectez !!!");
        this.router.navigate(['/login_pos']);
      }
    });
  }
}
