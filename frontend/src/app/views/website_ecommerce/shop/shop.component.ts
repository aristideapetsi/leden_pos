import { Component, OnInit,OnDestroy } from '@angular/core';
import { Product } from '../../../models/product';
import { ShopService } from 'src/app/services/shop/shop.service';
import { ProductService } from '../productsingle/product.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Category } from '../../../models/category';
import { CategoryService } from 'src/app/services/products/category.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { Cart } from 'src/app/models/cart';
import { LikeService } from 'src/app/services/like/like.service';
import { ArrayHelper } from 'src/app/helpers/ArrayHelper';
import { ToastHelper} from 'src/app/helpers/ToastHelper';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { SpinnerHelper } from 'src/app/helpers/SpinnerHelper';
import { ShopHelper } from 'src/app/helpers/ShopHelper';
import * as $ from 'jquery';
import { LabelType, Options } from '@angular-slider/ngx-slider';
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css',
  './slider_range.component.scss',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',
]
})
export class ShopComponent implements OnInit , OnDestroy{
  p: number = 1;
   products : Product[] = [];
   categories : Category[] = []; //categories for filter
  productsResearch : Product[] = []; //products for filter
  popularProducts : Product[] = [];
   productDetail : Product ;
  category_name : string = "";
  prodSelecCategories : string[] = [];
  viewList  : boolean = true;
   likeStatus: boolean = false;
   likedProdsId : number[] = [];

   cartItemList: Cart[] = [];
   totalPrice: number;
  dtOptions: any = {};
  // dtTrigger: Subject<any> = new Subject<any>();
  contentLoaded: boolean = true;
  addProdToCartStatus : boolean = false;
  addProdToCartIdProd: number;
  dropdownListCategories = [];
  dropdownListProducts = [];
  selectedCategories  : number[] = [];
  selectedProducts  : number[] = [];
  dropdownSettingsCat : IDropdownSettings;
  dropdownSettingsProd : IDropdownSettings;

  arrayHelper: ArrayHelper = new ArrayHelper();

  category_id: number;
  categoryDetail: Category;

  minPrice: number = 100;
  maxPrice: number = 25000;
  ceilfixed: number = this.maxPrice;
  options: Options = {
    floor: 0,
    ceil: this.ceilfixed + 500 ,
    step: 100,
    showTicks: true,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value +  "<p> FCFA</p> " ;
        case LabelType.High:
          return value +  "<p> FCFA</p> ";
        default:
          return value +  "<p> FCFA</p> ";
      }

    }
  };
  constructor(
    private shopService: ShopService,
    private categoryService: CategoryService,
    private productDetailService : ProductService,
    private cartService: CartService,
    private likeService: LikeService,
    private toastHelper: ToastHelper,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: SpinnerHelper,
    private shopHelper: ShopHelper
  ) {
  }

  ngOnInit(): void {
    // this.contentLoaded = true;
    this.category_id =  Number(this.route.snapshot.paramMap.get('id'));

    console.log(this.category_id);


      this.shopService.getAll().subscribe((response: any) =>{
        // console.log('contentload: ' + this.contentLoaded)
        // setTimeout(()=>{
          // this.contentLoaded = true;
          if(this.category_id == 0){
            this.products = response.products;
          }else{
            this.onCategorySelect(this.category_id);
          }
          this.minPrice = response.prodMinPrice;
          this.maxPrice = response.prodMaxPrice;
          // console.log('min price :' + this.minPrice  )
          this.dropdownListProducts = response.products;
          this.contentLoaded = false;
        // }, 2000);

      });


    // this.shopService.getMostPopularProducts().subscribe((response: any)=>{
    //     this.popularProducts = response.products;
    // });
    if(this.category_id == 0){
      this.categoryService.getProductCategories().subscribe((response: any) =>{
        this.dropdownListCategories = response.categories;
        console.log(response.categories)
      });
    }else{
      this.shopService.getCategoryInfo(this.category_id).subscribe((response:any) => {
        // console.log(response)
        this.onCategorySelect(response.category)
        this.categoryDetail = response.category
        // this.dropdownListCategories = response.category;

      });
    }


    // this.dtTrigger.next(this.products);
    this.dropdownSettingsCat = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Sélectionner Tout',
      unSelectAllText: 'Déselectionner Tout',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: 'Rechercher...',
      noDataAvailablePlaceholderText: 'Pas de catégories disponible'
    };
    this.dropdownSettingsProd = {
      singleSelection: false,
      idField: 'id',
      textField: 'product_name',
      enableCheckAll : false,
      // selectAllText: 'Sélectionner Tout',
      // unSelectAllText: 'Déselectionner Tout',
      defaultOpen: false,
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: 'Rechercher un produit...',
      noDataAvailablePlaceholderText: 'Aucun produit disponible'
    };
    this.getCartInfo();
    //get liked prods
    this.getLikedProdsId();



  }
  ngOnDestroy(): void {
    // this.dtTrigger.unsubscribe();
  }


  /**
   * get Liked Prods by user
   */
  getLikedProdsId(){
    this.likeService.getLikedProds().subscribe((response)=>{
      if(response.code == 200){
        this.likedProdsId = response.likedProducts;
      }else{
        this.likedProdsId = [];
      }
      // console.log(this.likedProdsId)
    });
  }

  /**
   * Check popular product in shop
   * @param idProd
   * @returns
   */
  checkIfPopularProd(idProd: number){
    return this.popularProducts.some(elem => elem.id === idProd)
  }

  /**
   * check if a product is in cart
   * @param idProd
   * @returns
   */
  checkIfProdInCart(idProd: number){
    return this.cartItemList.some(elem => elem.product_id === idProd);
  }
  /**
   * Get prod quantity in cart
   * @param idProd
   * @returns
   */
  getCartProdQty(idProd: number){
    return  Array.isArray(this.cartItemList) ? this.cartItemList.find(element => element.product_id == idProd)?.prod_qty : 0 ;
  }
  /**
   * Like product function
   */
  tapLike(idProd:number){
    this.likeService.likeProduct(idProd).subscribe((response)=>{
      if(response.code == "401"){
        this.toastHelper.toast_info('Aimer ce produit', response.message);
        Swal.fire({
          title: 'Bienvenue sur LEDEN POS',
          text: "Vous devez vous connecter avant de continuer!",
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ok, Se connecter!',
          cancelButtonText: 'ANNULER'
        }).then((result) => {
          if (result.isConfirmed) {
            //REdirection foward connexion page
            this.router.navigateByUrl('/login_pos');
          }
        })
        return;
      }else if(response.code = "200"){

        if(response.like_status == 1){
          this.likeStatus = true;
        }else if(response.like_status == 0){
          this.likeStatus = false;
        }
        this.toastHelper.toast_success('Aimer ce produit', response.message);
        this.getLikedProdsId();
      }
      // console.log("aime: " + this.likeStatus)
    })
  }
  /**
   * Filter products by price range
   */
   getProductByPriceRange(minPrice: number, maxPrice:number){
    this.spinner.loader();
    this.shopService.getProductByPriceRange(minPrice, maxPrice).subscribe((response: any)=>{
      this.products = response.products;
    });

   }

  /**
   * Filter products by category
  */
  filterProductByCategory(){
    this.products = [];

    this.shopService.FilterProductByCategory(this.selectedCategories).subscribe((response: any)=>{
      this.products = response.products;
      this.dropdownListProducts = response.products
    });
  }
  /**
   * Search products
  */
    searchProducts(){
      // this.products = [];
      this.shopService.searchProduct(this.selectedProducts).subscribe((response: any)=>{
        this.products = response.products;
        // console.log(this.products)
      });
    }
    /**
     * Get cart info
     */
    getCartInfo(){
      this.cartService.cartItemList$.subscribe((response)=>{
        this.cartItemList = response;
      })

      this.cartService.getCartInfo().subscribe((response)=>{
         this.cartService.setCart(response.cart, response.subTotal);
      })
    }

  /**
   * ChANGE PRODUCT LIST VIEW
   * @param value
   */
  changeView(value:any){
    switch(value){
      case "listView":
        this.viewList = true;
        break;
      case "gridView":
        this.viewList = false;
      break;
    }
  }

  /**
   *Action when category selected
   * @param item
   */
  onCategorySelect(item: any ) {
      this.selectedCategories.push(item.id);
    // console.log(this.selectedCategories)
    this.selectedCategories = this.selectedCategories.filter(this.arrayHelper.distinct);
    // console.log(this.selectedCategories)
    this.spinner.loader();
    this.filterProductByCategory();
    this.toastHelper.toast_success('Filtre catégorie Produit', "Filtré par " + item.name);
  }
    /**
   *Action when product selected
   * @param item
   */
   onProductSelect(item: any) {
    this.selectedProducts.push(item.id)
    this.selectedProducts = this.selectedProducts.filter(this.arrayHelper.distinct);
    this.spinner.loader();
    this.searchProducts();
    this.toastHelper.toast_success('Filtre Produit', "Filtré par " + item.product_name);
    // console.log(this.selectedProducts)
  }
/**
 * Action when all categories selected
 */
  onSelectAllCategory() {
    this.selectedCategories = [];
    this.spinner.loader();
    this.filterProductByCategory();
    this.toastHelper.toast_success('Filtre catégorie Produit', "Filter suivant toutes les catégories");
  }

/**
 * Action when all products selected
 */
   onSelectAllProduct() {
    this.selectedProducts = [];
    this.spinner.spin.show();
    this.searchProducts();
    this.spinner.spin.hide();
    this.toastHelper.toast_success('Filtre  Produit', "Filter suivant tous les produits");
  }
  /**
   * Action when category deselected
   */
  onCategoryDeSelect(item: any){
    this.spinner.spin.show();
    this.arrayHelper.searchItemAndRemoveInArray(this.selectedCategories, item);
    this.spinner.spin.hide();
    // this.spinner.loader();

    this.filterProductByCategory();
  }
  /**
   * Action when product deselected
   */
    onProductDeSelect(item: any){
      this.arrayHelper.searchItemAndRemoveInArray(this.selectedProducts, item);
      // this.spinner.spin.show();
      if(this.category_id != 0){
        this.onCategorySelect(this.categoryDetail);
      }

      // this.searchProducts();
      this.spinner.spin.hide();
    }

  /**
   * FILTER PRODUCT
   * @param value
   */
  filterProducts(value: any){
    this.spinner.loader();
    switch(value.target.value){
      case "popular":
        this.shopService.getPopularProducts().subscribe((response: any)=>{
          this.products = response.products;
        });
        break;
      case "lowToHigh":
        this.shopService.getProductsFromLowToCost().subscribe((response: any)=>{
          this.products = response.products;
        });
        break;
      case "HighToLow":
        this.shopService.getProductsFromCostToLow().subscribe((response: any)=>{
          this.products = response.products;
        });
        break;
      case "default":
        this.shopService.getAll().subscribe((response: any) =>{
          this.products = response.products;
        });

    }
    this.spinner.spin.hide();

  }

  removeAddCartBtn(){
    // this.addProdToCartStatus = false;
    this.addProdToCartIdProd = -1;
  }
  addBtn(id:any){
    // console.log(event)
    // this.addProdToCartStatus = true;
    this.addProdToCartIdProd = id;
  }

  /**
   * Add product to cart
   */
   addToCart(id: number, quantity: any, stock_prod: number){
    this.shopHelper.addToCart(id, quantity, stock_prod);
    this.addProdToCartIdProd = -1;
   }

  public showDetail(id:number){
    this.productDetail = this.products.filter(item => item.id == id)[0];

   }


}
