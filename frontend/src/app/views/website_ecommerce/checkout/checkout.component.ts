import { Component, OnInit } from '@angular/core';
// import {Country} from '@angular-material-extensions/select-country';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CartService } from 'src/app/services/cart/cart.service';
import { Cart } from '../../../models/cart';
import { User } from '../../../models/User';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { ToastrService } from 'ngx-toastr';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';

import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  title = 'Checkout';

  countryFormControl = new FormControl();
  countryFormGroup: FormGroup;
  cartItems : Cart[] = [];
  UserProfile! : User;
  isSignedIn: boolean = false;
  orderType: number = 1;
  roleUser: number;

  subTotal: number = 0;
  // mainAmount: number = 0;
  sharedAmount: number = 0;
  priceArray : number[] = [];
  directPay : boolean = true;
  amount : any= 0;

  submitted: boolean = false;
  submittedF: boolean = false;

  checkoutForm : FormGroup = this.formBuilder.group({
    firstname: new FormControl(this.UserProfile?.firstname),
    lastname: new FormControl(this.UserProfile?.lastname),
    address1:new FormControl(this.UserProfile?.address1),
    address2: new FormControl(this.UserProfile?.address2),
    city: new FormControl(this.UserProfile?.city),
    mainAmount: new FormControl(''),
    payment_channel: new FormControl('', [Validators.required]),
    email: new FormControl(''),
    notes: new FormControl(''),
    phone_number: [this.UserProfile?.phone_number],
    contacts: this.formBuilder.array([])
  });

  contactForm = this.formBuilder.group({
    phone_number: new FormControl(''),
    name: new FormControl(''),
    payment_channel: new FormControl(''),
    amount: new FormControl(this.sharedAmount),
  });
  amountShareArray : number[] = [];

  couponForm = new FormGroup({
    coupon_code : new FormControl('', Validators.required)
  });

  constructor(
    private formBuilder: FormBuilder,
    private cartService: CartService,
    public authService: AuthService,
    private auth: AuthStateService,
    private toast : ToastrService,
    private checkoutService: CheckoutService,
    private spinner: NgxSpinnerService
    ) {
      this.authService.profileUser().subscribe((data: any) =>{
        if(data.role != 2){
          this.UserProfile = data;
        }else{
          this.roleUser = data.role
        }
      })
   }

  ngOnInit(): void {
    this.countryFormGroup = this.formBuilder.group({
      country: []
    });
    this.auth.authStatus.subscribe((val) => {
      this.isSignedIn = val;
    });

    this.getCartInfo();

  }

  /**
   * Calculate discount
   */
  calculateDiscount(){
    if(sessionStorage.getItem('coupon')){
      if(sessionStorage.getItem('coupon') == 'fixed'){

      }
    }
  }

  /**
   * Apply coupon
   */
  applyCoupon(){

  }
  /**
   * place order
   * @returns
   */
  placeOrder(){
    const formData = new FormData();
    formData.append('firstname', this.checkoutForm.get('firstname')?.value);
    formData.append('lastname', this.checkoutForm.get('lastname')?.value);
    formData.append('address1', this.checkoutForm.get('address1')?.value);
    formData.append('address2', this.checkoutForm.get('address2')?.value);
    formData.append('city', this.checkoutForm.get('city')?.value);
    formData.append('phone_number', this.checkoutForm.get('phone_number')?.value);
    formData.append('email', this.checkoutForm.get('email')?.value);
    formData.append('payment_channel', this.checkoutForm.get('payment_channel')?.value);
    formData.append('total_price', this.subTotal.toString());
    formData.append('shipping_total', "0");
    formData.append('selling_price', this.subTotal.toString());
    formData.append('split_payment_infos', this.checkoutForm.get('contacts')?.value);
    // formData.append('qty_prod', this.checkoutForm.get('phone_number')?.value);
    if(this.checkoutForm.get('payment_channel')?.value != 1){
      this.spinner.show();
      // console.log(this.checkoutForm.get('contacts'))
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    }

    this.checkoutService.placeOrder(formData).subscribe((response) => {
      if(response.code == 200){
        Swal.fire({
          title: 'Commande Terminé',
          text: "Vous venez d'effectuer avec succès votre commande!!!!",
          icon: 'success',
        })
        this.checkoutForm.reset();
        this.getCartInfo();
      }else if(response.code == 204){
        Swal.fire({
          title: 'Commande Echoué',
          text: response.message,
          icon: 'error',
        });
        return 0;
      }

    });

    return 0;
  }
/**
 * Get cart info
 */
  getCartInfo(){
    this.cartService.cartItemList$.subscribe((response)=>{
      this.cartItems = response;
    })
    this.cartService.totalPrice$.subscribe((response)=>{
      this.subTotal = response;
    })

    this.cartService.getCartInfo().subscribe((response)=>{
      this.cartItems = response.cart;
      this.subTotal = response.subTotal;
      // this.sharedAmount = response.subTotal;
      // console.log(response.cart)
      // console.log(response.subTotal + "faca")
    })
  }

  /**
   *
   * @param event
   */
  onCountrySelected(event:any){

  }

  selectOrderTypeByCashier(e: any){
    if(e.target.value == 1){
      this.orderType = 1;
      this.toast.info('Commande', 'Commande en Cash');
    }else if(e.target.value == 2){
      this.orderType = 2;
      this.toast.info('Commande', 'Commande avec Mobile Money');
    }
  }

  ChangePayType(e: any){
    if(e.target.value == 1){
      this.directPay = true;
      this.toast.info('paiement', 'Régler votre paiement seul.');
    }else if(e.target.value == 2){
      this.directPay = false;
      this.toast.info('paiement', 'Partager votre paiement avec d\'autres contacts.');
    }
  }

  get contacts(): FormArray{
    return this.checkoutForm.controls["contacts"] as FormArray;
  }
/**
 * Add contact form
 */
  addContact(){
    this.submittedF = true;
    var mainAmount = this.checkoutForm.get('mainAmount')?.value
    if(this.contacts.length == 0){
      if(this.subTotal > mainAmount){
        this.sharedAmount += mainAmount;
      }else if(this.subTotal < mainAmount){
        this.toast.info('Contact', 'Le montant est supérieur au montant de la commande !!!');
        return 0;
      }else if (this.subTotal == mainAmount){
        this.toast.info('Contact', 'Le montant a été totalement partagé!!!');
        return 0;
      }
    }
    console.log(this.contactForm);
    console.log(this.sharedAmount)
    if(this.sharedAmount > this.subTotal){
      this.toast.info('Contact', 'Le montant a été totalement partagé!!!');
      console.log("Montant partagé supérieur au montant total")
      return;
    }
    if(this.contacts.length >= 4){
      this.toast.error('Contact', 'Impossible d\'ajouter plus de 4 personnes.');
      console.log('plus de 4 contacts')
      return;
    }
    this.checkoutForm.get('contacts')?.value.map((data: any)=>{
      if((data.amount <=  this.sharedAmount) && (this.sharedAmount < this.subTotal)){
        this.amountShareArray.push(data.amount);
        this.amountShareArray.forEach((element) => {
          this.sharedAmount += element;
        });
        this.amount = data.amount;
      }else{
        this.toast.error('Contact', 'Le montant est supérieur au montant restant à partager!!!');
        return 0;
      }
    })

    console.log('montant : ' + this.amount);
    if((this.sharedAmount > this.amount) && (this.sharedAmount < this.subTotal)){
      this.contacts.push(this.contactForm);
    }else{
      this.toast.info('Contact', 'Le montant a été totalement partagé!!!');
      return;
    }




    console.log('contact amount: ' + this.amount +' shared amount: ' + this.sharedAmount)





    console.log("montant restant à partage: " + (this.subTotal - this.sharedAmount ));

  }



  deleteContact(id: number){

    this.contacts.removeAt(id);
    this.checkoutForm.get('contacts')?.value.map((data: any)=>{
        this.amountShareArray.push(data.amount);
        this.amountShareArray.forEach((element) => {
          this.sharedAmount += element;
        });
        this.amount = data.amount;
    })
    console.log(this.sharedAmount);
    this.toast.success('Contact', 'Contact retiré.');
  }
}
