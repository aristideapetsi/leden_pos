import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/products/category.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css']
})
export class CategoryProductComponent implements OnInit {

  dtOptions: DataTables.Settings = {
    destroy: true,
    retrieve: true,
    paging: false
  };


  dtTrigger: Subject<any> = new Subject<any>();

  categories : Category[] = [];
  form: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    description: ['']
  });
  current_url = this.router.url;
  title_modal = "";
  addCatBtn = false;
  editCatBtn = false;
  category: Category = {'id': 0 , "name": "", "description": "","feature_image":"", "created_at": ""};
  selectedFile : any = null;

  contentLoaded : boolean = true;

  constructor(
    private router: Router,
    private formBuilder : FormBuilder,
    private categoryService: CategoryService,
    private toast: ToastHelper
  ) { }

  ngOnInit(): void {
    this.getCategories();

    this.form = new FormGroup({
      name : new FormControl(this.category.name, [
        Validators.required
      ]),
      feature_image: new FormControl(this.category.feature_image),
      description : new FormControl(this.category.description)
    })

  }

    /**
   * Get list of all categories
   */
     getCategories(){
      this.categoryService.getProductCategories().subscribe((response) => {
        this.categories = response.categories;
        this.dtTrigger.next(response.categories)
        this.contentLoaded = false;
      })
    }

    /**
     *
     * @param event
     */
  onFileSelected(event: any){
    this.selectedFile= event.target.files[0];

    this.form.patchValue({
      feature_image: this.selectedFile
    });
    this.form.get('feature_image')?.updateValueAndValidity();
  }


    /**
   * change state of modal| Add view
   */
     addCat(){
      this.title_modal = "Ajouter une nouvelle catégorie de produit";
      this.addCatBtn = true;
      this.editCatBtn = false;
      this.form.reset();

    }

  /**
   * Add product category
   */
  addCategory(){
    const formData = new FormData();
    formData.append('name', this.form.get('name')?.value);
    formData.append('description', this.form.get('description')?.value);
    formData.append('feature_image', this.selectedFile, this.selectedFile.name);

    this.categoryService.addCategory(formData).subscribe((response)=>{
      if(response.code == 200){
        this.toast.toast_success('Ajout de catégorie produit', response.message)
      }else if(response.code == 401){
        this.toast.toast_info('Ajout de catégorie produit', response.message)
      }else{
        this.toast.toast_error('Ajout de catégorie produit', response.message)
      }
    })
  }

  /***
   * Delete Product
   */
   removeCategory(id: number){
    Swal.fire({
      title: 'Etes-vous sur ?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Supprimer!',
      cancelButtonText: 'ANNULER'
    }).then((result) => {
      if (result.isConfirmed) {
        this.categoryService.removeCategory(id).subscribe(res => {
          if(res.code == 200){
              this.toast.toast_success('Catégorie suppression', res.message);
          }else if(res.code == 401){
            this.toast.toast_info('Catégorie suppression', res.message);
          }else{
            this.toast.toast_error('Catégorie suppression', "Erreur inattendue !!!");
          }
        })
        Swal.fire(
          'Supprimé!',
          'Cette catégorie a été supprimé!',
          'success'
        )
      }
    })

  }

  editCategory(id: number, name: string, description:  string, feature_image: string){
    this.form.reset();
    this.title_modal = "Modifier les informations de " + name;
    this.category.id = id;
    this.category.name = name;
    this.category.description = description;
    this.category.feature_image = feature_image;
    this.addCatBtn = false;
    this.editCatBtn = true;
  }

  get f(){
    return this.form.controls;
  }

  updateCategory(catId: number){
    const formData = new FormData();
    formData.append('name', this.form.get('name')?.value);
    formData.append('description', this.form.get('description')?.value);
    formData.append('feature_image', this.selectedFile, this.selectedFile.name);

    this.categoryService.updateCategory(catId, formData).subscribe((response) =>{
      console.log(response);
      this.router.navigate([this.current_url]);
      this.toast.toast_success('Cette catégorie a été mis à jour avec succès!', 'Mise à jour de la catégorie');
    });

  }


}
