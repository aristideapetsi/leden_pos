import { Component, OnInit } from '@angular/core';
import { Order } from '../../../models/Order';
import { CashierService } from 'src/app/services/cashier/cashier.service';
import Swal from 'sweetalert2';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { OrderNotificationService } from 'src/app/services/notifications/order-notification.service';
import Echo from 'laravel-echo';

@Component({
  selector: 'app-cashiers',
  templateUrl: './cashiers.component.html',
  styleUrls: ['./cashiers.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css']
})
export class CashiersComponent implements OnInit {

  orders_cashiers : Order[] = [];
  isShow = true;
  p:number = 1;

  constructor(
    private cashierService: CashierService,
    private toastr: ToastHelper,
    private orderNotifService: OrderNotificationService
  ) { }

  ngOnInit(): void {
    this.getCashierOrders();
    this.websockets();
  }

  websockets(){
    const echo = new Echo({
      broadcaster: 'pusher',
      cluster: 'mt1',
      key: 'ABCDEFG@_',
      wsHost: window.location.hostname,
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
        });

    echo.channel('orders')
    .listen('NewTrade', (resp: any)=>{
        this.getCashierOrders();
    });
  }

  /**
   * Get cashiers orders
   */
  getCashierOrders(){
    this.cashierService.getAll().subscribe((response) =>{
      this.orders_cashiers = response.orders;

      });
  }
  /**
   *cash money by cashier
   * @param order_id
   */
  cashMoneyByCashier(order_id: number){
    this.cashierService.cashMoneyByCashier(order_id).subscribe((response) => {
      console.log(response);
      this.ngOnInit();
      this.toastr.toast_success("Encaissement", "Commande N* " + order_id + " encaissé avec succès!!!")

    })
  }


  /**
   * Cancel Order
   * @param orderId
   */
  cancelOrder(orderId:number){
    Swal.fire({
      title: 'Etes-vous sur d\'annuler cette commande ?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Annuler!',
      cancelButtonText: 'ANNULER'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cashierService.cancelOrder(orderId).subscribe((response: any)=>{
          if(response.code == 200){
              this.ngOnInit();
              this.toastr.toast_success('Commande', 'Commande annulée avec succès !!!');
          }else if(response.code == '401'){
            this.toastr.toast_info('Commande', response.message);
          }else  {
            this.toastr.toast_error('Commande', 'Erreur Inattendue !!!');
          }
        })

        Swal.fire(
          'Annulé!',
          'Cette commande vient d\'etre annuler!',
          'success'
        )
      }
    })
  }

    /**
   *cash money by cashier
   * @param order_id
   */
   deliveredOrder(order_id: number){
    this.cashierService.deliveryOrderByCashier(order_id).subscribe((response) => {
      console.log(response);
      this.ngOnInit();
      this.toastr.toast_success("Livraison", "Commande N* " + order_id + " livré avec succès!!!")
    })
  }

  showDatatable(){
    this.isShow = !this.isShow;
  }
}
