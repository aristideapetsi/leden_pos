import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { CashierService } from 'src/app/services/cashier/cashier.service';
import { CashiersComponent } from '../cashiers.component';

@Component({
  selector: 'app-order-cashier-datatable',
  templateUrl: './order-cashier-datatable.component.html',
  styleUrls: ['./order-cashier-datatable.component.css']
})
export class OrderCashierDatatableComponent implements OnInit, OnDestroy {
  orders_data : any = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private cashierService: CashierService,
    private cashierComponent: CashiersComponent,
    private toastr: ToastHelper
  ) { }

  ngOnInit(): void {
    this.cashierService.getAllOrdersDataForCashier().subscribe((response: any) =>{
      console.log(response)
      this.orders_data = response.orders;
      //initiate our data table
      this.dtTrigger.next(this.orders_data)
    })
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  cashMoneyByCashier(order_id: number){
    this.cashierService.cashMoneyByCashier(order_id).subscribe((response) => {
      console.log(response);
      this.ngOnInit();
      this.toastr.toast_success("Encaissement", "Commande N* " + order_id + " encaissé avec succès!!!")

    })
  }
  deliveredOrder(order_id: number){
    this.cashierService.deliveryOrderByCashier(order_id).subscribe((response) => {
      console.log(response);
      this.ngOnInit();
      this.toastr.toast_success("Livraison", "Commande N* " + order_id + " livré avec succès!!!")
    })
  }
}
