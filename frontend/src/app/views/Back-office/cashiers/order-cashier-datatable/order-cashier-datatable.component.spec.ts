import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCashierDatatableComponent } from './order-cashier-datatable.component';

describe('OrderCashierDatatableComponent', () => {
  let component: OrderCashierDatatableComponent;
  let fixture: ComponentFixture<OrderCashierDatatableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderCashierDatatableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCashierDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
