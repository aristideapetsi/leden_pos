import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Order } from 'src/app/models/Order';
import { OrderService } from 'src/app/services/order/order.service';


@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: [
    './stats.component.css',
    '../../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
    '../../../../../assets_leden/vendors/themify-icons/themify-icons.css',
    '../../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
    '../../../../../assets_leden/css/app.min.css'
]
})
export class StatsComponent implements OnInit, OnDestroy {
  orders : any = [];
  dtOptions: DataTables.Settings = {processing: true, responsive: true};
  dtTrigger: Subject<any> = new Subject();

  orderDetails: Order ;
  contentLoaded : boolean = true;

  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.orderService.getAllOrders().subscribe((response: any) =>{
      this.orders = response.orders;
      this.contentLoaded = false;
      // console.log(this.orders);
      //initiate our data table
      this.dtTrigger.next(this.orders);
    })

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  getDiffTimeInMinutes(startDate: any, endDate: any){
    if(endDate == null ){
      return 0;
    }
    endDate = new Date(endDate);
    startDate = new Date(startDate);

    var minutes = Math.abs(endDate.getTime() - startDate.getTime()) / 36e5 * 60;
    return minutes;
  }

    /**
   * Get specific order by id
   * @param orderId
   */
     showOrder(orderId: number){
      this.orderService.getOrderDetail(orderId).subscribe((response) => {
        if(response.code == 200){
          this.orderDetails = response.order;
          console.log(this.orderDetails)
        }else{
          // print('Erreur Inttendue Commande !!!');
        }
      })
    }
}
