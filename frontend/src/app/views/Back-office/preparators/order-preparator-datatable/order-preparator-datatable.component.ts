import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { PreparatorService } from 'src/app/services/preparator/preparator.service';
import { PreparatorsComponent } from '../preparators.component';

@Component({
  selector: 'app-order-preparator-datatable',
  templateUrl: './order-preparator-datatable.component.html',
  styleUrls: ['./order-preparator-datatable.component.css']
})
export class OrderPreparatorDatatableComponent implements OnInit {

  orders_data : any = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private preparatorService: PreparatorService,
    private preparatorComponent: PreparatorsComponent
  ) { }

  ngOnInit(): void {
    this.preparatorService.getAllOrdersDataForPrepa().subscribe((response: any) =>{
      console.log(response)
      this.orders_data = response.orders;
      //initiate our data table
      this.dtTrigger.next(this.orders_data)
    })
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  validateOrderByPreparator(order_id: number){
    this.preparatorComponent.validateOrderByPreparator(order_id);
    this.ngOnInit();
  }
}
