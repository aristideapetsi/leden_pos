import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPreparatorDatatableComponent } from './order-preparator-datatable.component';

describe('OrderPreparatorDatatableComponent', () => {
  let component: OrderPreparatorDatatableComponent;
  let fixture: ComponentFixture<OrderPreparatorDatatableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderPreparatorDatatableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPreparatorDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
