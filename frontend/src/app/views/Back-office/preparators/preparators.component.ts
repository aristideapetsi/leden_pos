import { Component, OnInit } from '@angular/core';
import { Order } from '../../../models/Order';
import { Product } from 'src/app/models/product';
import { PreparatorService } from 'src/app/services/preparator/preparator.service';
import { ToastrService } from 'ngx-toastr';
import Echo from 'laravel-echo';

@Component({
  selector: 'app-preparators',
  templateUrl: './preparators.component.html',
  styleUrls: ['./preparators.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css']
})
export class PreparatorsComponent implements OnInit {

  prepa_orders : Order[] = [];
  product: Product[] = [];
  isShow = true;
  p: number = 1;

  // textButtonShowDatatable = "Afficher plus de commandes";
  constructor(
    private orderPrepaService : PreparatorService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getPrepOrders();
    this.websockets();
  }

  getPrepOrders(){
    this.orderPrepaService.getAll().subscribe((response) =>{
      this.prepa_orders = response.orders;
      // console.log(response.orders)
    })
  }


  websockets(){
    const echo = new Echo({
      broadcaster: 'pusher',
      cluster: 'mt1',
      key: 'ABCDEFG@_',
      wsHost: window.location.hostname,
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
        });

    echo.channel('orders')
    .listen('NewTrade', (resp: any)=>{
        this.getPrepOrders();
    });
  }
/**
 * Validate order by preparator
 * @param order_id
 */
  validateOrderByPreparator(order_id: number){
    this.orderPrepaService.validateOrderPrepare(order_id).subscribe((response) => {
      console.log(response)
    });
    this.ngOnInit();
    this.toastr.success("préparation", "Commande N* " + order_id + " terminé avec succès!!!")
    console.log('order validate by preparator')
  }

  showDatatable(){
    this.isShow = !this.isShow;
  }

}
