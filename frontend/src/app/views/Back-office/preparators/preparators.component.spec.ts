import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparatorsComponent } from './preparators.component';

describe('PreparatorsComponent', () => {
  let component: PreparatorsComponent;
  let fixture: ComponentFixture<PreparatorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparatorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
