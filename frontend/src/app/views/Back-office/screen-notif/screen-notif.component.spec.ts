import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenNotifComponent } from './screen-notif.component';

describe('ScreenNotifComponent', () => {
  let component: ScreenNotifComponent;
  let fixture: ComponentFixture<ScreenNotifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScreenNotifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenNotifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
