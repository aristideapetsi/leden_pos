import { Component, OnInit } from '@angular/core';
import  Echo  from 'laravel-echo';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { Order } from 'src/app/models/Order';
import { CashierService } from 'src/app/services/cashier/cashier.service';
import { OrderNotificationService } from 'src/app/services/notifications/order-notification.service';

@Component({
  selector: 'app-screen-notif',
  templateUrl: './screen-notif.component.html',
  styleUrls: ['./screen-notif.component.css']
})
export class ScreenNotifComponent implements OnInit {

  ordersNotifs : any = [];
  orders : Order[] = [];

   daysArray = ['','Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
   date = new Date();
   hour: any;
   minute: any;
   second: any;
   day: string;

  constructor(
    private orderNotifService : OrderNotificationService,
    private cashierService: CashierService,
    private toast: ToastHelper
  ) { }

  ngOnInit(): void {
    this.websockets();
    this.getorderNotifs();
    this.getCashierOrders();

    setInterval(()=>{
      const date = new Date();
      this.updateDate(date);
    }, 1000);

    this.day = this.daysArray[this.date.getDay()];

  }

  updateDate(date:any){
    const hours = date.getHours();
    this.hour = hours;

    const minutes = date.getMinutes();
    this.minute = minutes < 10 ? '0' + minutes : minutes.toString();

    const seconds = date.getSeconds();
    this.second = seconds < 10 ? '0' + seconds : seconds.toString() ;
  }


  playSound() {
    //   let audio: HTMLAudioElement = new Audio('/assets_leden/notif.wav');
    //  audio.play();
      var aSound = document.createElement('audio');
       aSound.setAttribute('src', '/assets_leden/order_update.wav');
       aSound.play();
    }

  websockets(){
    const echo = new Echo({
      broadcaster: 'pusher',
      cluster: 'mt1',
      key: 'ABCDEFG@_',
      wsHost: window.location.hostname,
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
        });

    echo.channel('order-update')
    .listen('OrderUpdate', (resp: any)=>{
      // console.log(resp.order);
      this.toast.toast_success('Commande', resp.order);
      // this.orderCount ++;
      this.playSound();
      // this.getorderNotifs();
      this.getCashierOrders();
    });
  }
     /**
     * get orders notifications
     */
      getorderNotifs(){
        this.orderNotifService.getOrderUpdateNotifications().subscribe((response: any)=>{
          this.ordersNotifs = response.notifications;
          // this.orderCount = this.ordersNotifs.length ;
        });
      }

        /**
   * Get cashiers orders
   */
  getCashierOrders(){
    this.cashierService.getAll().subscribe((response) =>{
      this.orders = response.orders;
      console.log(this.orders)
      });
  }
}
