import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/models/User';
import { DashStatsService } from 'src/app/services/stats_admin/dash-stats.service';

import * as Chart from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css']
})
export class DashboardComponent implements OnInit {

  average_order_price = 0;
  total_incomes = 0;
  total_sales = 0;
  total_users = 0;
  unique_years : any = [];
  increase_compare = 0;

  canvas: any;
  ctx: any;
  data: any = [];

  UserProfile!: User;
  constructor(
    public authService: AuthService,
    private dashStats: DashStatsService,
    ) {
    this.authService.profileUser().subscribe((data: any) => {
      this.UserProfile = data;
      // console.log(this.UserProfile)
    });

  }

  ngOnInit(): void {
    this.dashStats.getAll().subscribe((response: any) =>{
      // console.log(response);
      this.average_order_price = Math.round(response.average_order_price * 1000)/1000;
      this.total_incomes = response.total_incomes;
      this.total_sales = response.total_sales;
      this.total_users = response.total_users;
      this.unique_years = (new Set(response.unique_years));
      this.increase_compare = response.increase_compare;
      // console.log(this.unique_years)

    });

    this.getDashboardStats();


  }


  changeOrdersByYear(event: any){
    const year = event.target.value;
    this.dashStats.getOrdersStatsByYear(year).subscribe((response: any) => {
      // console.log(response.datasets[0].values);
      this.canvas = document.getElementById('orderChart');
      this.ctx = this.canvas.getContext('2d');
      const lineCanvasEle: any = document.getElementById('orderChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'line',
          data: {
            labels: response.chart['labels'],
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'orange' },
            ],
          },
          options: {
            responsive: true,
          }
        });
    })

  }

  getDashboardStats(){
    this.dashStats.getOrdersStats().subscribe((response: any) => {
      // console.log(response)
      // console.log(response.datasets[0].values);
      this.canvas = document.getElementById('orderChart');
      this.ctx = this.canvas.getContext('2d');
      const lineCanvasEle: any = document.getElementById('orderChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'line',
          data: {
            labels: response.chart['labels'],
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'orange' },
            ],
          },
          options: {
            responsive: true,
          }
        });
    // this.lineChartLabels = response.chart
    // this.chartOrders = response.datasets;
    // console.log(this.chartOrders)
    // this.inlinePlugin = this.textPlugin;

    })

  }


}
