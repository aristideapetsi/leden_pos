import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountHelper } from 'src/app/helpers/AccountHelper';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { User } from 'src/app/models/User';
import { AccountService } from 'src/app/services/account/account.service';
import { AuthService } from 'src/app/services/auth/auth.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    './profile.component.css',
    '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
    '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
    '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
    '../../../../assets_leden/css/app.min.css']
})
export class ProfileComponent implements OnInit {
  UserProfile! : User;
  loading : boolean = false;
  submitted: boolean = false;
  errors: any;

  PasswordForm = new FormGroup({
    password: new FormControl('', Validators.required),
    verification_password: new FormControl('', [Validators.required, Validators.min(6)]),
  });


  filePath: string;
  AvatarForm : FormGroup = this.formBuilder.group({
    avatar: [null, Validators.required]
  });
  selectedFile:any;
  AccountForm : FormGroup = new FormGroup({
    firstname: new FormControl('',Validators.required),
    lastname: new FormControl('',Validators.required),
    username: new FormControl('',Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone_number: new FormControl(''),
    address: new FormControl('')
  });
  constructor(
    public authService: AuthService,
    private formBuilder : FormBuilder,
    private accountService: AccountService,
    private toast: ToastHelper,
    private accountHelper: AccountHelper
    ) {
    //  this.UserProfile = this.accountHelper.getProfile();
    this.getProfile();
   }

   getProfile(){
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
    })
   }
  ngOnInit(): void {
    this.UserProfile = this.accountHelper.getProfile();
  }
  onFileSelected(event: any){
    this.selectedFile = event.target.files[0];

   this.AccountForm.patchValue({
     avatar: this.selectedFile
   });
   this.AccountForm.get('avatar')?.updateValueAndValidity();
   const reader = new FileReader();
   reader.onload = () => {
     this.filePath = reader.result as string;
   }
   reader.readAsDataURL(this.selectedFile)
 }

  updateAvatar(){
    this.submitted = true;
    const formData = new FormData();
    formData.append('avatar', this.selectedFile, this.selectedFile.name);
    this.accountService.updateProfileAvatar(formData).subscribe((response)=>{
      if(response.code == 200){
        this.submitted = false;
        this.toast.toast_success('Mise à jour Profil', response.message);
        this.getProfile();
      }else if(response.code == 401){
        this.toast.toast_error('Mise à jour Profil', response.message);
      }else{
        this.toast.toast_error('Mise à jour Profil', 'Erreur Inattendue !!!');
      }
    });
  }

  /**
   * Update Admin personnal info
   */
  updateAdminInfo(){
    this.loading = true;
    this.errors = false;
    this.submitted = true;

    const formData = new FormData();

    var firstname = this.AccountForm.get('firstname')?.value;
    var lastname = this.AccountForm.get('lastname')?.value;
    var username = this.AccountForm.get('username')?.value;
    var email = this.AccountForm.get('email')?.value;
    var address1 = this.AccountForm.get('address1')?.value;
    var phone_number = this.AccountForm.get('phone_number')?.value;

    if(firstname == null){
      firstname = this.UserProfile.firstname;
    }
    if(lastname == null){
      lastname = this.UserProfile.lastname;
    }
    if(username == null){
      username = this.UserProfile.username;
    }

    if(email == null){
      email = this.UserProfile.email;
    }
    if(phone_number == null || phone_number == undefined){
      phone_number = this.UserProfile.phone_number;
    }
    if(address1 == null){
      address1 = this.UserProfile.address1;
    }
    formData.append('firstname', firstname);
    formData.append('lastname',  lastname);
    formData.append('username', username);
    formData.append('phone_number', phone_number);
    formData.append('email',  email);
    formData.append('address1',  address1);

    this.accountService.updateAdminAccountInfo(formData).subscribe((response) => {
      this.loading = false;
      this.errors = response.message;
      if(response.code == 200){
        this.toast.toast_success('Mise à jour compte',response.message);
        this.getProfile();
      }else if(response.code == 401){
        this.toast.toast_info('Mise à jour compte',response.message);
      }else{
        this.toast.toast_error('Mise à jour compte', 'Erreur Inattendue !!!');
      }
    })
  }

  /**
   * update admin password
   */
  updatePassword(){
    this.loading = true;
    this.errors = false;
    this.submitted = true;

    const formData = new FormData();

    formData.append('password', this.PasswordForm.get('password')?.value);
    formData.append('verification_password',  this.PasswordForm.get('verification_password')?.value);

    this.accountService.updateAdminPassword(formData).subscribe((response) => {
      this.loading = false;

      if(response.code == 200){
        this.toast.toast_success('Mise à jour mot de passe',response.message);
      }else if(response.code == 401){
        this.toast.toast_info('Mise à jour mot de passe',response.message);
      }else if(response.code == 204){
        this.errors = response.message;
        return;
      }else{
        this.toast.toast_error('Mise à jour mot de passe', 'Erreur Inattendue !!!');
      }
    })
  }


}
