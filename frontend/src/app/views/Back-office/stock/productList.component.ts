import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { ProductService } from 'src/app/services/products/product.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ShopService } from 'src/app/services/shop/shop.service';
import { CategoryService } from 'src/app/services/products/category.service';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { ArrayHelper } from 'src/app/helpers/ArrayHelper';
import { Category } from 'src/app/models/category';
import { Product } from 'src/app/models/product';
import { SpinnerHelper } from 'src/app/helpers/SpinnerHelper';

@Component({
  selector: 'app-product-list',
  templateUrl: './productList.component.html',
  styleUrls: [
    './stock.component.css',
    '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
    '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
    '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
    '../../../../assets_leden/css/app.min.css'
]
})
export class ProductListComponent implements OnInit, OnDestroy {
  products : any = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  emptyBtnDisabled: boolean = false;
  id: number = 0;
  stock : number = 0;
  product_name: string = "";
  productsToEmpty : number[] = [];
  p: number = 1;

  contentLoaded: boolean = true;
  categories : Category[] = []; //categories for filter
  productsResearch : Product[] = []; //products for filter


  dropdownListCategories = [];
  dropdownListProducts = [];
  selectedCategories  : number[] = [];
  selectedProducts  : number[] = [];
  dropdownSettingsCat : IDropdownSettings;
  dropdownSettingsProd : IDropdownSettings;

  arrayHelper: ArrayHelper = new ArrayHelper();
  constructor(
    private productService: ProductService,
    private shopService: ShopService,
    private categoryService: CategoryService,
    private toastHelper: ToastHelper,
    private spinner: SpinnerHelper
  ) { }

  ngOnInit(): void {
    // this.contentLoaded = false;
    this.productService.getAll().subscribe((response: any ) =>{
      this.products = response.products;
      this.contentLoaded = false;
      //initiate our data table
      // this.dtTrigger.next(this.products)
    })

    this.categoryService.getProductCategories().subscribe((response: any) =>{
      this.dropdownListCategories = response.categories;
    });
    this.dropdownSettingsCat = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Sélectionner Tout',
      unSelectAllText: 'Déselectionner Tout',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: 'Rechercher...',
      noDataAvailablePlaceholderText: 'Pas de catégories disponible'
    };
    this.dropdownSettingsProd = {
      singleSelection: false,
      idField: 'id',
      textField: 'product_name',
      enableCheckAll : false,
      // selectAllText: 'Sélectionner Tout',
      // unSelectAllText: 'Déselectionner Tout',
      defaultOpen: false,
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: 'Rechercher un produit...',
      noDataAvailablePlaceholderText: 'Aucun produit disponible'
    };
  }

  ngOnDestroy(): void {
    // this.dtTrigger.unsubscribe();
  }

    /**
   * Filter products by category
  */
     filterProductByCategory(){
      this.products = [];

      this.spinner.loader();
      this.shopService.FilterProductByCategory(this.selectedCategories).subscribe((response: any)=>{
        this.products = response.products;
        this.dropdownListProducts = response.products
      });
    }
    /**
     * Search products
    */
      searchProducts(){
        this.products = [];
        this.spinner.loader();
        this.shopService.searchProduct(this.selectedProducts).subscribe((response: any)=>{
          this.products = response.products;
          // console.log(this.products)
        });
      }
  /**
   *Action when category selected
   * @param item
   */
   onCategorySelect(item: any) {
    this.selectedCategories.push(item.id);
    this.selectedCategories = this.selectedCategories.filter(this.arrayHelper.distinct);
    // console.log(this.selectedCategories)
    this.spinner.loader();
    this.filterProductByCategory();
    this.toastHelper.toast_success('Filtre catégorie Produit', "Filtré par " + item.name);
  }
    /**
   *Action when product selected
   * @param item
   */
   onProductSelect(item: any) {
    this.selectedProducts.push(item.id)
    this.selectedProducts = this.selectedProducts.filter(this.arrayHelper.distinct);
    this.spinner.loader();
    this.searchProducts();
    this.toastHelper.toast_success('Filtre Produit', "Filtré par " + item.product_name);
    // console.log(this.selectedProducts)
  }
/**
 * Action when all categories selected
 */
  onSelectAllCategory() {
    this.selectedCategories = [];
    this.spinner.loader();
    this.filterProductByCategory();
    this.toastHelper.toast_success('Filtre catégorie Produit', "Filter suivant toutes les catégories");
  }

/**
 * Action when all products selected
 */
   onSelectAllProduct() {
    this.selectedProducts = [];
    this.spinner.loader();
    this.searchProducts();
    this.toastHelper.toast_success('Filtre  Produit', "Filter suivant tous les produits");
  }
  /**
   * Action when category deselected
   */
  onCategoryDeSelect(item: any){
    this.arrayHelper.searchItemAndRemoveInArray(this.selectedCategories, item);
    this.spinner.loader();
    this.filterProductByCategory();
  }
  /**
   * Action when product deselected
   */
    onProductDeSelect(item: any){
      this.arrayHelper.searchItemAndRemoveInArray(this.selectedProducts, item);
      this.spinner.loader();
      this.searchProducts();
    }

  /**
   * FILTER PRODUCT
   * @param value
   */
  filterProducts(value: any){
    // console.log(value)
    this.spinner.loader();
    switch(value.target.value){
      case "popular":
        this.shopService.getPopularProducts().subscribe((response: any)=>{
          this.products = response.products;
        });
        break;
      case "lowToHigh":
        this.shopService.getProductsFromLowToCost().subscribe((response: any)=>{
          this.products = response.products;
        });
        break;
      case "HighToLow":
        this.shopService.getProductsFromCostToLow().subscribe((response: any)=>{
          this.products = response.products;
        });
        break;
      case "default":
        this.shopService.getAll().subscribe((response: any) =>{
          this.products = response.products;
        });
    }

  }

  cancProd(id: number){
    // this.emptyBtnDisabled = true;
    this.productsToEmpty.push(id);
    console.log(this.productsToEmpty)
    $('#cancProd-' + id).prop("disabled", true);
    // var id = $('#cancProd').data('id')
    console.log('.productsCard-'+ id )
    $('.productsCard-'+ id ).addClass("border border-light")
    $('.productsCard-'+ id + ' img' ).addClass("overlay-img")
    $('.prodTitle-'+ id ).removeClass("text-dark")
    // $('.prodTitle-'+ id ).addClass("text-light")
    // $('.qty-'+ id).removeClass('badge-dark')
    // $('.qty-'+ id).addClass('badge-light');
    // this.productsToEmpty.push(id);
    console.log(this.productsToEmpty);

  }

  editProd(id: number, stock: number, product_name: string){
    this.id = id;
    this.stock = stock;
    this.product_name = product_name;
  }

  updateStock(id: number, stock: any, product_name: string){
    if(stock == "" || stock == undefined ){
      this.toastHelper.toast_error("Mise à jour Stock", "Le champ stock est obligatoiore !!!");
    }else{
      this.productService.updateStockProd(id, stock).subscribe((response) => {
        console.log(response);
        this.ngOnInit();
        this.toastHelper.toast_success("Mise à jour Stock", "Le stock de " + product_name + " a été mise à jour !!!");
      })
    }

  }


  resetProds(){
    $('.productsC button').prop("disabled", false);
    $('.productsC').removeClass("border border-light")
    $('.productsC img' ).removeClass("overlay-img")
    // $('.productsC span').removeClass('badge-light')
    // $('.productsC span' ).addClass("text-white")
    // $('.productsC span').addClass('badge-dark')
    $('.productsC h6').addClass('text-dark')
    // productsNA = [];
    this.productsToEmpty = [];
    console.log("deselec : " + this.productsToEmpty)
}

  emptyProds(){
    if(this.productsToEmpty.length == 0){
      this.toastHelper.toast_error("Mise à jour Stock", "Aucun produit n'a été sélectionné !!!");
    }else{
      Swal.fire({
        title: 'Voulez-vous vider ces stocks?',
        text: "Vous ne pourrez plus revenir en arrière!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, Vider!',
        cancelButtonText: 'ANNULER'
      }).then((result) => {
        if (result.isConfirmed) {
          this.productService.emptyStockProduct(this.productsToEmpty).subscribe((response) =>{
            console.log(response);
            this.ngOnInit();
            this.toastHelper.toast_success("Mise à jour Stock", "Les stocks sélectionnés ont été vidés avec succès!!!");
            this.productsToEmpty = [];
          })
        }else{
          this.resetProds();
        }
      })

    }

  }
}
