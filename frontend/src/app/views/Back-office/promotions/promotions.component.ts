import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { Coupon } from 'src/app/models/Coupon';
import { CouponService } from 'src/app/services/coupon/coupon.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css'
]
})
export class PromotionsComponent implements OnInit {

  title_modal = "";
  btnAdd = false;
  btnEdit = false;
  coupons : any = [];
  dtOptions: DataTables.Settings =
  {
    destroy: true,
    retrieve: true,
    paging: false
  };



  dtTrigger: Subject<any> = new Subject<any>();

  coupon: Coupon = {'id': 0, 'code': "", "type": "","value": "", "cart_value": ""}

  current_url = this.router.url;
  form = new FormGroup({
    code:  new FormControl('', Validators.required),
    type:  new FormControl('', Validators.required),
    value:  new FormControl('', Validators.required),
    cart_value: new FormControl('', Validators.required),
  },
  );
  submitted: boolean = false;
  errors:any = null;
  loading: boolean = false;

  contentLoaded: boolean = true;

  constructor(
    private router: Router,
    private toastr: ToastHelper,
    private couponService: CouponService
  ) { }

  ngOnInit(): void {
    this.getCouponsList();
  }

  getCouponsList(){
    this.couponService.getAllCoupons().subscribe((response: any) =>{
      this.coupons = response.coupons;
      this.contentLoaded = false;
      //initiate our data table
      this.dtTrigger.next(this.coupons)
    })

  }
  ngOnDestroy(): void {
      this.dtTrigger.unsubscribe();
  }

   get f(){
    return this.form.controls;
  }

  addCoupon(){
    this.title_modal = "Ajouter un nouveau coupon";
    this.btnAdd = true;
    this.btnEdit = false;
    this.submitted = false;
    this.form.reset();

  }

  createCoupon(){
    this.submitted = true;
    this.loading = true;
    const formData = new FormData();
    formData.append('code', this.form.get('code')?.value);
    formData.append('type', this.form.get('type')?.value);
    formData.append('value', this.form.get('value')?.value);
    formData.append('cart_value', this.form.get('cart_value')?.value);
      this.couponService.addCoupon(formData).subscribe((response: any) => {
        this.loading = false;
        if(response.code == 204){
          this.errors = response.message;
          return;
        }else if(response.code == 200){
          console.log('coupon created successfully');
          this.form.reset();
          this.toastr.toast_success("Ajout de coupon", "Coupon ajouté avec succès !!!");
          // this.router.navigateByUrl('users/index');
          this.getCouponsList();
        }

      })


  }


  /***
   * Delete User
   */
  deleteCoupon(id: number){
    Swal.fire({
      title: 'Etes-vous sur ?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Supprimer!',
      cancelButtonText: 'ANNULER'
    }).then((result) => {
      if (result.isConfirmed) {
        this.couponService.deleteCoupon(id).subscribe(res => {
            console.log(res)
            // this.rerenderDatatable(this.users);
        })
        Swal.fire(
          'Supprimé!',
          'Ce coupon a été supprimé!',
          'success'
        )
        this.toastr.toast_success("Suppression de coupon", "Coupon retiré avec succès !!!");

        this.getCouponsList();

      }
    })

  }

  /**
   * Edit user with modal
   * @param id
   * @param role
   * @param username
   * @param lastname
   * @param firstname
   * @param email
   */
  editCoupon(id: number, code: string, type: string, value: string, cart_value: string){
    this.form.reset();
    this.title_modal = "Modifier les informations du coupon N* " + id;
    this.coupon.id = id;
    this.coupon.code = code;
    this.coupon.type = type;
    this.coupon.value = value;
    this.coupon.cart_value = cart_value;
    this.btnEdit = true;
    this.btnAdd = false;
  }

  /**
   * update coupon
   * @param couponId
   */
  updateCoupon(couponId: number){
    this.couponService.updateCoupon(couponId, this.coupon).subscribe((response) =>{
      console.log(response);
      this.router.navigate([this.current_url]);
      this.toastr.toast_success('Ce coupon a été mis à jour avec succès!', 'Mise à jour du coupon');
      this.getCouponsList();
    });
  }

}
