import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';

@Component({
  selector: 'app-left-bar',
  templateUrl: './left-bar.component.html',
  styleUrls: ['./left-bar.component.css']
})
export class LeftBarComponent implements OnInit {
  UserProfile! : User;
  isLoggedIn: boolean ;
  isAdmin: boolean = false;
  isSuperAdmin: boolean = false;
  isCashier: boolean= false;
  isPreparator: boolean = false;
  isDeliverman: boolean = false;

  contentLoaded : boolean = false;
  constructor(
    public authService: AuthService,
    private authStatus : AuthStateService
    ) {
      this.authService.profileUser().subscribe((data: any) =>{
        this.UserProfile = data;

        this.contentLoaded = true;
        var role = this.UserProfile.role;
        if(role == 0){
          this.isSuperAdmin = true;
        }else if(role == 1){
          this.isAdmin = true;
        }else if(role == 2){
          this.isCashier = true;
        }else if(role == 3){
          this.isPreparator = true;
        }else if(role == 4){
          this.isDeliverman = true;
        }

      })
  }

  ngOnInit(): void {
    this.contentLoaded = false;
    this.authStatus.authStatus.subscribe(value => this.isLoggedIn = value);

  }

}
