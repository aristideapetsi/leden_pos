import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import { User } from 'src/app/models/User';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import  Echo  from 'laravel-echo';
import { OrderNotificationService } from 'src/app/services/notifications/order-notification.service';
@Component ({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css',
  '../../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../../assets_leden/css/app.min.css']
})



export class TopBarComponent implements OnInit {

  UserProfile! : User;
  isLoggedIn: boolean = false;

  contentLoaded : boolean = false;
  ordersNotifs : any = [];

  orderCount = 0;

  constructor(
    public authService: AuthService,
    private auth: AuthStateService,
    public router: Router,
    public token: TokenService,
    private toast: ToastHelper,
    private orderNotifService: OrderNotificationService
    ) {
   }

  ngOnInit(): void {
    this.contentLoaded = false;
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
      this.contentLoaded = true;
    })
    this.auth.authStatus.subscribe((val) => {
      this.isLoggedIn = val;

    });

    this.websockets();
    this.getorderNotifs();
  }

  playSound() {
  //   let audio: HTMLAudioElement = new Audio('/assets_leden/notif.wav');
  //  audio.play();
    var aSound = document.createElement('audio');
     aSound.setAttribute('src', '/assets_leden/notif.wav');
     aSound.play();
  }
  websockets(){
    const echo = new Echo({
      broadcaster: 'pusher',
      cluster: 'mt1',
      key: 'ABCDEFG@_',
      wsHost: window.location.hostname,
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
        });

    echo.channel('orders')
    .listen('NewTrade', (resp: any)=>{
      console.log(resp);
      this.toast.toast_success('Commande', resp.order);
      this.orderCount ++;
      this.playSound();
      this.getorderNotifs();
    });
  }

    // Signout
    signOut() {
      this.auth.changeAuthStatus(false);
      this.token.removeToken();
      this.router.navigate(['login']);
      this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');

    }

    /**
     * get orders notifications
     */
    getorderNotifs(){
      this.orderNotifService.getUnreadOrdersNotifications().subscribe((response: any)=>{
        this.ordersNotifs = response.notifications;
        this.orderCount = this.ordersNotifs.length ;
      });
    }

    /**
     * mark notif as read
     * @param notif_id
     */
    markNotifAsRead(notif_id:any){
      this.orderNotifService.markNotifAsRead(notif_id).subscribe((response: any)=>{
        // console.log(response.message);
        if(response.code == 200) {
          this.getorderNotifs();
        }
      });
    }

    /**
     * mark all notif as read
     *
     */
    markAllNotifAsRead(){

    }
}
