import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-shop-settings',
  templateUrl: './shop-settings.component.html',
  styleUrls: ['./shop-settings.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css']
})
export class ShopSettingsComponent implements OnInit {

  UserProfile : User;
  
  constructor(
    private authService : AuthService
  ) {
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
    })
   }

  ngOnInit(): void {
  }

}
