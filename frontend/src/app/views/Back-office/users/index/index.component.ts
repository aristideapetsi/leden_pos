import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from 'src/app/models/User';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { ToastHelper } from 'src/app/helpers/ToastHelper';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css',
  '../../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../../assets_leden/css/app.min.css']
})
export class IndexComponent implements OnInit, OnDestroy {

  title_modal = "";
  btnAdd = false;
  btnEdit = false;
  users : any = [];
  dtOptions: DataTables.Settings = {
    destroy: true,
    retrieve: true,
    paging: false};

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtTrigger: Subject<any> = new Subject<any>();

  user: User = {'id': 0, 'firstname': "", "lastname": "","avatar": "", "email": "", "username": "", "address1": "","address2": "", "created_at": "",
                "phone_number": "", "role": 0, "email_verified_at": "", "updated_at": "", "city": "", "type": ""}

  current_url = this.router.url;
  form = new FormGroup({
    username:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
    firstname:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
    lastname:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
    email: new FormControl('', [ Validators.required, Validators.email ]),
    phone_number: new FormControl('', [ Validators.required, Validators.pattern("^[0-9]*$") ]),
    role: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    confirmPassword: new FormControl('', [Validators.required]),
    verification_password: new FormControl('', [Validators.required]),
  },
  );
  submitted: boolean = false;
  errors:any = null;
  loading: boolean = false;

  constructor(public userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastHelper
    ) { }
    rerenderDatatable(users: User[]): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next(users);
      });
    }
  ngOnInit(): void {
    this.getUsersList();
    this.dtOptions = {
      processing: true,
      responsive: true
    }
  }

  getUsersList(){
    this.userService.getAll().subscribe((response: any) =>{
      this.users = response.users;
      //initiate our data table
      this.dtTrigger.next(this.users)
    })

  }
  ngOnDestroy(): void {
      this.dtTrigger.unsubscribe();
  }

   get f(){
    return this.form.controls;
  }

  addUser(){
    this.title_modal = "Ajouter un nouvel Utilisateur";
    this.btnAdd = true;
    this.btnEdit = false;
    this.submitted = false;
    this.form.reset();

  }

  createUser(){
    this.submitted = true;
    this.loading = true;
      this.userService.createUser(this.form.value).subscribe((response: any) => {
        this.loading = false;
        if(response.code == 204){
          this.errors = response.message;
          return;
        }else if(response.code == 200){
          console.log('User created successfully');
          this.form.reset();
          this.toastr.toast_success("Ajout Utilisateur", "Utilisateur ajouté avec succès !!!");
          // this.router.navigateByUrl('users/index');
          this.getUsersList();
        }

      })


  }

   mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors ) {
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
      return null;
    };
  }

  /***
   * Delete User
   */
  deleteUser(id: number){
    Swal.fire({
      title: 'Etes-vous sur ?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Supprimer!',
      cancelButtonText: 'ANNULER'
    }).then((result) => {
      if (result.isConfirmed) {
        this.userService.delete(id).subscribe(res => {
            console.log(res)
            this.rerenderDatatable(this.users);
        })
        Swal.fire(
          'Supprimé!',
          'Cet utilisateur a été supprimé!',
          'success'
        )
      }
    })

  }

  /**
   * Edit user with modal
   * @param id
   * @param role
   * @param username
   * @param lastname
   * @param firstname
   * @param email
   */
  editUser(id: number, role: number, username: string, lastname: string, firstname: string, email: string){
    this.form.reset();
    this.title_modal = "Modifier les informations de " + username;
    this.user.id = id;
    this.user.role = role;
    this.user.username = username;
    this.user.lastname = lastname;
    this.user.firstname = firstname;
    this.user.email = email;
    this.btnEdit = true;
    this.btnAdd = false;
  }

  updateUser(userId: number){
    this.userService.update(userId, this.user).subscribe((response) =>{
      console.log(response);
      this.router.navigate([this.current_url]);
      this.toastr.toast_success('Ce compte a été mis à jour avec succès!', 'Mise à jour du compte');
    });
  }
}
