import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DashStatsService } from 'src/app/services/stats_admin/dash-stats.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css'
]
})
export class StatisticsComponent implements OnInit {

  canvas: any;
  ctx: any;
  data: any = [];

  average_order_price = 0;
  total_incomes = 0;
  total_sales = 0;
  total_users = 0;
  unique_years : any = [];
  increase_compare = 0;

  total_category_products = 0;
  total_products = 0;
  total_orders = 0;

  constructor(
    private statsService: DashStatsService,
    private dashStats: DashStatsService
  ) { }

  ngOnInit(): void {
    this.dashStats.getAll().subscribe((response: any) =>{
      // console.log(response);
      this.average_order_price = Math.round(response.average_order_price * 1000)/1000;
      this.total_incomes = response.total_incomes;
      this.total_sales = response.total_sales;
      this.total_users = response.total_users;
      this.unique_years = (new Set(response.unique_years));
      this.increase_compare = response.increase_compare;

    });
    this.getOverviewStats();
    this.getProudctByCategory();
    this.getYearlyIncomes();
    this.getDashboardStats();
    this.getMonthlyIncomes();
    this.getUsersStats();
    this.getProductsStats();
    this.getOrdersComparisonCart();

  }

  viewUsersStats(){
    this.getUsersStats();
  }

  getProductsStats(){
    this.statsService.getProductsStats().subscribe((response: any)=>{
      this.canvas = document.getElementById('productsChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('productsChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'pie',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, backgroundColor: [
                '#ff6384',
                '#36a2eb',
            ] },

            ],
          },
          options: {
            responsive: true,
          }
        });
    });

  }

  getOrdersComparisonCart(){
    this.statsService.getcompareOrdersStats().subscribe((response: any)=>{
      this.canvas = document.getElementById('ordersCompChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('ordersCompChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'pie',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, backgroundColor: [
                'gray',
                'green',
                'red',
                'orange',
            ]},

            ],
          },
          options: {
            responsive: true,
          }
        });
    });

  }
  getUsersStats(){
    this.statsService.getUsersStats().subscribe((response: any)=>{
      this.canvas = document.getElementById('usersChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('usersChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'pie',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, backgroundColor: [
                'gray',
                '#36a2eb',
                '#ff6384',
                'blue',
            ]},

            ],
          },
          options: {
            responsive: true,
          }
        });
    });

  }
  changeIncomeByYear(event: any){
    const year = event.target.value
    this.statsService.getIncomesByYear(year).subscribe((response)=>{
      // console.log(response)
      this.canvas = document.getElementById('MonthlyIncomeChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('MonthlyIncomeChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'bar',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'yellow' },

            ],
          },
          options: {
            responsive: true,
          }
        });
    });
  }

  changeOrdersByYear(event: any){
    const year = event.target.value;
    this.dashStats.getOrdersStatsByYear(year).subscribe((response: any) => {
      // console.log(response.datasets[0].values);
      this.canvas = document.getElementById('orderChart');
      this.ctx = this.canvas.getContext('2d');
      const lineCanvasEle: any = document.getElementById('orderChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'line',
          data: {
            labels: response.chart['labels'],
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'orange' },
            ],
          },
          options: {
            responsive: true,
          }
        });
    })

  }

  getProudctByCategory(){
    this.statsService.getProductsByCategoryStats().subscribe((response)=>{
      // console.log(response)
      this.canvas = document.getElementById('productCategoryChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('productCategoryChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'line',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'green' },

            ],
          },
          options: {
            responsive: true,
          }
        });
    });
  }


  /**
   * yearly incomes
   */
  getYearlyIncomes(){
    this.statsService.getIncomes().subscribe((response)=>{
      // console.log(response)
      this.canvas = document.getElementById('incomeChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('incomeChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'bar',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'yellow' },

            ],
          },
          options: {
            responsive: true,
          }
        });
    });
  }
 /**
   * monthly incomes
   */
  getMonthlyIncomes(){
    this.statsService.getMonthlyIncomes().subscribe((response)=>{
      console.log(response)
      this.canvas = document.getElementById('MonthlyIncomeChart');
      this.ctx = this.canvas.getContext('2d');

      const lineCanvasEle: any = document.getElementById('MonthlyIncomeChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'bar',
          data: {
            labels: response.chart.labels,
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'green' },

            ],
          },
          options: {
            responsive: true,
          }
        });
    });
  }
  /**
   * get monthly orders chart
   */
   getDashboardStats(){
    this.dashStats.getOrdersStats().subscribe((response: any) => {
      // console.log(response)
      // this.data.push(response);
      console.log(response.datasets[0].values);
      this.canvas = document.getElementById('orderChart');
      this.ctx = this.canvas.getContext('2d');
      const lineCanvasEle: any = document.getElementById('orderChart')
      const lineChar = new Chart(lineCanvasEle.getContext('2d'), {
        type: 'line',
          data: {
            labels: response.chart['labels'],
            datasets: [
              { data: response.datasets[0].values, label: response.datasets[0].name, borderColor: 'orange' },
            ],
          },
          options: {
            responsive: true,
          }
        });
    })

  }

  getOverviewStats(){
    this.statsService.getOverviewStat().subscribe((response)=>{
      if(response.code == 200){
        this.total_category_products = response.total_product_category;
        this.total_orders = response.total_orders;
        this.total_products = response.total_products;
      }else{
        console.log('erreur inattendue !!!');
      }
    });
  }
}
