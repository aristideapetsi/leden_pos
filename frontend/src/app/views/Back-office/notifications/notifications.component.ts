import { Component, OnInit } from '@angular/core';
import { OrderNotificationService } from 'src/app/services/notifications/order-notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  ordersNotifs : any = [];
  orderCount = 0;
  p : number = 1;
  constructor(
    private orderNotifService: OrderNotificationService
  ) { }

  ngOnInit(): void {
    this.getorderNotifs();
  }
    /**
     * get orders notifications
     */
     getorderNotifs(){
      this.orderNotifService.getAllOrderNotifications().subscribe((response: any)=>{
        this.ordersNotifs = response.notifications;
        this.orderCount = this.ordersNotifs.length ;
      });
    }

}
