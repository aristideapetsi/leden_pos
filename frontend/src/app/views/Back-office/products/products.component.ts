import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { Product } from 'src/app/models/product';
import Swal from 'sweetalert2';
import { ProductService } from 'src/app/services/products/product.service';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/products/category.service';
import { DataTableDirective } from 'angular-datatables';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css',
  '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
  '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
  '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
  '../../../../assets_leden/css/app.min.css']
})
export class ProductsComponent implements OnInit, OnDestroy {
  dtOptions: DataTables.Settings = {
    destroy: true,
    retrieve: true,
    paging: false
  };

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtTrigger: Subject<any> = new Subject<any>();

  products : Product[] = [];
  form: FormGroup ;
  current_url = this.router.url;
  title_modal = "";
  addProdBtn = false;
  editProdBtn = false;
  product: Product = {'id': 0, 'product_name': "", "description": "", "stock_qty": 0 , "price": 0, "category_id": 0, "created_at": "",
                "feature_image": "", "stock_status" : "", "total_sales": 0, categories: {'id': 0 , "name": "", "description": "","feature_image":"", "created_at": ""}}

  categories: Category[] = [];
  selectedFile : any = null;
  contentLoaded : boolean = true;
  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private toastr: ToastrService,
    private router: Router,
    private formBuilder : FormBuilder
  ) {

   }
   rerenderDatatable(products: Product[]): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next(products);
    });
  }
  ngOnInit(): void {
    this.productService.getAll().subscribe((response: any) =>{
      this.products = response.products;
      this.dtTrigger.next(this.products);
      this.contentLoaded = false;
    })

    this.getCategories();

    this.form = new FormGroup({
      product_name : new FormControl(this.product.product_name, [
        Validators.required
      ]),
      feature_image: new FormControl(this.product.feature_image),
      description : new FormControl(this.product.description),
      price: new FormControl(this.product.price,[
        Validators.required
      ]),
      stock_qty : new FormControl(this.product.stock_qty),
      category_id: new FormControl(this.product.category_id,[
        Validators.required
      ])
    })
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  onFileSelected(event: any){
     this.selectedFile= event.target.files[0];

    this.form.patchValue({
      feature_image: this.selectedFile
    });
    this.form.get('feature_image')?.updateValueAndValidity();
  }
  /**
   * change state of modal| Add view
   */
  addProd(){
    this.title_modal = "Ajouter un nouveau produit";
    this.addProdBtn = true;
    this.editProdBtn = false;
    this.form.reset();

  }

  get f(){
    return this.form.controls;
  }


  /**
   * Get list of all categories
   */
  getCategories(){
    this.categoryService.getProductCategories().subscribe((response) => {
      this.categories = response.categories;
    })
  }

  /**
   * Add new product
   */
    addProduct(){
      const formData : any = new FormData();
      formData.append('product_name', this.form.get('product_name')?.value);
      formData.append('feature_image', this.selectedFile, this.selectedFile.name);
      formData.append('description', this.form.get('description')?.value);
      formData.append('price', this.form.get('price')?.value);
      formData.append('category_id', this.form.get('category_id')?.value);
      formData.append('stock_qty', this.form.get('stock_qty')?.value);

      // console.log(this.formBuilder.control)
      // console.log(this.form.value)
        this.productService.addProduct(formData).subscribe((res: any) => {
          if(this.form.errors){
            return;
          }else{
            if(res.status == 200){
              console.log('Product created successfully');
              this.form.reset();
              this.toastr.success("Ajout Produit", "Produit ajouté avec succès !!!");

            }else if(res.status = 204){
              this.toastr.error("Ajout Produit", "Un problème est survenu !!!");
            }
          // this.router.navigateByUrl('users/index');
          }
        })
    }

  /***
   * Delete Product
   */
  removeProduct(id: number){
  Swal.fire({
    title: 'Etes-vous sur ?',
    text: "Vous ne pourrez plus revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, Supprimer!',
    cancelButtonText: 'ANNULER'
  }).then((result) => {
    if (result.isConfirmed) {
      this.productService.deleteProduct(id).subscribe(res => {
      })
      Swal.fire(
        'Supprimé!',
        'Ce produit a été supprimé!',
        'success'
      )
    }
  })

}

/**
 * Edit user with modal
 * @param id
 * @param role
 * @param username
 * @param lastname
 * @param firstname
 * @param email
 */
editProduct(id: number, product_name: string, description: string, stock_qty: number, price: number, category_id: number, feature_image: string){
  this.form.reset();
  this.title_modal = "Modifier les informations de " + product_name;
  this.product.id = id;
  this.product.product_name = product_name;
  this.product.description = description;
  this.product.stock_qty = stock_qty;
  this.product.price = price;
  this.product.category_id = category_id;
  this.product.feature_image = feature_image;
  this.addProdBtn = false;
  this.editProdBtn = true;
}

updateProduct(prodId: number){
  this.productService.updateProd(prodId, this.product).subscribe((response) =>{
    console.log(response);
    this.router.navigate([this.current_url]);
    this.toastr.success('Ce produit a été mis à jour avec succès!', 'Mise à jour du produit');
  });
}
}
