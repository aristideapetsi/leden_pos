import { Component,OnDestroy, OnInit, VERSION } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import Swal from 'sweetalert2';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css',
    '../../../../assets_leden/vendors/@fortawesome/fontawesome-free/css/all.min.css',
    '../../../../assets_leden/vendors/themify-icons/themify-icons.css',
    '../../../../assets_leden/vendors/line-awesome/css/line-awesome.min.css',
    '../../../../assets_leden/css/app.min.css']
})
export class LoginComponent implements OnInit {

  // Variables
  form: FormGroup;
  loading: boolean = false;
  errors:any = null;
  UserProfile: User;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private token: TokenService,
    private authState: AuthStateService,
    private toast: ToastHelper

  ) {
    this.form = fb.group({
      email: [
        '',
        [Validators.required, Validators.email]
      ],
      password: [
        '',
        Validators.required
      ]
    });
  }

  ngOnInit(): void {

  }


  /**
   * Login the user based on the form values
   */
   login(): void {
    this.loading = true;
    this.errors = false;
    // var role = this.UserProfile?.role;
    this.authService.signin(this.form.value).subscribe(
      (result) => {
        this.responseHandler(result);
        this.loading = false;
      },
      (error) => {
        this.errors = error.error;
        this.loading = false;
      },
      () => {
        this.authState.changeAuthStatus(true);
        this.form.reset();
        this.loading = false;
        this.authService.profileUser().subscribe((data: any) =>{
          this.UserProfile = data;
          var role = this.UserProfile?.role;
          if(role == 0 || role== 1 ){
            this.router.navigate(['/dashboard']);
            }
            if(this.UserProfile.role == 2 ){
                this.router.navigate(['/orders/cashiers_list']);
            }
            if(role == 3 ){
              this.router.navigate(['/orders/prepa_list']);
            }
            if(role == 4 ){
              this.router.navigate(['/dashboard_ecomm']);
            }
            if(role == 5 ){
              this.router.navigate(['/shop']);
            }
            });
      });
  }
  /**
   * Getter for the form controls
   */
  get controls() {
    return this.form.controls;
  }

    //handle response
    responseHandler(data: any){
      // console.log("Expire dans : " + data.user.role);
      this.token.handleData(data.access_token);
      this.autoLogout(data.expires_in)

    }


logout() {
  this.authState.changeAuthStatus(false);
  this.token.removeToken();
  this.router.navigate(['login']);
  this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');

}

/**
 *
 * @param expirationTime
 */
 autoLogout(expirationTime: number){
  // console.log(expirationTime);
  setTimeout(()=>{
    this.logout();
    Swal.fire({
      title: 'Session Expiré',
      text: "Vous etes déconnecté suite à l'expiration de votre session!",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Me reconnecter!',
      cancelButtonText: 'ANNULER'
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['/login']);
      }
    })

  }, expirationTime * 1000)
}

}
