import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/models/User';
import { DashStatsService } from 'src/app/services/stats_admin/dash-stats.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.css']
})
export class DashboardAdminComponent implements OnInit {

  average_order_price = 0;
  total_incomes = 0;
  total_sales = 0;
  total_users = 0;
  unique_years : any = [];
  increase_compare = 0;

  chartOrders: Chart.ChartDataSets[] = [];
  lineChartLabels: any;

  lineChartOptions: any = {
    responsive: true
  };
  lineChartLegend = true;
  lineChartType = 'line';
  inlinePlugin: any;
  textPlugin: any;

  UserProfile!: User;
  constructor(
    public authService: AuthService,
    private dashStats: DashStatsService,
    ) {
    this.authService.profileUser().subscribe((data: any) => {
      this.UserProfile = data;
      console.log(this.UserProfile)
    });
  }

  ngOnInit(): void {
    this.dashStats.getAll().subscribe((response: any) =>{
      console.log(response);
      this.average_order_price = Math.round(response.average_order_price * 1000)/1000;
      this.total_incomes = response.total_incomes;
      this.total_sales = response.total_sales;
      this.total_users = response.total_users;
      this.unique_years = (new Set(response.unique_years));
      this.increase_compare = response.increase_compare;
      console.log(this.unique_years)

    });

    this.dashStats.getOrdersStats().subscribe((response: any) => {
      console.log(response)
      // inline plugin
    this.textPlugin = [{
      id: 'textPlugin',
      beforeDraw(chart: any): any {
        const width = chart.chart.width;
        const height = chart.chart.height;
        const ctx = chart.chart.ctx;
        ctx.restore();
        const fontSize = (height / 114).toFixed(2);
        ctx.font = `${fontSize}em sans-serif`;
        ctx.textBaseline = 'middle';
        const text = 'Text Plugin';
        const textX = Math.round((width - ctx.measureText(text).width) / 2);
        const textY = height / 2;
        ctx.fillText(text, textX, textY);
        ctx.save();
      }
    }];

    this.lineChartLabels = response.chart
    this.chartOrders = response.datasets;
    console.log(this.chartOrders)
    this.inlinePlugin = this.textPlugin;
    // this.chartOrders[0].data = response.datasets.values;
    // this.lineChartLabels = response.chart.labels;
    // console.log(response.datasets[0].values)
    })

  }


  getDashboardStats(){

  }
}
