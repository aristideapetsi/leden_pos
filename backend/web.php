<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//---------------Login page / route-----------------
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//---------------Dashboard Admin / route-----------------
Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard')->middleware('role:1');
/**=============================================USERS========================================================== */
/**
 *users routes begin ---------------------------
 */
Route::group(['middleware' => ['role:1']], function() {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    //---------------Users list / route-----------------
    Route::get('/listUsers', [UsersController::class, 'listUsers'])->name('users.list');
    //---------------Add User / route-----------------
    Route::post('/addUser', [UsersController::class, 'addUser'])->name('addUser');
    //---------------Update User / route-----------------
    Route::post('/updateUser/{id}', [UsersController::class, 'updateUser'])->name('updateUser');
    //---------------Delete User / route-----------------
    Route::get('/deleteUser/{id}', [UsersController::class, 'removeUser'])->name('deleteUser');

});

/**
 *users routes end ------------------------------
 */
/**=============================================ORDERS========================================================== */
 /**
  * Orders routes begin -------------------------
  */
  Route::group(['middleware' => ['role:2']], function() {
      //orders Cashier data list in datatable
    Route::get('/orders_data', [OrderController::class, 'getOrdersData'])->name('ordersData');
    //---------------Cashier order list / route-----------------
    Route::get('/order_cashier_list', [OrderController::class, 'orderListCashier'])->name('orderListCashier');
    //---------------Cashier order STAT list / route-----------------
    Route::get('/getOrderStatCashier', [OrderController::class, 'getOrdersWooCashier'])->name('orderStatsCashier');

    //------------------deliver order / route---------------
    Route::post('/deliverOrder/{order_id}', [OrderController::class, 'deliveryOrder'])->name('deliveryOrder');

  });
    //-----------------collect cash by cashier / route----------------
  Route::post('/collectCash/{id}', [OrderController::class, 'collectCashByCashier'])->name('collectCashByCashier')->middleware('role:2');
   //------------------get products by order / route-------------------
   Route::get('/getProductsByOrder/{order_id}', [OrderController::class, 'getProductsByOrder'])->name('getProductsByOrder');

  Route::group(['middleware' => ['role:3']], function() {
      //orders Preparator data list in datatable
    Route::get('/orders_prep_data', [OrderController::class, 'getOrdersDataPrep'])->name('orders_prep_data');
    //---------------Preparator order list / route-----------------
    Route::get('/order_list_prep', [OrderController::class, 'orderListPrep'])->name('orderListPrep');
    //---------------Preparator order STAT list / route-----------------
    Route::get('/getOrderStatPrep', [OrderController::class, 'getOrdersWooPrep'])->name('orderStatsPrep');
    //------------------Validate Preparator order / route--------------------------
    Route::post('/validateOrderPrep/{order_id}', [OrderController::class, 'validateOrder'])->name('validateOrder');


    //------------------Preparator empty products / route ---------------
    Route::post('/emptyProducts/', [OrderController::class, 'EmptyProductStock'])->name('emptyStockProd');
    //manage product stock
    Route::get('/manageProdsStock', [OrderController::class, 'manageStockProds'])->name('manageStock');
    //get Products List
    Route::get('/getProductsList', [OrderController::class, 'getProductsList'])->name('getProductsList');
    //update stock
    Route::post('/updateStock/{id}', [OrderController::class, 'updateStock'])->name('updateStock');
    //update sigle prod stock
    Route::post('/updateProdStock/{id}', [OrderController::class, 'updateStockProd'])->name('updateStockProd');

  });
      //------------------order list stats / route------------
      Route::get('/orderStats', [OrderController::class, 'orderStats'])->name('orderStats')->middleware('role:1');
      
 /**
  * Orders routes end -------------------------
 */
//About page --------------------------------------------------------------------------
Route::get('/about', [HomeController::class, 'about'])->name('about');
//Auth::routes();
Route::get('/gen-key', function(){
    $exitCode = Artisan::call('key:generate');
    return 'DONE'; //Return anything
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('key:generate');
    return 'DONE'; //Return anything
});


