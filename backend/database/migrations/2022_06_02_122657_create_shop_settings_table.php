<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('shop_id');
            $table->text('shop_logo');
            $table->string('contacts');
            $table->string('email');
            $table->string('address');
            $table->string('description');
            $table->text('main_banner1');
            $table->text('main_banner2');
            $table->text('main_banner3');
            $table->string('text_banner1');
            $table->string('text_banner2');
            $table->string('text_banner3');
            $table->string('shop_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_settings');
    }
}
