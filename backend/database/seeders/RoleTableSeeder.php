<?php

namespace Database\Seeders;

use App\Models\role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = new role();
        $admin_role->name = 'admin';
        $admin_role->description = 'admin role';
        $admin_role->save();

        $role_cashier = new Role();
        $role_cashier->name = 'caissier';
        $role_cashier->description = 'cashier role';
        $role_cashier->save();

        $role_preparator = new Role();
        $role_preparator->name = 'preparator';
        $role_preparator->description = 'preparator role';
        $role_preparator->save();
    }
}
