<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Back_office\CategoryProdController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\OrderController;
use App\Http\Controllers\Frontend\ProductCategoryController;
use App\Http\Controllers\Frontend\ShopController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api'

], function ($router) {
    Route::post('/login', [App\Http\Controllers\AuthController::class, 'login']);
    Route::post('/register', [App\Http\Controllers\AuthController::class, 'register']);
    Route::post('/logout', [App\Http\Controllers\AuthController::class, 'logout']);
    Route::get('/refresh', [App\Http\Controllers\AuthController::class, 'refresh'])->name('refresh');
    Route::get('/user-profile', [App\Http\Controllers\AuthController::class, 'userProfile'])->name('userProfile');
    Route::post('/sendPasswordResetLink', [AuthController::class, 'sendPasswordResetLink']);
    Route::post('/signup', [App\Http\Controllers\AuthController::class, 'signup']);

});

Route::group([
    'middleware' => 'api',
    'prefix' => 'users'
], function(){
    Route::get('/getAllUsers', [App\Http\Controllers\UsersController::class, 'listUsers']);
    Route::post('/addUser', [App\Http\Controllers\UsersController::class, 'addUser']);
    Route::delete('/removeUser/{id}', [App\Http\Controllers\UsersController::class, 'removeUser']);
    Route::put('/updateUser/{id}', [App\Http\Controllers\UsersController::class, 'updateUser']);

    //Frontend
    Route::post('/updateAddressCustomer', [UsersController::class, 'updateAddressCustomer']);
    Route::post('/updateAccountInfo', [UsersController::class, 'updateAccountInfo']);
    Route::post('/updateAdminPass', [UsersController::class, 'updateAdminPass']);
    Route::post('/updateAdminAccountInfo', [UsersController::class, 'updateAdminAccountInfo']);
    Route::post('/updateProfileAvatar', [UsersController::class, 'updateProfileAvatar']);

});
/**
 * Orders routes
 */
Route::group([
    'middleware' => 'api',
    'prefix' => 'orders'
], function(){
    //get all orders
    Route::get('/getAllOrders', [App\Http\Controllers\OrderController::class, 'orderStats']);
    //get cashiers orders
    Route::get('/getOrdersCashier', [App\Http\Controllers\OrderController::class, 'getOrdersCashier']);
    //get cashiers orders for datatable
    Route::get('/getOrdersCashierData', [App\Http\Controllers\OrderController::class, 'getOrdersDataCashiers']);
    //get preparator orders
    Route::get('/getOrdersPrepa', [App\Http\Controllers\OrderController::class, 'getOrdersPrep']);
    //get cashiers orders for datatable
    Route::get('/getOrdersPrepaData', [App\Http\Controllers\OrderController::class, 'getOrdersDataPrep']);
    //validate order by preparator
    Route::post('/validateOrderByPrepa/{order_id}', [App\Http\Controllers\OrderController::class, 'validateOrder']);
    //cash money by cashier
    Route::post('/cashMoneyByCashier/{order_id}', [App\Http\Controllers\OrderController::class, 'CollectCashByCashier']);
     //delivery order by cashier
     Route::post('/deliveryOrderByCashier/{order_id}', [App\Http\Controllers\OrderController::class, 'deliveryOrder']);

    Route::get('/cancelOrder/{order_id}', [App\Http\Controllers\OrderController::class, 'cancelOrder']);
     //Frontend
     Route::get('/orderByCustomer/', [OrderController::class, 'getOrderByCustomer']);
     Route::get('/getorderDetail/{id}', [OrderController::class, 'getorderDetail']);
});

//Category products routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'categories'
],function(){
    Route::get('/getAllCategories', [CategoryProdController::class, 'listCategoryProds']);
    Route::post('/addCategoryProd', [CategoryProdController::class, 'addCategory']);
    Route::get('/removeCategoryProd/{id}', [CategoryProdController::class, 'removeCategory']);
    Route::post('/updateCategoryProd/{id}', [CategoryProdController::class, 'updateCategory']);
});
//products routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'products'
], function(){
    //Add Product
    Route::post('/addProduct', [ProductController::class, 'addProduct']);
    //remove product
    Route::get('/removeProduct/{id}', [ProductController::class, 'removeProduct']);
    //update product info
    Route::post('/updateProduct/{id}', [ProductController::class, 'updateProduct']);

    //get products list
    Route::get('/getAllProducts', [App\Http\Controllers\ProductController::class, 'getProductsList']);
    //empty product stock
    Route::post('/emptyProductStock', [App\Http\Controllers\ProductController::class, 'emptyProductStock']);
    //update stock product
    Route::post('/updateStockProd/{id}/stock/{stock}', [App\Http\Controllers\ProductController::class, 'updateStockProd']);

});

Route::group([
    'middleware' => 'api',
    'prefix' => 'statistics'
], function(){
    Route::get('/getOrdersStats', [App\Http\Controllers\StatisticsController::class, 'getProductsList']);
    Route::get('/dashboard', [App\Http\Controllers\StatisticsController::class, 'dashboard']);
    Route::get('/getOverviewStats', [App\Http\Controllers\StatisticsController::class, 'getOverviewStats']);

    // Route::post('/emptyProductStock', [App\Http\Controllers\ProductController::class, 'EmptyProductStock']);
});
//shop Frontend
Route::group([
    'middleware' => 'api',
    'prefix' => 'shop'
], function(){
    Route::get('/products', [ShopController::class, 'getAllProducts']);
    Route::post('/filterProductByCategory/', [ShopController::class, 'filterProductByCategory']);
    Route::get('/getMostPopularProducts', [ShopController::class, 'getMostPopularProducts']);
    Route::get('/getPopularProducts', [ShopController::class, 'getPopularProducts']);
    Route::get('/getProductsFromLowToCost', [ShopController::class, 'getProductsFromLowToCost']);
    Route::get('/getProductsFromCostToLow', [ShopController::class, 'getProductsFromCostToLow']);
    Route::post('/filterProductByPriceRange', [ShopController::class, 'filterProductByPriceRange']);
    //get details of product
    Route::get('/productDetail/{id}', [ShopController::class, 'getProductDetail']);
    //get related product
    Route::get('/relatedProduct/{idCat}', [ShopController::class, 'getRelatedProduct']);
    //Search product(s)
    Route::post('/searchProducts/', [ShopController::class, 'searchProducts']);
    //get All Product route
    Route::get('/getAllCategories', [App\Http\Controllers\Frontend\ProductCategoryController::class, 'getAllCategories']);
    //get Category info
    Route::get('/getCategoryInfo/{id}', [ProductCategoryController::class, 'getCategoryInfo']);
    //like product route
    Route::get('/likeProduct/{idProd}', [ShopController::class, 'addLikeToProduct']);
    //get liked products by user
    Route::get('/likedProducts', [ShopController::class, 'getProductsLiked']);

});
//Cart Frontend
Route::group([
    'middleware' => 'api',
    'prefix' => 'cart'
], function(){
    //add product to cart
    Route::post('/addToCart/{id}/prod_qty/{prod_qty}', [CartController::class, 'addProductToCart']);
    Route::get('/getCartContent', [CartController::class, 'getCartContent']);
    Route::post('/removeItemToCart', [CartController::class, 'removeItemToCart']);


});

/**
 * Checkout routes
 */
Route::group([
    'middleware' => 'api',
    'prefix' => 'checkout'
], function(){
    //add product to cart
    Route::post('/place-order', [CheckoutController::class, 'placeOrder']);
    // Route::get('/getCartContent', [CartController::class, 'getCartContent']);
    // Route::post('/removeItemToCart', [CartController::class, 'removeItemToCart']);
});

//Coupon Backend
Route::group([
    'middleware' => 'api',
    'prefix' => 'coupons'
], function(){
    //add product to cart
    Route::post('/updateCoupon', [CouponController::class, 'updateCoupon']);
    Route::post('/addCoupon', [CouponController::class, 'storeCoupon']);
    Route::get('/deleteCoupon/{id}', [CouponController::class, 'deleteCoupon']);
    Route::get('/getAllCoupons', [CouponController::class, 'getAllCoupons']);
});

//notifications
Route::group([
    'middleware' => 'api',
    'prefix' => 'notifications'
], function(){
    Route::get('/getOrdersUpdateNotifications/', [NotificationController::class, 'getOrdersUpdateNotifications']);
    Route::get('/getAllOrderNotifications/', [NotificationController::class, 'getAllOrderNotifications']);
    Route::get('/getOrderUnreadNotifications/', [NotificationController::class, 'getOrderUnreadNotifications']);
    Route::get('/markNotifAsRead/{notif_id}', [NotificationController::class, 'markNotifAsRead']);

    //customer notiifcations
    Route::get('/getCustomerUnreadNotif/{id_customer}', [NotificationController::class, 'getCustomerUnreadNotif']);
    Route::get('/getAllNotifForCustomer/{id_customer}', [NotificationController::class, 'getAllNotifForCustomer']);


    // Route::get('/getAllCoupons', [CouponController::class, 'getAllCoupons']);
});
