<script>
    $(document).ready(function(){
        var ordersTable = $('#table_orders');
        var seeMoreBtn = $('#see_more');
        var see_more_hide_btn = $('#see_more_hide');

        // ordersTable.hide();

        seeMoreBtn.on('click', function(){
            ordersTable.show();
            see_more_hide_btn.show();
            seeMoreBtn.hide();
        });
        see_more_hide_btn.on('click', function(){
            ordersTable.hide();
            see_more_hide_btn.hide();
            seeMoreBtn.show();
        })

        var table = $('#ordersT').DataTable({
                ajax: '/orders_prep_data',
                serverSide: true,
                processing: true,
                aaSorting:[[0,"desc"]],
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'num_items_sold', name: 'num_items_sold'},
                    {data: 'status', name: 'status'},
                    {data: 'net_total', name: 'net_total'},
                    {data: 'date_created_gmt', name: 'date_created_gmt'},
                    {data: 'action', name: 'action'},
                ]
            });
    })
</script>
