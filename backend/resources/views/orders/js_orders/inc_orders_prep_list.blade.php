<style>
    .overlay-img{
        background-color:#000; opacity:.6;
    }
</style>
<script>
    $(document).ready(function(){
        var orderBtn = $('.orderBtn');

        var orders = $("#orders");

        /**@
         *loader orders begin======================================================
         */
        function loader(){
            $('button').prop('disabled', true)
            $('#orders').append('<div style="" id="loadingDiv"><div class="loader"><span style="color: red">Chargement Commandes...</span></div></div>');
        }

        function removeLoader(){
            $( "#loadingDiv" ).fadeOut(500, function() {
                // fadeOut complete. Remove the loading div
                $('button').prop('disabled', false)
                $( "#loadingDiv" ).remove(); //makes page more lightweight
            });
        }
        /**@
         *loader orders end=====================================================================
         */
        let getOrders = function (){
            loader();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: "/getOrderStatPrep",
                success: function (data) {
                    console.log(data.orders);
                    if(data.orders.length > 0){

                        for(var i = 0 ; i < data.orders.length; i++){
                            var date_created_gmt = new Date(data.orders[i].date_created_gmt);
                            var hour = date_created_gmt.getHours();
                            var min = date_created_gmt.getMinutes();
                            var productsOr = [];
                            $('#message').html("");
                            productsOr = getProductionsByOrder(data.orders[i].id);
                            orders.append(`<div class="col col-md-4 orderItem"> <div class="card" style="border: 1px solid black">
                                            <div class="card-cup bg-warning text-white justify-content-center">
                                                <h5 class="mb-0"> N* ${data.orders[i].id}</h5>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title text-dark font-weight-bold">Détails de la commande</h5>
                                                <table >
                                                    <tbody style="display: block;height:100px; overflow-y: auto; ">
                                                        ${data.orders[i].line_items.map(prod => `<tr><td>${prod.name} :</td><td> <span class="text-white badge badge-dark"> X ${prod.quantity} </span></td>`).join('\n      ')}



                                                    <tbody>
                                                </table>
                                                <div class="text-muted font-13">Commandé à : ${hour} h : ${min}</div>
                                                <hr>
                                                <div class="flexbox">
                                                    <button  data-toggle="modal"  data-order="${data.orders[i].id}" data-target="#OrderDone" data-whatever="@getbootstrap" class="btn btn-warning orderShow" href="#"> <i class="fa fa-eye text-white"></i></button>
                                                    <div><button type="button"  class="btn btn-success orderBtn" data-id="${data.orders[i].id}" >Terminé <i class="fa fa-check"></i></button></div>
                                                </div>
                                            </div>
                                    </div></div>`);
                                    removeLoader()
                        }
                    }else{
                        $('#message').html("Aucune commande disponible actuellement !!!").addClass('text-center');
                        removeLoader()
                    }
                    //cancel some products
                    $(".orderShow").on('click', function(){
                        productsNA = [];
                        var orderC = $(this).data('order')
                        var productsOrC = [];
                        var productImg = [];
                        console.log("commande : " + orderC)
                        $('.orderID').html(orderC)
                        $('.orderTr').remove();
                        productsOrC = getProductionsByOrder(orderC);
                        var products = productsOrC.products;
                        var images = productsOrC.images;
                        console.log(images)
                        $('.productsC').remove()

                        for(var i = 0; i < products.length, images.length; i++){
                            $('.productsCheck').append(`<div class="col col-md-6 ">
                                <div class="shadow card productsC productsCard-${products[i].product_id}" id="order1" style="border: 1px solid orange">
                                    <div class="card-body">

                                        <div class="flexbox">
                                            <img  class="rounded shadow" src="${images[i] == null ? "/assets_leden/img/default.png" : images[i].guid}" alt="image"  width="80" height="60"/>
                                        </div>
                                        <hr/>
                                        <h6 class="text-center prodTitle-${products[i].product_id}"><b><span class="qty-${products[i].product_id} text-white badge badge-dark" > X ${products[i].product_qty} </span> ${products[i].post_title} <b></h6>
                                    </div>
                                </div>
                                </div>`)


                            // $('.productsCheck').append(`<tr class="orderTr"><td><label>${productsOrC[i].post_title} :</label></td><td></td><td > <input style="margin-right: 20%" type="checkbox" name="products_selected[]" class="products_selected"><td>`);
                        }

                    });

                    // //strikethrough
                    // input[type=checkbox]:checked + label {
                    // color:red;
                    // text-decoration: line-through;
                    // }
                    $('.orderBtn').on('click', function(){
                        var id = $(this).data("id");

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: "POST",
                            url: "/validateOrderPrep/" + id,
                            success: function(){
                                Command: toastr["success"](`Commande N* ${id}`, "Commande terminé avec succès!!")
                                    toastr.options = {
                                        "closeButton": true,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": true,
                                        "positionClass": "toast-top-right",
                                        "preventDuplicates": true,
                                        "onclick": null,
                                        "showDuration": "300",
                                        "hideDuration": "1000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    }
                                    // $( "#orders" ).load(window.location.href + " #orders" );
                                    $( ".orderItem" ).remove()
                                    getOrders();
                                    console.log('page refresh')

                            }
                        })

                    });


                }
             }); //end ajax
             $('#orderBtn').on('click', function(){
                 console.log("id =  " + $("#orderBtn").data("id"));

            });
        }

        getOrders();
        //-----------------------------------------------------------------------------
        //--------------------------verify while page idle refresh page begin-----------------
        //-----------------------------------------------------------------------------

        //Increment the idle time counter every minute
        var idleTime = 0;
        var idleInteral = setInterval(timerIncrement, 60000) //1min
        //zero the idle timer on mouse movement
        $(this).mousemove(function(e){
            idleTime = 0;
        });
        $(this).keypress(function(e){
            idleTime = 0;
        });
        function timerIncrement(){
            idleTime += 1;
            if(idleTime > 2){
                $( ".orderItem" ).remove()
                // $( "#orders" ).load(window.location.href + " #orders" );
                getOrders();
            }
        }
        //-----------------------------------------------------------------------------
        //--------------------------verify while page idle refresh page end-----------------
        //-----------------------------------------------------------------------------

        //reload orders
        $('.btn-reload-orders').on('click', function(){
            $( ".orderItem" ).remove()
            getOrders();
            //$( "#orders" ).load(window.location.href + " #orders" );
        })

        /**
         *
         * function getProductionsByOrder
         */
        var getProductionsByOrder = function (id){
            var products = [];
             $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: '/getProductsByOrder/' + id,
                async: false,
                success: function(data){
                     products =  {"products": data.products, "images": data.products_images};
                     console.log(products)
                }
            });
            return products;
        }


    })

</script>
