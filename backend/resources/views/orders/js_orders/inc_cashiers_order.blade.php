

<script>

    $(document).ready(function(){

        //-----------------------------------------------------------------------------
        //--------------------------verify while page idle refresh page begin-----------------
        //-----------------------------------------------------------------------------

        //Increment the idle time counter every minute
        var idleTime = 0;
        var idleInteral = setInterval(timerIncrement, 60000) //1min
        //zero the idle timer on mouse movement
        $(this).mousemove(function(e){
            idleTime = 0;
        });
        $(this).keypress(function(e){
            idleTime = 0;
        });
        function timerIncrement(){
            idleTime += 1;
            if(idleTime > 2){
                $( ".orderItem" ).remove();
                // $( "#orders" ).load(window.location.href + " #orders" );
                getOrders();
            }
        }
        //-----------------------------------------------------------------------------
        //--------------------------verify while page idle refresh page end-----------------
        //-----------------------------------------------------------------------------

        //reload orders
        $('.btn-reload-orders').on('click', function(){
            $( ".orderItem" ).remove();
            //$( ".orders" ).load(window.location.href + " .orders" );
            loader();
            getOrders();
        })

        var getProductionsByOrder = function (id){
            var products = [];
             $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: '/getProductsByOrder/' + id,
                async: false,
                success: function(data){

                    console.log(data)
                    // if(data.products.length > 0){
                    //     products =  data.products;
                    // }
                     products =  data.products;
                }
            });
            return products;
        }
        loader()
        function loader(){

            $('#orders').append('<div style="" id="loadingDiv"><div class="loader"><span style="color: red">Chargement Commandes...</span></div></div>');
        }

        function removeLoader(){
            $( "#loadingDiv" ).fadeOut(500, function() {
            // fadeOut complete. Remove the loading div
            $( "#loadingDiv" ).remove(); //makes page more lightweight
        });
        }
        var orders = $("#orders");
        getOrders();
        
        /**
         *Get orders from database for cashiers
         */
        function getOrders() {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: "/getOrderStatCashier",
                success: function (data) {
                    var total_orders = data.orders.length;
                    // console.log(data.orders);


                    if(data.orders.length > 0){
                        for(var i = 0 ; i < total_orders; i++){
                        var status_order = "";
                        var status = "";
                        var date_created_gmt = new Date(data.orders[i].date_created_gmt);
                        var hour = date_created_gmt.getHours();
                        var min = date_created_gmt.getMinutes();

                        var productsOr = [];
                        productsOr = getProductionsByOrder(data.orders[i].order_id);
                        console.log(productsOr);
                        $('#message').html("");
                        if(data.orders[i].status == "completed"){
                            status_order = "<span class='badge badge-success'>Livré</span>";
                            status = "completed";
                        }else if(data.orders[i].status == "on-hold"){
                            status_order = "<span class='badge badge-secondary'>En Attente ...</span>";
                            status = "hold-on";
                        }else if(data.orders[i].status == "processing"){
                            status_order = "<span class='badge badge-secondary'>En attente paiement</span>";
                            status = "processing";
                        }else if(data.orders[i].status == "pending"){
                            status_order = "<span class='badge badge-danger'>En cours...</span>";
                            status = "pending";
                        }
                        else if(data.orders[i].status  == "wc-ready-kitcken"){
                            status_order = "<span class='badge badge-primary'>Pret...</span>";
                            status = "wc-ready-kitcken";
                        }
                        orders.append(`<div class="col col-md-4 orderItem"><div class="card" id="order1" style="border: 1px solid black">
                                        <div class="card-cup bg-warning text-white justify-content-center">
                                            <h5 class="mb-0"> N* ${data.orders[i].id} </h5>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title text-dark font-weight-bold">Détails de la commande </h5>
                                            <table >
                                                <tbody style="display: block;height:100px; overflow-y: auto; ">
                                                        ${data.orders[i].line_items.map(prod => `<tr><td>${prod.name} :</td><td> <span class="text-white badge badge-dark"> ${(parseInt(prod.subtotal_tax) + parseInt(prod.subtotal)) / prod.quantity } X ${prod.quantity} </span></td>`).join('\n      ')}
                                                <tbody>
                                            </table>
                                            <div class="text-muted font-13">Total : <span style='color: red'>${data.orders[i].total}</span> ${data.orders[i].currency} / ${data.orders[i].payment_method_title}</div>
                                            <div class="text-muted font-13">Commandé à : ${hour} h : ${min}</div>
                                            <div class="text-muted font-13">Statut Cuisine :  ${status_order}</div>
                                            <hr>
                                            <div class="flexbox"><button data-id="${data.orders[i].id}" data-target="#OrderDone" data-whatever="@getbootstrap" class="btn btn-danger collectCash" ${status == "processing"  ? "" : "disabled = 'disabled'"} href="#"> <i class="fa fa-check"></i>${status == "processing" ? " Encaisser" : " Déjà Encaissé"}</button>
                                                <div><button class="btn btn-success deliverOrder" data-id="${data.orders[i].id}" id="orderDone" type="button" ${status == "completed" ? "disabled = 'disabled'" : ""} ><i class="fa fa-check"></i>${status != "completed" ? " Livrer" : " Déjà Livrer"} </button></div>
                                            </div>
                                        </div>
                                    </div></div>`)

                        }
                        removeLoader()
                        // setTimeout(removeLoader, 2500);
                    }else{
                        $('#message').html("Aucune commande disponible actuellement !!!").addClass('text-center');
                        removeLoader()
                    }
                             /**
                             * Collect Cash function
                             */
                            $('.collectCash').on('click', function(){
                                var idC = $(this).data("id");
                                // alert(idC)
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: "POST",
                                    url: "/collectCash/" + idC,
                                    success: function(){
                                        Command: toastr["success"](`Commande ${idC}`, "Commande Encaissé avec succès!!")

                                        toastr.options = {
                                            "closeButton": true,
                                            "debug": false,
                                            "newestOnTop": false,
                                            "progressBar": true,
                                            "positionClass": "toast-top-right",
                                            "preventDuplicates": true,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "5000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "swing",
                                            "hideEasing": "linear",
                                            "showMethod": "fadeIn",
                                            "hideMethod": "fadeOut"
                                        }
                                        // $( ".orders" ).load(window.location.href + " .orders" );
                                        $( ".orderItem" ).remove();
                                        getOrders();
                                        // console.log('page refresh')
                                    }
                                });
                            });//done collect cash

                            /**
                             * Deliver Order function
                             */
                             $('.deliverOrder').on('click', function(){
                                var idD = $(this).data("id");
                                console.log("order id: " + idD)
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: "POST",
                                    url: "/validateOrderPrep/" + idD,
                                    success: function(){
                                        Command: toastr["success"](`Commande ${idD}`, "Commande Livré avec succès!!")

                                        toastr.options = {
                                            "closeButton": true,
                                            "debug": false,
                                            "newestOnTop": false,
                                            "progressBar": true,
                                            "positionClass": "toast-top-right",
                                            "preventDuplicates": true,
                                            "onclick": null,
                                            "showDuration": "300",
                                            "hideDuration": "1000",
                                            "timeOut": "5000",
                                            "extendedTimeOut": "1000",
                                            "showEasing": "swing",
                                            "hideEasing": "linear",
                                            "showMethod": "fadeIn",
                                            "hideMethod": "fadeOut"
                                        }
                                        // $( ".orders" ).load(window.location.href + " .orders" );
                                        $('.orderItem').remove();
                                        getOrders();
                                        // console.log('page refresh')
                                    }
                                });
                            });//done collect cash
                }
             }); //end ajax

        }


    })
</script>
