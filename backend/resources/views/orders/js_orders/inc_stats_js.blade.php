<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        //$.noConflict();
        var token = ''
        var modal = $('.modal');
        var form = $('.form');

        var table = $('#orders').DataTable({
                ajax: '{{ route('orderStats') }}',
                serverSide: true,
                processing: true,
                aaSorting:[[0,"desc"]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'total', name: 'total'},
                    {data: 'payment_method_title', name: 'payment_method_title'},
                    {data: 'status', name: 'status'},
                    {data: 'date_created_gmt', name: 'date_created_gmt'},
                ]
            });

    })
</script>
