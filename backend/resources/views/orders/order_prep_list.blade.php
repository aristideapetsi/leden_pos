@extends('layouts.app')
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous"> --}}
@section('title')Commandes Prépa @endsection
@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" /><!-- THEME STYLES-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<div class="page-wrapper">
    <div class="content-wrapper">
        <!-- BEGIN: Sidebar-->
        @include('layouts.left_sidebar')
        <!-- END: Sidebar-->
        <!-- BEGIN: Content-->
        <div class="content-area">
            <!-- BEGIN: Header-->
            @include("layouts.top_sidebar")
            <!-- END: Header-->
            <div class="page-content fade-in-up">
                <!-- BEGIN: Page heading-->
                <div class="page-heading">
                    <h1 class="page-title page-title-sep">Commandes / Préparateur</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="la la-home font-20"></i></a></li>
                        {{-- <li class="breadcrumb-item">Commandes</li>
                        <li class="breadcrumb-item">Liste Préparateurs</li> --}}
                        <div>
                            <a href="/manageProdsStock" class="btn btn-warning text-white">Gérer Stock <i class="fa fa-cogs"></i></a>

                            <button class="btn btn-success btn-reload-orders">Rafraichir Commandes <i class="fa fa-spinner"></i></button>
                        </div>

                    </ol>
                </div><!-- BEGIN: Page content-->
                <div>


                    <div class="card">
                        <div class="card-body">
                            <h5 class="box-title">Commandes</h5>
                            <div class="flexbox mb-4">

                            </div>
                            <div class="row" id="orders">
                                <span id="message" class="text-center"></span>

                            </div>

                            <button type="button" class="btn btn-warning text-white collapse" id="see_more">Commandes non traitées <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-danger text-white " id="see_more_hide">Cacher table <i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive " id="table_orders">
                                <span id="addSuccess"></span>
                                <table class="table  table-bordered table-checkable" id="ordersT">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>#ID Commande</th>
                                            <th>Statut </th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div><!-- END: Page content-->
            </div><!-- BEGIN: Footer-->
            @include('layouts.footer')
            <!-- END: Footer-->
        </div><!-- END: Content-->
    </div>
</div>

</div><!-- END: Quick sidebar-->

<!-- Modal add User-->

<div class="modal fade" id="OrderDone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: 1px solid orange">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Liste des Produits à préparer / Commande N* <span class="orderID"> </span> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">

            <span class="text-center">Détails de la commande</span><br/>
            <hr width="100"/>
            <form action="javascript:;" class="form">

                <div class="row productsCheck" style="height: 265px; overflow-y: scroll">
                    {{-- <table style="margin-left: 35%" >
                        <tbody class="productsCheck  justify-content-center" style=""></tbody>
                    </table> --}}
                </div>

                  {{-- <button type="button" class="btn btn-success btn-save" onclick="emptyProds()">Enrégistrer <i class="fa fa-save"></i></button>
                  <button type="button" class="btn btn-warning text-white" id="resetProd">Réinitialiser sel. <i class="fa fa-refresh"></i></button> --}}
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer <i class="fa fa-times"></i></button>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal add/edit user-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


{{-- <script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script> --}}
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
{{-- <script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS--> --}}

<script src="{{ asset('assets_leden/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
{{-- <script src="{{ asset('assets_leden/vendors/chart.js/dist/Chart.min.js') }}"></script><!-- CORE SCRIPTS--> --}}

<!-- Bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
{{-- begin Datatable links --}}
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
{{-- end Datatable links --}}

<script src="{{ asset('assets_leden/js/app.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('orders.js_orders.inc_orders_prep_datatable')
@include('orders.js_orders.inc_orders_prep_list')

@endsection
