@extends('layouts.app')

<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

@section('title') Historique Commandes @endsection
@section('content')
<div class="page-wrapper">
    <div class="content-wrapper">
        <!-- BEGIN: Sidebar-->
        @include('layouts.left_sidebar')
        <!-- END: Sidebar-->
        <!-- BEGIN: Content-->
        <div class="content-area">
            <!-- BEGIN: Header-->
            @include("layouts.top_sidebar")
            <!-- END: Header-->
            <div class="page-content fade-in-up">
                <!-- BEGIN: Page heading-->
                <div class="page-heading">
                    <h1 class="page-title page-title-sep">Commandes</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="la la-home font-20"></i></a></li>
                        <li class="breadcrumb-item">Commandes</li>
                        <li class="breadcrumb-item">Stats</li>
                    </ol>
                </div><!-- BEGIN: Page content-->
                <div>


                    <div class="card">
                        <div class="card-body">
                            <h5 class="box-title">Historique des Commandes</h5>
                            <div class="flexbox mb-4">

                            </div>
                            <div class="">
                                <span id="addSuccess"></span>
                                <table class="table table-bordered data-table" id="orders">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>#ID Com.</th>
                                            <th>Total Net (FCFA)</th>
                                            <th>Mode Paiement</th>
                                            <th>Statut</th>
                                            <th>Date</th>
                                            {{-- <th class="no-sort"></th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div><!-- END: Page content-->
            </div><!-- BEGIN: Footer-->
            @include('layouts.footer')
            <!-- END: Footer-->
        </div><!-- END: Content-->
    </div>
</div>

</div><!-- END: Quick sidebar-->



{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> --}}


<script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
{{-- <script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS--> --}}

<script src="{{ asset('assets_leden/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
{{-- <script src="{{ asset('assets_leden/vendors/chart.js/dist/Chart.min.js') }}"></script><!-- CORE SCRIPTS--> --}}
<script src="{{ asset('assets_leden/js/app.min.js') }}"></script><!-- PAGE LEVEL SCRIPTS-->
<!-- Bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

{{-- begin Datatable links --}}
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
{{-- end Datatable links --}}

{{-- <script src="{{ asset('assets_leden/js/app.min.js') }}"></script> --}}

@include('orders.js_orders.inc_stats_js')
@endsection
