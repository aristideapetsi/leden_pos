@extends('layouts.app')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" /><!-- THEME STYLES-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@section('title')A propos @endsection
@section('content')
<div class="page-wrapper">
    <div class="content-wrapper">
        <!-- BEGIN: Sidebar-->
        @include('layouts.left_sidebar')
        <!-- END: Sidebar-->
        <!-- BEGIN: Content-->
        <div class="content-area">
            <!-- BEGIN: Header-->
            @include("layouts.top_sidebar")
            <!-- END: Header-->
            <div class="page-content fade-in-up">
                <!-- BEGIN: Page heading-->
                <div class="page-heading">
                    <h1 class="page-title page-title-sep">A propos</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="la la-home font-20"></i></a></li>
                        {{-- <li class="breadcrumb-item">Produits</li>
                        <li class="breadcrumb-item">Gestion Stock</li> --}}
                    </ol>
                </div><!-- BEGIN: Page content-->
                <div>

                <div class="card">
                    <div class="card-body">
                        {{-- <h5 class="box-title">A propos</h5> --}}
                        <div class="flexbox mb-4">

                        </div>
                        <div class="">
                            <h1 class="text-center">LEDEN <span class="text-success">P</span><span class="text-danger">O</span><span class="text-warning">S</span></h1>
                            <br/>
                            <p class="text-center" style="color: silver">Version 1.0.0</p>
                            <p class="text-center" style="color: silver">from</p>
                            <p class="text-center"><img src="{{ asset('assets_leden/img/logo2.png') }}" height="50" alt=""></p>
                            <br/>
                        </div>

                        <br/>

                    </div>
                </div>


                </div><!-- END: Page content-->
            </div><!-- BEGIN: Footer-->
            @include('layouts.footer')
            <!-- END: Footer-->
        </div><!-- END: Content-->
    </div>
</div>

</div><!-- END: Quick sidebar-->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


{{-- <script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script> --}}
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
{{-- <script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS--> --}}

<script src="{{ asset('assets_leden/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
{{-- <script src="{{ asset('assets_leden/vendors/chart.js/dist/Chart.min.js') }}"></script><!-- CORE SCRIPTS--> --}}

<!-- Bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

{{-- begin Datatable links --}}
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
{{-- end Datatable links --}}

<script src="{{ asset('assets_leden/js/app.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@endsection
