@extends('layouts.app')
@section('title') Tableau de bord @endsection
@section('content')

<div class="page-wrapper">
    <div class="content-wrapper">
        <!-- BEGIN: Sidebar-->
        @include('layouts.left_sidebar')
        <!-- END: Sidebar-->
        <!-- BEGIN: Content-->
        <div class="content-area">
            <!-- BEGIN: Header-->
            @include("layouts.top_sidebar")
            <div class="page-content fade-in-up">
                <!-- BEGIN: Page heading-->
                <div class="page-heading">
                    <h1 class="page-title page-title-sep">Tableau de bord </h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="la la-home font-20"></i></a></li>
                        <li class="breadcrumb-item">Tableau de bord</li>

                    </ol>
                </div><!-- BEGIN: Page content-->
                <div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card card-fullheight">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <h5 class="box-title mb-2">Graphe des Commandes</h5>
                                            <div class="text-muted font-13">Analyse des commandes mensuelles</div>
                                        </div>
                                        <ul class="nav nav-pills nav-pills-links d-none d-sm-flex">
                                            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#tab-3">Choisir Année
                                                <select id="select-year" >
                                                @foreach ($unique_years as $year)
                                                    <option value="{{ $year }}" class="year">{{ $year}}</option>
                                                @endforeach
                                            </select></a></li>
                                        </ul>
                                    </div>
                                    <div id="chart" style="height: 280px"></div>
                                    <div class="flexbox flex-wrap mt-5">
                                        <div class="d-flex mb-4 mb-sm-0">
                                            <div class="pr-4 pr-sm-5">
                                                <h6 class="mb-2 font-15 text-muted">Utilisateurs</h6>
                                                <div class="h4 mb-0 text-primary">{{ $total_users }}</div>
                                            </div>
                                            <div class="pl-0 pl-sm-5">
                                                <h6 class="mb-2 font-15 text-muted">Total Ventes</h6>
                                                <div class="h4 mb-0 text-danger">+XOF {{ intval($total_sales) }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body text-white" style="background-image: linear-gradient(45deg,#f39c12 0,#e91e63 100%);">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <h5 class="box-title mb-2">Commandes</h5>
                                            {{-- <div class="text-light font-13">From 14.12 - 21.12</div> --}}
                                        </div><i class="ti-shopping-cart text-white-50 font-40"></i>
                                    </div>
                                    <div class="flexbox mb-2">
                                        <div class="h1 mb-0">+ {{ intval($average_order_price) }} XOF</div>
                                    </div>
                                    <div class="text-white">Valeur moyenne des commandes - XOF {{ intval($average_order_price) }}</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <h5 class="box-title mb-2">Revenus</h5>
                                            {{-- <div class="text-muted font-13">Du 14.12 - 21.12</div> --}}
                                        </div><a class="text-muted" href="#"><i class="ti-more-alt"></i></a>
                                    </div>
                                    <div class="flexbox mb-2">
                                        <div class="h1 mb-0">XOF {{ $total_incomes }}</div>
                                    </div>
                                    <div class="text-muted">+ {{ $increase_compare * 100 }}% de commandes plus que la semaine dernière</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="shadow card col-lg-4 rounded-0 border-right">
                            <div class="card-body text-center">
                                <div class="mb-4 pb-3"><i class="ti-briefcase text-muted font-40"></i></div>
                                <h5 class="mb-3">Gérer Utilisateurs</h5>
                                <p>Avoir une vue d'ensemble des utilisateurs et les gérer.</p>
                                <div><a class="btn btn-warning shadow d-inline-flex align-items-center text-white" href="{{ route('users.index') }}"><span class="mr-2">Consulter</span><i class="fas fa-angle-right font-16"></i></a></div>
                            </div>
                        </div>
                        <div class="shadow card col-lg-4 rounded-0 border-right">
                            <div class="card-body text-center">
                                <div class="mb-4 pb-3"><i class="ti-announcement text-muted font-40"></i></div>
                                <h5 class="mb-3">Gérer Commandes</h5>
                                <p>Consulter les dernières commandes </p>
                                <div><a class="btn btn-warning shadow d-inline-flex align-items-center text-white" href="{{ route('orderListCashier') }}"><span class="mr-2">Consulter</span><i class="fas fa-angle-right font-16"></i></a></div>
                            </div>
                        </div>
                        <div class="shadow card col-lg-4 rounded-0">
                            <div class="card-body text-center">
                                <div class="mb-4 pb-3"><i class="ti-world text-muted font-40"></i></div>
                                <h5 class="mb-3">Gérer Stock</h5>
                                <p>Controler le stock de ses produits en quelques minutes.</p>
                                <div><a class="btn btn-warning shadow d-inline-flex align-items-center text-white" href="{{ route('manageStock') }}"><span class="mr-2">Consulter</span><i class="fas fa-angle-right font-16"></i></a></div>
                            </div>
                        </div>
                    </div>



                </div><!-- END: Page content-->
            </div><!-- BEGIN: Footer-->
            @include('layouts.footer')
           <!-- END: Footer-->
        </div><!-- END: Content-->
    </div>
</div><!-- BEGIN: Search form-->
<div class="modal fade" id="search-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin-top: 100px">
        <div class="modal-content">
            <form class="search-top-bar" action="#"><input class="form-control search-input" type="text" placeholder="Search..."><button class="reset input-search-icon" type="submit"><i class="ft-search"></i></button><button class="reset input-search-close" type="button" data-dismiss="modal"><i class="ft-x"></i></button></form>
        </div>
    </div>
</div><!-- END: Search form-->
<!-- BEGIN: Quick sidebar-->

<!-- BEGIN: Page backdrops-->
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Chargement...</div>
</div><!-- END: Page backdrops-->
<!-- CORE PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
{{-- <script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS--> --}}

<script src="{{ asset('assets_leden/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
{{-- <script src="{{ asset('assets_leden/vendors/chart.js/dist/Chart.min.js') }}"></script><!-- CORE SCRIPTS--> --}}
<script src="{{ asset('assets_leden/js/app.min.js') }}"></script><!-- PAGE LEVEL SCRIPTS-->
<!-- Charting library -->
<script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>
<!-- Chartisan -->
<script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>
<!-- Your application script -->

<script>
    let chart = new Chartisan({
      el: "#chart",
      url: "@chart('products_by_order')",
      hooks: new ChartisanHooks()
        .colors(['orange'])
        .responsive()
        .beginAtZero()
        .legend({ position: 'bottom' })
        .title("Commandes Mensuelle")
        .datasets([{ type: 'line', fill: false }]),
      // You can also pass the data manually instead of the url:
      // data: { ... }
    })
    $(document).ready(function(){
        $('#select-year').on('change', function(){
            var year = $(this).children("option:selected").val();
            chart.update({
                el: '#chart',
                url: "api/chart/products_by_order?year="+year,
                hooks: new ChartisanHooks()
                .colors(['blue'])
            });
          })
    })
  </script>
@endsection
