<div class="page-sidebar custom-scroll ps" id="sidebar">
    <div class="sidebar-header"><a class="sidebar-brand" href="{{ route('dashboard') }}">LEDEN POS</a><a class="sidebar-brand-mini" href="#">LPOS</a><span class="sidebar-points"><span class="badge badge-success badge-point mr-2"></span><span class="badge badge-danger badge-point mr-2"></span><span class="badge badge-warning badge-point"></span></span></div>
    <ul class="sidebar-menu metismenu">
        @if (Auth::user()->role == 1)
            <li class="mm-active"><a href="{{ route('dashboard') }}"><i class="sidebar-item-icon fa fa-home "></i><span>TABLEAU DE BORD</span></a></li>
        @endif
        {{-- <li class="heading"><span>GESTION UTILISATEURS</span></li> --}}
        @if (Auth::user()->role == 1)
            <li><a href="{{ route('users.index') }}"><i class="sidebar-item-icon fa fa-users"></i><span class="nav-label">Liste des utilisateurs</span><i class="arrow la la-angle-right"></i></a>
        @endif
        </li>
        {{-- <li class="heading"><span>GESTION COMMANDES</span></li> --}}
        <li class=""><a href="javascript:;" aria-expanded="false"><i class="sidebar-item-icon fa fa-coffee"></i><span class="nav-label">Gestion Commandes</span><i class="arrow fa fa-angle-right"></i></a>
            <ul class="nav-2-level mm-collapse" style="height: 0px;">
                <!-- 2-nd level-->
                @if (Auth::user()->role == 1 || Auth::user()->role == 3)
                    <li><a href="{{ route('orderListPrep') }}">Prep Liste Commandes</a></li>
                @endif
                @if (Auth::user()->role == 1 || Auth::user()->role == 2)
                    <li><a href="{{ route('orderListCashier') }}">Caissier Liste Commandes</a></li>
                @endif
            </ul>
            @if (Auth::user()->role == 1)
                <li><a href="{{ route('orderStats') }}"><i class="sidebar-item-icon fa fa-bars"></i> <span class="nav-label">Historique Commandes</span></a></li>
            @endif
        </li>
         @if (Auth::user()->role == 1 || Auth::user()->role == 3)
            <li><a href="{{ route('manageStock') }}"><i class="sidebar-item-icon fa fa-database"></i> <span class="nav-label">Gestion Stock</span></a></li>
         @endif
        <li><a href="{{ route('about') }}"><i class="sidebar-item-icon fa fa-info-circle"></i> <span class="nav-label">A Propos</span></a></li>



    </ul>
</div>
