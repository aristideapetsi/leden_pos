<nav class="navbar navbar-expand navbar-light fixed-top header">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link navbar-icon sidebar-toggler" id="sidebar-toggler" href="#"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a></li>
                    @if (Auth::user()->role == "1")
                        <li class="nav-item dropdown d-none d-sm-inline-block"><a class="nav-link dropdown-toggle megamenu-link" href="#" data-toggle="dropdown"><span>Actions Rapides<i class="ti-angle-down arrow ml-2"></i></span></a>
                            <div class="dropdown-menu nav-megamenu" style="min-width: 400px">
                                <div class="row m-0">
                                    <div class="col-6"><a class="mega-menu-item" href="{{ route('users.index') }}"><i class="ft-activity item-badge mb-4"></i>
                                            <h5 class="mb-2">UTILISATEURS</h5>
                                            <div class="text-muted font-12">Gestion Utilisateurs.</div></a></div>
                                    <div class="col-6"><a class="mega-menu-item bg-primary text-white" href="{{ route('orderListCashier') }}"><i class="ft-globe item-badge mb-4 text-white"></i>
                                            <h5 class="mb-2">COMMANDES</h5>
                                            <div class="text-white font-12">Gestion Commandes</div></a></div>

                                </div>
                            </div>
                        </li>
                    @endif

                </ul>
                <ul class="navbar-nav">

                    <li class="nav-divider"></li>
                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle no-arrow d-inline-flex align-items-center" data-toggle="dropdown" href="#">
                        <span class="d-none d-sm-inline-block mr-2">@if (Auth::user()) {{ Auth::user()->username }}@endif</span>
                        <span class="position-relative d-inline-block"><img class="rounded-circle" src="{{ asset('assets_leden/img/users/admin-image.png') }}" alt="image" width="36" /><span class="badge-point badge-success avatar-badge"></span></span></a>
                        <div class="dropdown-menu dropdown-menu-right pt-0 pb-4" style="min-width: 280px;">
                            <div class="p-4 mb-4 media align-items-center text-white" style="background-color: #2c2f48;"><img class="rounded-circle mr-3" src="{{ asset('assets_leden/img/users/admin-image.png') }}" alt="image" width="55" />
                                <div class="media-body">
                                    <h5 class="mb-1">@if (Auth::user())
                                        {{ Auth::user()->firstname  }} {{ Auth::user()->lastname  }}
                                    @endif  </h5>
                                    <div class="font-13">Administrator</div>
                                </div>
                            </div>
                            <a class="dropdown-item d-flex align-items-center" href="#"><i class="ft-user mr-3 font-18 text-muted"></i>Profil</a>
                            <div class="dropdown-divider my-3"></div>
                            <div class="mx-4">
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button class="btn btn-link p-0" type="submit"><span class="btn-icon"><i class="ft-power mr-2 font-18"></i>Déconnexion</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </li>
                    <li><a class="nav-link navbar-icon quick-sidebar-toggler" href="javascript:;"><span class="ti-align-right"></span></a></li>
                </ul>
            </nav><!-- END: Header-->
