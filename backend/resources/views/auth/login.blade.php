@extends('layouts.app')

@section('content')
<style>body {
	background-color: #eff4ff;
}

.auth-wrapper {
	flex: 1 0 auto;
	display: flex;
	align-items: center;
	justify-content: center;
    padding: 50px 15px 30px 15px;
}

.auth-content {
	max-width: 400px;
	flex-basis: 400px;
    box-shadow: 0 5px 20px #d6dee4;
}
.home-link {
	position: absolute;
	left: 5px;
	top: 10px;
}
</style>
<div class="page-wrapper">
    <div class="auth-wrapper">
        <div class="card auth-content mb-0">
            <div class="card-body py-5">
                <div class="text-center mb-5">
                    <h3 class="mb-3 text-warning">LEDEN POS</h3>
                    <div class="font-18 text-center">Connexion à votre compte</div>
                </div>
                <form id="login-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="mb-4">
                        <div class="md-form mb-0">
                            <input class="md-form-control  @error('email') is-invalid @enderror" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus ><label>Email</label>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-4">
                        <div class="md-form mb-0">
                            <input class="md-form-control @error('password') is-invalid @enderror" type="password" name="password" required autocomplete="current-password"><label>Mot de Passe</label>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                    {{-- <div class="flexbox mb-5"><label class="ui-switch switch-solid">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="ml-0"></span> Se souvenir de moi</label>
                    </div> --}}
                        <button class="btn btn-warning btn-rounded btn-block text-white" type="submit">Connexion</button>
                </form>
                <div class="text-center mt-5 font-13">
                    <div class="mb-2 text-muted">2022 © All rights reserved</div>
                    {{-- <div>See<a class="hover-link ml-2" href="#" style="border-bottom: 1px solid">Politique de Con</a></div> --}}
                </div>
            </div>
        </div>
    </div>
</div><!-- BEGIN: Page backdrops-->
<script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
<script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS-->
<script>
    $(function() {
        $('#login-form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            errorClass: 'invalid-feedback',
            validClass: 'valid-feedback',
            errorPlacement: function(error, element) {
                if (element.hasClass('md-form-control')) {
                    error.insertAfter(element.closest('.md-form'));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(e) {
                $(e).addClass("invalid").removeClass('valid');
            },
            unhighlight: function(e) {
                $(e).removeClass("invalid").addClass('valid');
            },
        });
    });
</script>
@endsection
