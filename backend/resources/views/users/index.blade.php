@extends('layouts.app')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@section('title') Liste des utilisateurs @endsection
@section('content')
<div class="page-wrapper">
    <div class="content-wrapper">
        <!-- BEGIN: Sidebar-->
        @include('layouts.left_sidebar')
        <!-- END: Sidebar-->
        <!-- BEGIN: Content-->
        <div class="content-area">
            <!-- BEGIN: Header-->
            @include("layouts.top_sidebar")
            <!-- END: Header-->
            <div class="page-content fade-in-up">
                <!-- BEGIN: Page heading-->
                <div class="page-heading">
                    <h1 class="page-title page-title-sep">Utilisateurs</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="la la-home font-20"></i></a></li>
                        {{-- <li class="breadcrumb-item">Utilisateurs</li>
                        <li class="breadcrumb-item">Administrateur</li> --}}
                    </ol>
                </div><!-- BEGIN: Page content-->
                <div>


                    <div class="card">
                        <div class="card-body">
                            <h5 class="box-title">Utilisateurs</h5>
                            <div class="flexbox mb-4">
                                <button type="button" class="btn btn-warning add text-white" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Ajouter Utilisateur <i class="fa fa-plus"></i></button>

                            </div>
                            <div class="table-responsive">
                                <span id="addSuccess"></span>
                                <table class="table  table-bordered table-checkable" id="users">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#ID</th>
                                            <th>Role</th>
                                            <th>Identifiant </th>
                                            <th>Prénoms </th>
                                            <th>Nom</th>
                                            <th>Email</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                            {{-- <th class="no-sort"></th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div><!-- END: Page content-->
            </div><!-- BEGIN: Footer-->
            @include('layouts.footer')
            <!-- END: Footer-->
        </div><!-- END: Content-->
    </div>
</div>

</div><!-- END: Quick sidebar-->

<!-- Modal add User-->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: 1px solid orange">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter un nouvel Utilisateur</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">

            <div class="form-group" id="validation-errors"></div>
            <span>&nbsp;Les champs avec (<span class="text-danger">*</span>) sont obligatoires</span>
            <form action="javascript:;" class="form">
                <input type="hidden" name="id">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="md-form"><input type="text" name="firstname" class="md-form-control" ><label>Prénoms<span class="text-danger">*</span></label></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="md-form"><input class="md-form-control" type="text" name="lastname"><label>Nom <span class="text-danger">*</span></label></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="md-form"><input type="text" name="email" class="md-form-control" ><label>Email <span class="text-danger">*</span></label></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="md-form"><input class="md-form-control" type="text" name="username"><label>Identifiant <span class="text-danger">*</span></label></div>
                    </div>
                </div>

                <div class="md-form"><input class="md-form-control" type="password" name="password"><label>Mot de passe <span class="text-danger">*</span></label></div>
                <div class="md-form"><input class="md-form-control" type="password" name="verification_password"><label>Retaper le Mdp</label></div>
                <select class="mdb-select md-form md-form-control" id="select_role" name="role">
                    <option value="" disabled selected>Choisir le role</option>
                    <option value="1">Admin</option>
                    <option value="2">Caissier</option>
                    <option value="3">Préparateur</option>
                  </select>
                  <button type="button" class="btn btn-success btn-save">Enrégistrer <i class="fa fa-save"></i></button>
                  <button type="button" class="btn btn-warning btn-update text-white">Mettre à jour <i class="fa icons-reload" aria-hidden="true"></i></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer <i class="fa fa-times"></i></button>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal add/edit user-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


{{-- <script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script> --}}
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
{{-- <script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS--> --}}

<script src="{{ asset('assets_leden/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
{{-- <script src="{{ asset('assets_leden/vendors/chart.js/dist/Chart.min.js') }}"></script><!-- CORE SCRIPTS--> --}}

<!-- Bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

{{-- begin Datatable links --}}
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
{{-- end Datatable links --}}

<script src="{{ asset('assets_leden/js/app.min.js') }}"></script>

@include('users.inc_users_js')
@endsection
