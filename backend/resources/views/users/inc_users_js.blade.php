<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        //$.noConflict();
        var token = ''
        var modal = $('.modal');
        var form = $('.form');
        var btnAdd = $('.add'),
            btnSave = $('.btn-save'),
            btnUpdate = $('.btn-update');

        var table = $('#users').DataTable({
                ajax: '{{ route('users.list') }}',
                serverSide: true,
                processing: true,
                aaSorting:[[0,"desc"]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'role', name: 'role'},
                    {data: 'username', name: 'username'},
                    {data: 'firstname', name: 'firstname'},
                    {data: 'lastname', name: 'lastname'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action'},
                ]
            });

            /* begin add user with modal*/
        btnAdd.click(function(){
            modal.modal()
            form.trigger('reset')
            modal.find('.modal-title').text('Ajouter un nouvel utilisateur')
            btnSave.show();
            btnUpdate.hide()
            if($("span").hasClass("error_append") == true){
                $('.error_append').remove();
            }
            if($("span").hasClass('success_append') == true){
                $('.success_append').remove();
            }
            if($("span").hasClass('delete_append') == true){
                $('.delete_append').remove();
            }
            if($("span").hasClass("update_append") == true){
                $('.update_append').remove();
            }
        })

        btnSave.click(function(e){
            e.preventDefault();
            var data = form.serialize()
            console.log(data)
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/addUser",
                data: data+'&_token='+token,
                success: function (data) {
                    if (data.success) {
                        table.draw();
                        form.trigger("reset");
                        modal.modal('hide');
                        $('#addSuccess').append('<span class="alert alert-error badge badge-success success_append"><strong>Utilisateur ajouté avec succès</strong></span>')
                        if($("span").hasClass('error_append') == true){
                            $('.error_append').remove();
                        }
                    }
                    else {
                        if($("span").hasClass('error_append') == true){
                            $('.error_append').remove();
                        }
                        var arr = data.message;
                        $.each(arr, function(index, value)
                        {
                            if (value.length != 0)
                            {
                                console.log(value)
                                $("#validation-errors").append('<span class="alert alert-error badge badge-danger error_append"><strong>'+ value +'</strong><span>');

                            }
                        });
                    }
                }
             }); //end ajax
        })

   /* ----------------------end add user with modal ---------------------*/

/* ----------------------begin edit user with modal ---------------------*/
        $(document).on('click','.btn-edit',function(){
            btnSave.hide();
            btnUpdate.show();
            $('label').addClass('active')
            if($("span").hasClass('delete_append') == true){
                $('.delete_append').remove();
            }
            if($("span").hasClass('success_append') == true){
                $('.success_append').remove();
            }
            if($("span").hasClass("error_append") == true){
                $('.error_append').remove();
            }
            if($("span").hasClass("update_append") == true){
                $('.update_append').remove();
            }

            modal.find('.modal-title').text('Modifier les informations de cet utilisateur')
            modal.find('.modal-footer button[type="submit"]').text('Update')

            var rowData =  table.row($(this).parents('tr')).data()

            form.find('input[name="id"]').val(rowData.id)
            form.find('input[name="username"]').val(rowData.username)
            form.find('input[name="firstname"]').val(rowData.firstname)
            form.find('input[name="lastname"]').val(rowData.lastname)
            form.find('input[name="email"]').val(rowData.email)
            // form.find('input[name="role"]').val(rowData.role)
            //console.log( rowData.role)
            //$('#select_role option[value="'+ rowData.role + '"]').prop('selected', true);
            modal.modal()
        })
/* ----------------------end edit user with modal ---------------------*/

/* ----------------------begin update user with modal ---------------------*/
        btnUpdate.click(function(){
            if(!confirm("Vous etes sur de vouloir modifier ?")) return;
            var formData = form.serialize()+'&_method=POST&_token='+token
            var updateId = form.find('input[name="id"]').val()
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/updateUser/" + updateId,
                data: formData,
                success: function (data) {
                    if (data.success) {
                        table.draw();
                        modal.modal('hide');
                        $('#addSuccess').append('<span class="alert alert-error badge badge-success update_append"><strong>Mise à jour éffectuée avec succès</strong></span>')

                    }else {
                        if($("span").hasClass('error_append') == true){
                            $('.error_append').remove();
                        }
                        var arr = data.message;
                        $.each(arr, function(index, value)
                        {
                            if (value.length != 0)
                            {
                                console.log(value)
                                $("#validation-errors").append('<span class="alert alert-error badge badge-danger error_append"><strong>'+ value +'</strong><span>');

                            }
                        });
                    }
                }
             }); //end ajax
        })
/* ----------------------end update user with modal ---------------------*/

/* ----------------------begin delete user with modal ---------------------*/
        $(document).on('click','.btn-delete',function(){
            if(!confirm("Voulez-vous vraiment retirer cet utilisateur?")) return;
            if($("span").hasClass('delete_append') == true){
                $('.delete_append').remove();
            }
            if($("span").hasClass("update_append") == true){
                $('.update_append').remove();
            }
            var rowid = $(this).data('rowid')
                        var el = $(this)
                        if(!rowid) return;
                        $.ajax({
                            type: "GET",
                            dataType: 'JSON',
                            url: "/deleteUser/" + rowid,
                            data: {_method: 'delete',_token:token},
                            success: function (data) {
                                if (data.success) {
                                    table.row(el.parents('tr'))
                                        .remove()
                                        .draw();
                                }
                            }
                        }); //end ajax
                        swal.fire("Poof! Le compte de l'utilisateur a été supprimé!", {
                        icon: "success",
                        });
                        $('#addSuccess').append('<span class="alert alert-error badge badge-success delete_append"><strong>Utilisateur supprimé avec succès</strong></span>')


        })
        /* ----------------------end delete user with modal ---------------------*/
    })
</script>
