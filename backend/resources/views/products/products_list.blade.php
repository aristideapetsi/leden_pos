@extends('layouts.app')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" /><!-- THEME STYLES-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@section('title') Gestion Stock @endsection
@section('content')
<div class="page-wrapper">
    <div class="content-wrapper">
        <!-- BEGIN: Sidebar-->
        @include('layouts.left_sidebar')
        <!-- END: Sidebar-->
        <!-- BEGIN: Content-->
        <div class="content-area">
            <!-- BEGIN: Header-->
            @include("layouts.top_sidebar")
            <!-- END: Header-->
            <div class="page-content fade-in-up">
                <!-- BEGIN: Page heading-->
                <div class="page-heading">
                    <h1 class="page-title page-title-sep">Produits</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="la la-home font-20"></i></a></li>
                        {{-- <li class="breadcrumb-item">Produits</li>
                        <li class="breadcrumb-item">Gestion Stock</li> --}}
                        <div>
                            <button class="btn btn-success btn-reload-orders">Rafraichir Liste Stocks <i class="fa fa-spinner"></i></button>
                        </div>
                    </ol>
                </div><!-- BEGIN: Page content-->
                <div>

                <div class="card">
                    <div class="card-body">
                        <h5 class="box-title">Liste de Produits</h5>
                        <div class="flexbox mb-4">

                        </div>
                        <div class="row productsCheck" style="height: 400px; overflow-y:scroll">
                            <span id="message"></span>


                            <br/>
                        </div>
                        <div class="col col-md-8">
                            <button type="button" class="btn btn-success btn-save col-md-4" onclick="emptyProds()">Vider les stocks sel. <i class="fa fa-save"></i></button>
                            <button type="button" class="btn btn-warning text-white col-md-4" id="resetProd">Réinitialiser sel. Prod. <i class="fa fa-refresh"></i></button>

                        </div>
                        <br/>

                    </div>
                </div>


                </div><!-- END: Page content-->
            </div><!-- BEGIN: Footer-->
            @include('layouts.footer')
            <!-- END: Footer-->
        </div><!-- END: Content-->
    </div>
</div>

</div><!-- END: Quick sidebar-->

<!-- Modal add User-->

<div class="modal fade"  id="modalEditStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: 1px solid orange">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mise à jour du stock : <span id="stockLabel"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body">

            <div class="form-group" id="validation-errors"></div>
            {{-- <span>&nbsp;&nbsp; champs avec (<span class="text-danger">*</span>) sont obligatoires</span> --}}
            <form action="javascript:;" class="form">
                <input type="hidden" name="id">
                <div class="row">
                    <div class="col-sm-6">
                        <input type="hidden" name="id" id="idProd"/>
                        <div class="md-form"><input type="number" min="0" name="stock" required class="md-form-control stock" ><label>Stock<span class="text-danger">*</span></label></div>
                    </div>
                </div>

                  <button type="button" class="btn btn-warning btn-update text-white">Mettre à jour <i class="fa icons-reload" aria-hidden="true"></i></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer <i class="fa fa-times"></i></button>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal add/edit user-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


{{-- <script src="{{ asset('assets_leden/vendors/jquery/dist/jquery.min.js') }}"></script> --}}
<script src="{{ asset('assets_leden/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('assets_leden/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script><!-- CORE SCRIPTS-->
{{-- <script src="{{ asset('assets_leden/js/app.min.js')}}"></script><!-- PAGE LEVEL SCRIPTS--> --}}

<script src="{{ asset('assets_leden/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets_leden/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script><!-- PAGE LEVEL PLUGINS-->
{{-- <script src="{{ asset('assets_leden/vendors/chart.js/dist/Chart.min.js') }}"></script><!-- CORE SCRIPTS--> --}}

<!-- Bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

{{-- begin Datatable links --}}
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
{{-- end Datatable links --}}

<script src="{{ asset('assets_leden/js/app.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@include('products.inc_products_js')
@endsection
