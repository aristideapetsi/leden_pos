<style>
    .overlay-img{
        background-color:#000; opacity:.6;
    }
</style>

<script>
        let productsNA = [];
        let modalEdit = document.getElementById('modalEditStock');

        /**@
         *loader orders begin======================================================
         */
            function loader(){
                $('button').prop('disabled', true);
                $('.productsCheck').append('<div style="" id="loadingDiv"><div class="loader"><span style="color: red">Chargement stocks...</span></div></div>');
            }

            function removeLoader(){
                $( "#loadingDiv" ).fadeOut(500, function() {
                    // fadeOut complete. Remove the loading div
                    $('button').prop('disabled', false);
                    $( "#loadingDiv" ).remove(); //makes page more lightweight
                });
            }
        /**@
         *loader orders end=====================================================================
         */
        /**
         *Edit modal stock
         */
        var editProd = function(id,currentStock, prodName){
           $('#idProd').val(id);
            $('label').addClass('active');
            $('.stock').val(currentStock);
            $('#stockLabel').html(prodName);
        }
        /**
         * Stack products N/A
         * */
         var cancProd = function(id){
            $('#cancProd-' + id).prop("disabled", true);
                // var id = $('#cancProd').data('id')
                console.log('.productsCard-'+ id )
                $('.productsCard-'+ id ).addClass("border border-light")
                $('.productsCard-'+ id + ' img' ).addClass("overlay-img")
                $('.prodTitle-'+ id ).removeClass("text-dark")
                $('.prodTitle-'+ id ).addClass("text-light")
                $('.qty-'+ id).removeClass('badge-dark')
                $('.qty-'+ id).addClass('badge-light')
                productsNA.push(id);
                console.log(productsNA);
        }


    $(document).ready(function(){
        function error_toast(message, title){
                Command: toastr["error"](title, message)
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }
            function success_toast(message, title){
                Command: toastr["success"](`Mise à jour Stock`, "Stock Vidé avec succès!!")
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }
        /**
         *Send products array to declare prods N/A
        */
            $('.btn-save').on('click', function(){
                if(productsNA.length == 0){
                    error_toast('Mise à jour stock', 'Aucun produit séléctionné !!!');
                    return;
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    data: {"prods": productsNA},
                    url: '/emptyProducts?prods=',
                    success: function(response){
                        $( ".ProductItem" ).remove();
                        getProductsList();
                        success_toast("Stock Vidé avec succès!!", `Mise à jour Stock`);
                    }

                });
            })


        // }
        /**@argument
         * Update product stock
         *
         */
        $('.btn-update').on('click', function(){
            var id = $('#idProd').val();
            var stock = $('.stock').val();
            if(stock == ""){
               error_toast('Mise à jour stock', 'Champ Stock Obligatoire!!!');
                return;
            }
            if(stock < 0){
               error_toast('Mise à jour stock', 'Impossible stock négatif');
                return;
            }else if (stock >= 0){
                $.ajax({
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    type: "POST",
                    data: {
                        "stock": stock,
                        "prodId": id
                    },
                    url: '/updateProdStock/' + id,
                    success: function(response){
                        $( ".ProductItem" ).remove();
                        getProductsList();
                        success_toast("Stock mise à jour avec succès!!",`Mise à jour Stock` )
                    }
                });

            }else {
                error_toast('Ereur Inattendu !!!', 'Mise à jour Stock');
            }

        })

        //-----------------------------------------------------------------------------
        //--------------------------verify while page idle refresh page begin-----------------
        //-----------------------------------------------------------------------------

        //Increment the idle time counter every minute
        var idleTime = 0;
        var idleInteral = setInterval(timerIncrement, 60000) //1min
        //zero the idle timer on mouse movement
        $(this).mousemove(function(e){
            idleTime = 0;
        });
        $(this).keypress(function(e){
            idleTime = 0;
        });
        function timerIncrement(){
            idleTime += 1;
            if(idleTime > 2){
                $( ".ProductItem" ).remove()
                getProductsList();

            }
        }
        //-----------------------------------------------------------------------------
        //--------------------------verify while page idle refresh page end-----------------
        //-----------------------------------------------------------------------------

        //reload products
            $('.btn-reload-orders').on('click', function(){
            $( ".ProductItem" ).remove()
            getProductsList();
        })
        //reset product selected
        $('#resetProd').on('click', function(){
            loader();
            $('.productsC button').prop("disabled", false);
            $('.productsC').removeClass("border border-light")
            $('.productsC img' ).removeClass("overlay-img")
            $('.productsC span').removeClass('badge-light')
            $('.productsC span' ).addClass("text-white")
            $('.productsC span').addClass('badge-dark')
            $('.productsC h6').addClass('text-dark')
            productsNA = [];
            removeLoader();
            console.log(productsNA);
        })

        /**
         *  Save cancel Prod (N/A Prods)
         */
         $('#btn-save').on('click', function(){
            $.Ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: "/emptyProducts/" + id,
                success: function(){
                    $( ".ProductItem").remove();
                    getProductsList();
                }
            });
        });


        getProductsList();
        function getProductsList(){
            loader();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: '/getProductsList/',
                async: false,
                success: function(data){
                    var products = data.products;
                    // var images = data.products_images;
                    // var stocks = data.products_stock;
                    //  console.log(stocks);

                    if(products.length > 0){

                        for(var i = 0; i < products.length; i++){
                            $('.productsCheck').append(`<div class="col col-md-4 ProductItem">
                                <div class="shadow card productsC productsCard-${products[i].id}" id="order1" style="border: ${products[i].stock_quantity != 0 ? "1px solid orange" : "1.5px solid red"}">
                                    <div class="card-body container">

                                        <div class="row row-cols-2">
                                            <div class="col">
                                                <img  class="rounded shadow" src="${products[i] == null ? "/assets_leden/img/default.png" : products[i].images[0].src}" alt="image"  width="80" height="60"/>
                                            </div>
                                            <div class="col">
                                                <button type="button" id="cancProd-${products[i].id}" onclick="cancProd(${products[i].id})" data-id="${products[i].id}" class="shadow btn btn-danger"><i class="fa fa-times"></i></button>
                                                <button type="button" id="editProd-${products[i].id}" data-toggle="modal" data-target="#modalEditStock" onclick="editProd(${products[i].id},${products[i].stock_quantity}, '${products[i].name}')" data-id="${products[i].id}" class="shadow btn btn-warning text-white"><i class="fa fa-edit"></i></button>
                                            </div>
                                        </div>
                                        <hr style="height: 0.1px; ${products[i].stock_quantity != 0 ? 'background-color: black' : 'background-color:red'}"/>
                                        <h6 class="text-center prodTitle-${products[i].id}"><b><span class="qty-${products[i].id} text-white badge ${products[i].stock_quantity != 0 ? "badge-dark" : "badge-danger"} " > X ${products[i].stock_quantity != null || products[i].stock_quantity != 0   ? products[i].stock_quantity : "0"} </span> ${products[i].name} <b></h6>
                                    </div>
                                </div>
                                </div>`)

                        }
                        removeLoader();
                    }else{
                        $('.productsCheck').append("<div id='NoProduct'>Aucun produit enrégistré !!!!!</div>");
                         removeLoader();
                    }


                }
            });
        }
   });
</script>

