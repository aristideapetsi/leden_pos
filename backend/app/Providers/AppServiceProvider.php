<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use ConsoleTVs\Charts\Registrar as Charts;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Charts $charts)
    {
        $charts->register([
            \App\Charts\ProductsByOrder::class,
            \App\Charts\ProductByCategory::class,
            \App\Charts\Incomes::class,
            \App\Charts\MonthlyIncomes::class,
            \App\Charts\UsersChart::class,
            \App\Charts\ProductsChart::class,
            \App\Charts\OrderCompareChart::class,
        ]);
        Schema::defaultStringLength(191);
    }
}
