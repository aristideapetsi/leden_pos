<?php

// declare(strict_types = 1);

namespace App\Charts;

use App\Models\OrderStat;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class Incomes extends BaseChart
{
    public function handler(Request $request): Chartisan
    {
        $amount = $this->getYearlyIncomeData();
        $years = $amount['years'];
    //  dd($amount['years']);
        return Chartisan::build()
            ->labels($years)
            ->dataset('Chiffres d\'affaire', $amount['incomes']);
    }

    /**
     * Transaction management
     */

    public function getAllYear(){
        $year_array = array();
        $incomes_dates = OrderStat::orderBy('created_at', 'ASC')
                                    //  ->where('delete_state','=', 0)
                                    ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
                                    ->pluck('created_at');
        if(!empty($incomes_dates)){
            foreach($incomes_dates as $unformatted_date){;
                array_push($year_array, (new \DateTime($unformatted_date))->format('Y'));
            }
        }
        // dd(array_unique($year_array));
        return array_unique($year_array);
    }

    public function getYearlyAmount($year){
        // $month = 06;
        $sum = 0;
        // dd($year);
        $amount = OrderStat::whereYear('created_at', $year)
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
        ->get();

        foreach($amount as $m)
        {
            $sum += ($m->total_price);
        }
        // dd($sum);
        return $sum ;
    }



    public function getYearlyIncomeData(){
        $yearly_project_data = array();
        $year_array = $this->getAllYear();
        //  dd($year_array );
        $year_name_array = array();
        $yearly_income_array = array();
        if(!empty($year_array) ){
            foreach($year_array as $year_no => $year){
                $yearly_income = $this->getYearlyAmount($year);
                array_push($yearly_income_array, $yearly_income);
                array_push($year_name_array, $year);
            }
        }

        $year_array = $this->getAllYear();
        $yearly_income_data = array(
            'years' => $year_name_array,
            'incomes' => $yearly_income_array,
        );

        return $yearly_income_data;
    }
}
