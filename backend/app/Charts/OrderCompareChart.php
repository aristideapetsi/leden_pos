<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\OrderStat;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class OrderCompareChart extends BaseChart
{
   /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {

        $data = $this->getOrdersCountData();

        return Chartisan::build()
            ->labels(['Commandes annulées', 'Commandes livrées', 'Commandes payées non traitées', 'Commandes restées en cuisine'])
            ->dataset('orders', $data['orders']);
    }

    public function getOrdersCountData(){
        $orders_array = [];

        $ordersCancelled = count(OrderStat::where('status', 'cancelled')->get());
        $ordersDelivered = count(OrderStat::where('status', 'delivered')->get());
        $ordersReadyKitchen = count(OrderStat::where('status', 'ready-kitchen')->get());
        $ordersCompleted = count(OrderStat::where('status', 'completed')->get());


        array_push($orders_array, $ordersCancelled);
        array_push($orders_array, $ordersDelivered);
        array_push($orders_array, $ordersReadyKitchen);
        array_push($orders_array, $ordersCompleted);

        $orders_data = array(
            'orders' => $orders_array,
        );
        return $orders_data;
    }
}
