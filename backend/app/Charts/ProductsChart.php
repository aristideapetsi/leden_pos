<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Like;
use App\Models\Product;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class ProductsChart extends BaseChart
{
        /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {

        $data = $this->getProductsCountData();

        return Chartisan::build()
            ->labels(['Produits Vendus', 'Total Likes'])
            ->dataset('Produits', $data['products']);
    }

    public function getProductsCountData(){
        $products_array = [];
        $total_sales_sum = 0;
        $total_stock_sum = 0;

        $products = Product::all();
        foreach($products as $product){
            $total_sales_sum += $product->total_sales;
        }

        foreach($products as $product){
            $total_stock_sum += $product->stock_qty;
        }
        $likes_total = count(Like::where('status', 1)->get());
        // $users = count(User::all());
        $productSales = $total_sales_sum;
        // $products_total = count($products);

        array_push($products_array, $productSales);
        array_push($products_array, $likes_total);
        // array_push($products_array, $total_stock_sum);

        $products_data = array(
            'products' => $products_array,
        );
        return $products_data;
    }
}
