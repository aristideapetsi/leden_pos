<?php

// declare(strict_types = 1);

namespace App\Charts;

use App\Models\OrderStat;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class MonthlyIncomes extends BaseChart
{
    public function handler(Request $request): Chartisan
    {
        $year = $request->year;
        $data = $this->getYearlyIncomesData($year);

    //  dd($amount['years']);
        return Chartisan::build()
            ->labels($data['months'])
            ->dataset('Chiffres d\'affaire', $data['incomes_data']);
    }

    /**
     * Transaction management
     */
    public function getIncomes($month, $year){
        $sum = 0;
        $orders = OrderStat::whereMonth('created_at', $month)->whereYear('created_at', $year)
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
                                    ->get();

        foreach($orders as $m)
        {
            $sum += ($m->total_price);
        }
        // dd($sum);
        return $sum ;
    }

    public function getYearlyIncomes($year){
        // $month = 06;
        $sum = 0;
        // dd($year);
        $amount = OrderStat::whereYear('created_at', $year)
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
                                    ->get();

        foreach($amount as $m)
        {
            $sum += ($m->total_price);
        }
        // dd($sum);
        return $sum ;
    }




    public function getYearlyIncomesData($year){
        $monthly_data = array();
        $month_array = ['','Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin','Juillet', 'Aout', 'Septembre','Octobre','Novembre','Décembre'];
        $month_name_array = array();
        $monthly_orders_count_array = array();
        $orders_count_array = array();

        if (!empty($month_array)) {
            for($i=1; $i<=12; $i++) {
                // dd($month_no+1);
                if($year == null){
                    $monthly_orders_count_array = $this->getIncomes($i, Carbon::now()->format('Y'));
                }else{
                    $monthly_orders_count_array = $this->getIncomes($i,$year);
                }

                array_push($orders_count_array, $monthly_orders_count_array);
                array_push($month_name_array, $month_array[$i]);
            }
        }

        // $month_array = $this->getAllMonth();
        $monthly_data = array(
            'months' => $month_name_array,
            'incomes_data' => $orders_count_array,
        );
        return $monthly_data;
    }
}
