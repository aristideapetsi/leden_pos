<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\CategoryProduct;
use App\Models\Product;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class ProductByCategory extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $products_by_category = $this->getProductsByCategoryData();

        return Chartisan::build()
            ->labels($products_by_category['categories'])
            ->dataset('Produits', $products_by_category['products_by_category_count'])
            ->dataset('Catégories', $products_by_category['categories']);
    }

    /**
     * Retrieve all categories
     */
    public function getAllCategoriesForProducts(){
        $categories_array = array();
        $categories = CategoryProduct::all();

        if(!empty($categories)){
            foreach($categories as $c){
                $categories_array[$c->id] = $c->name;
            }
        }
        return $categories_array;

    }

    /**
     * Retrieve number of product by category
     */
    public function getProductCategoriesCount($category_id){
        $product_counts = Product::where('category_id', $category_id)
                                    // ->where('delete_state','=', 0)
                                    ->get()->count();
        return $product_counts ;
    }
    /**
     * Retrieve products by category data
     */

    public function getProductsByCategoryData(){
        $category_product_data = array();
        $category_array = $this->getAllCategoriesForProducts();
        // dd($month_array );
        $category_name_array = array();
        $products_by_category_count_array = array();
        if(!empty($category_array) ){
            foreach($category_array as $category_no => $category_name){
                $category_product_count = $this->getProductCategoriesCount($category_no);
                array_push($products_by_category_count_array, $category_product_count);
                array_push($category_name_array, $category_name);
            }
        }

        $category_array = $this->getAllCategoriesForProducts();
        $category_products_data = array(
            'categories' => $category_name_array,
            'products_by_category_count' => $products_by_category_count_array,
            // 'max' => $max_don
        );
        return $category_products_data;
    }
}
