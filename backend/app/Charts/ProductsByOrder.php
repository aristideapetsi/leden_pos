<?php

//declare(strict_types = 1);

namespace App\Charts;

use App\Models\OrderProduct;
use App\Models\OrderStat;
use App\Models\Post;
use App\Models\ProductMeta;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Order;
class ProductsByOrder extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $year = $request->year;
        // dd($year);
        $data = $this->getYearlyOrdersData($year);
        // dd($data);
        return Chartisan::build()
            ->labels($data['months'])
            ->dataset('Commandes', $data['orders_data']);
    }

    public function getOrdersCount($month, $year){

        $orders = OrderStat::whereMonth('created_at', $month)->whereYear('created_at', $year)
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
        ->count();
        return $orders ;
    }

    public function getYearlyOrders($year){
        // $month = 06;
        $sum = 0;
        // dd($year);
        $orders = OrderStat::whereYear('created_at', $year)
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
        ->count();
        return $orders ;
    }


    public function getYearlyOrdersData($year){
        $monthly_data = array();
        $month_array = ['','Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin','Juillet', 'Aout', 'Septembre','Octobre','Novembre','Décembre'];
        $month_name_array = array();
        $monthly_orders_count_array = array();
        $orders_count_array = array();

        if (!empty($month_array)) {
            for($i=1; $i<=12; $i++) {
                // dd($month_no+1);
                if($year == null){
                    $monthly_orders_count_array = $this->getOrdersCount($i, Carbon::now()->format('Y'));
                }else{
                    $monthly_orders_count_array = $this->getOrdersCount($i,$year);
                }

                array_push($orders_count_array, $monthly_orders_count_array);
                array_push($month_name_array, $month_array[$i]);
            }
        }

        // $month_array = $this->getAllMonth();
        $monthly_data = array(
            'months' => $month_name_array,
            'orders_data' => $orders_count_array,
        );
        return $monthly_data;
    }
}
