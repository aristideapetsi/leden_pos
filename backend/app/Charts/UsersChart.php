<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\User;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class UsersChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {

        $data = $this->getUsersCountData();

        return Chartisan::build()
            ->labels(['Caissier', 'Préparateurs', 'admins', 'Clients'])
            ->dataset('utilisateurs', $data['users']);
    }

    public function getUsersCountData(){
        $users_array = [];

        // $users = count(User::all());
        $cashiers = count(User::where('role', 2)->get());
        $preparators = count(User::where('role', 3)->get());
        $admins = count(User::where('role', 1)->get());
        $customers = count(User::where('role', 4)->get());

        // array_push($users_array, $users);
        array_push($users_array, $cashiers);
        array_push($users_array, $preparators);
        array_push($users_array, $admins);
        array_push($users_array, $customers);

        $users_data = array(
            'users' => $users_array,
        );
        return $users_data;
    }
}
