<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;
    protected $table = "order_products";

    protected $fillable = ["id", "order_id", "product_id", "variation_id", "customer_id", "created_at", "product_qty", "selling_price"];

    public function order(){
        return $this->belongsTo(OrderStat::class);
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
