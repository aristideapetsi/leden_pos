<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopSetting extends Model
{
    use HasFactory;
    protected $table = "shop_settings";

    protected $fillable = ['shop_id', 'shop_logo', 'contacts', 'email', 'address', 'description', 'main_banner1',
    'main_banner2', 'main_banner3', 'text_banner1', 'text_banner2', 'text_banner3', 'shop_name'];
}
