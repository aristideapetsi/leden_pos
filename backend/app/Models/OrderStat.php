<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStat extends Model
{
    use HasFactory;
    // protected $primaryKey = "order_id";
    protected $table = "orders";

    protected $fillable = ["id", "prod_qty", "total_price","status", "customer_id", "shipping_total","payment_channel","firstname", "lastname", "address1", "address2", "notes", "contacts", "created_at", "updated_at", "tracking_no", "created_at", "cooked_at"];

    // public function products(){
    //     return $this->belongsToMany(Product::class, 'order_products', 'order_id', 'product_id')->using(OrderProduct::class);
    // }
    public  function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'customer_id');
    }
}
