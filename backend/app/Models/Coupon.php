<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
    protected $table = "coupons";
    protected $fillable = ['code','type','value', 'cart_value'];

    // public static function findByCode($code){
    //     return self::where('code', $code)->first();
    // }

    // public function discount($subtotal)
    // {
    //     if($this->type == 'fixed'){
    //         return $this->value;
    //     }else if($this->type == 'percent'){
    //         return ($this->percent_off / 100) * $subtotal;
    //     }else{
    //         return 0;
    //     }
    //    return ($subtotal * ($this->percent_off / 100));
    // }

}
