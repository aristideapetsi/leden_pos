<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentChannel extends Model
{
    use HasFactory;
    protected $table = "payment_channels";

    protected $fillable = ["id", "name", "feature_image", "created_at"];
}
