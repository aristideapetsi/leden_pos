<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ["id","description", "total_sales", "stock_quantity", "stock_status", "product_name", "category_id", "product_image", "price"];

   public  function orderProducts()
   {
       return $this->hasMany(OrderProduct::class);
   }

   public function categories(){
       return $this->belongsTo(CategoryProduct::class, 'category_id', 'id');
   }
}
