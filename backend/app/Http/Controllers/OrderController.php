<?php

namespace App\Http\Controllers;

use App\Events\NewTrade;
use App\Events\OrderUpdate;
use App\Models\OrderItem;
use App\Models\OrderProduct;
use App\Models\OrderStat;
use Carbon\Carbon;
use DateTime;
use Facade\Ignition\Exceptions\ViewException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\User;
use App\Notifications\OrderNotification;
use App\Notifications\OrderUpdateNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

 class OrderController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    /**
     * List order preparator
     */
    public function orderListPrep()
    {
       return view('orders.order_prep_list');
    }
        /**
     * Get Order List in datatable for Preparator
     */
    public function getOrdersDataPrep(Request $request)
    {
            $orders = OrderStat::where('status', '=', 'completed')->get();
            $orders_array = [];


            foreach($orders as $order){
                $date_gmt = Carbon::create($order->created_at);
                if($date_gmt->between(Carbon::yesterday(), Carbon::now())){
                    array_push($orders_array, $order);
                }
            }
        return response()->json([
            'orders' => $orders_array,
         ]);

    }
    /**
     * Get Order List in datatable for cashier
     */
    public function getOrdersDataCashiers(Request $request)
    {
            $orders = OrderStat::all();
            $orders_array = [];

            foreach($orders as $order){
                $date_gmt = Carbon::create($order->created_at);
                if($date_gmt->between(Carbon::yesterday(), Carbon::now())){
                    array_push($orders_array, $order);
                }
            }

        return response()->json([
            'orders' => $orders_array,
         ]);
}
    /**
     * Get Cashier orders list by json
    */
     public function getOrdersCashier()
    {
        $orders = OrderStat::where('status', '!=', 'delivered')->with('orderProducts.product')->get();

        $orders_array = [];

        foreach($orders as $order){
            $date_gmt = Carbon::create($order->created_at);
            if($date_gmt->isSameDay(Carbon::today())){
                array_push($orders_array, $order);
            }
        }

        return response()->json([
            'orders' => $orders_array,
         ]);
    }

    /**
     * Get preparators orders list by json
     */
    public function getOrdersPrep()
    {
       $orders = OrderStat::where('status', '=','completed')->with('orderProducts.product')->get();
       $orders_array = [];

       foreach($orders as $order){
           $date_gmt = Carbon::create($order->created_at);
           if($date_gmt->isSameDay(Carbon::today())){
               array_push($orders_array, $order);
           }
       }
        return response()->json([
            'orders' => $orders_array,
         ]);
    }
    /**
     * List cashier order
     */
    public function orderListCashier()
    {
        return view('orders.order_cashier');
    }

    /**
     * Validate Order By preparator | preparation done
     */
    public function validateOrder($order_id)
    {
        $order = OrderStat::find($order_id);

        $order->status = "ready-kitchen";
        $order->cooked_at = Carbon::now();

        $order->save();
        $order_products = OrderProduct::where('order_id', $order->id)->get();
        foreach($order_products as $order_product){
          //Decrement product quantity
            $product = Product::find($order_product->product_id);
            $product->stock_qty -= $order_product->product_qty;
            $product->total_sales += $order_product->product_qty;
            if($product->stock_qty == 0){
                $product->stock_status = "out_of_stock";
            }
            $product->save();
        }
        $userSchema = User::find(Auth::user()->id);
        $orderData = [
            'order_id' => $order->id,
            'message' => 'Commande N* '  . $order->id . ' prete en cuisine'
        ];

        Notification::send($userSchema, new OrderUpdateNotification($orderData));
        event(new OrderUpdate('Commande N* '  . $order->id . ' prete en cuisine'));

        return response()->json(['message' => 'Order Validate successfully !!! ']);
    }
    /**
     * Cancel order by cashier
     */

     public function cancelOrder($order_id)
     {
         if(Auth::user()){
            $order = OrderStat::find($order_id);

            $order->status = "cancelled";

            $order->save();
            return response()->json([
                'message' => 'Commande annulée avec succès !!! ',
                'code' => 200,
                'status' => 'success'
            ]);

         }else{
            return response()->json([
                'message' => 'Veuillez-vous connectez pour continuer!!! ',
                'code' => 401,
                'status' => 'failed'
            ]);
         }
     }

    /**
     * Get Info About product
     */
    public function getProductsByOrder($order_id)
    {
        // $products = Post::where('order_id', '=', $order_id)->where('order_item_type', '=', 'line_item')->get();
        $products = DB::table('wp_wc_order_product_lookup')
                    ->leftJoin('wp_posts', 'wp_posts.id', '=', 'wp_wc_order_product_lookup.product_id')
                    ->where('wp_wc_order_product_lookup.order_id', '=', $order_id)
                    ->select('wp_posts.*', 'wp_wc_order_product_lookup.*')
                    ->distinct()
                    ->get();
        // dd($products);
        $products_images = [];
        foreach($products as $product){
            $image = DB::table('wp_wc_product_meta_lookup')
            ->leftJoin('wp_posts', 'wp_posts.post_parent', '=', 'wp_wc_product_meta_lookup.product_id')
            ->where('wp_posts.post_parent', '=', $product->product_id)
            ->select('wp_posts.guid')
            ->first();
            array_push($products_images, $image);
        }
        // dd($products_images);



        return response()->json([
            'products' => $products,
            'products_images' => $products_images
        ]);
    }

    /**
     * Collect Order Money by cashier
     */
    public function CollectCashByCashier($order_id)
    {

        $order = OrderStat::find($order_id);

        $order->status = "completed";
        $order->payment_channel = "1";

        $order->save();
        $userSchema = User::find(Auth::user()->id);
        $orderData = [
            'order_id' => $order->id,
            'message' => 'Commande N* '  . $order->id . ' en cours de prépa'
        ];

        Notification::send($userSchema, new OrderUpdateNotification($orderData));
        event(new OrderUpdate('Commande N* '  . $order->id . ' en cours de prépa'));


        return response()->json(['message' => 'Order cash collected successfully !!! ']);
    }

    /**
     * Deliver order
     */
    function deliveryOrder($order_id)
    {
        $order = OrderStat::find($order_id);

        $order->status = "delivered";


        $order->save();
        $userSchema = User::find(Auth::user()->id);
        $orderData = [
            'order_id' => $order->id,
            'message' => 'Commande N* '  . $order->id . ' a été livré'
        ];

        Notification::send($userSchema, new OrderUpdateNotification($orderData));
        event(new OrderUpdate('Commande N* '  . $order->id . ' a été livré'));

        return response()->json(['message' => 'Order delivered successfully !!! ']);
    }


    /**
     * Update post meta value |
     */
    // public function updateMetaPostValue($idPost, $meta_key, $meta_value)
    // {
    //     $post_meta = postMeta::where('post_id', '=', $idPost)->where('meta_key', '=', $meta_key)->first();
    //     $post_meta->meta_value = $meta_value;
    //     $post_meta->timestamps = false;
    //     $post_meta->save();
    // }

    /**
     * Get orders stats in datatable
     */
    public function orderStats(Request $request)
    {
        if(Auth::user()){
            $orders_stats = OrderStat::orderBy('created_at', 'desc')->with('user')->get();
            return response()->json([
                'orders' => $orders_stats,
                'code' => 200,
                'status' => 'success',
                'message' => 'order get successfully !!!'
            ]);
        }else{
            $orders_stats = [];

            return response()->json([
                'orders' => $orders_stats,
                'code' => 401,
                'status' => 'failed',
                'message' => 'unauthorized'
            ]);
        }
        // if ($request->ajax()) {
        //     $orders = OrderStat::all();
        //     return datatables()->of($orders)
        //             ->addColumn('date_created_gmt', function($row){
        //                 return Carbon::create($row->date_created_gmt)->isoFormat('DD/MM/YYYY');
        //             })
        //             ->addColumn('status', function($row){
        //                 $html ='';
        //                 if($row->status == "on-hold"){
        //                     $html = 'En Attente...';
        //                 }else if($row->status == "processing"){
        //                     $html = 'En attente paiement';
        //                 }else if($row->status == "pending"){
        //                     $html = 'En cours ...';
        //                 }else if($row->status == "wc-ready-kitcken"){
        //                     $html = 'Pret Cuisine.';
        //                 }
        //                 else if($row->status == "completed"){
        //                     $html = 'Terminée.';
        //                 }
        //                 return $html;
        //             })
        //             ->toJson();

        // }
        // return view('orders.orders_stats_list');
    }

}
