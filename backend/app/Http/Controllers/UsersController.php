<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    /**
     * User Management index page
     */
    public function index()
    {
        return view('users.index');
    }


    /**
     * Add User to database
     */
    public function addUser(Request $request)
    {
        // do validation
        $v = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'role' => 'required',
            'password' => 'min:6',
            'verification_password' => 'required_with:password|same:password|min:6'
        ]);
        // dd($request->all());
        if ($v->fails())
        {
            return [
                'success' => false,
                'message' => $v->errors(),
                'code' => 204
            ];
        }

        $user = new User();
        $user->username = $request->username;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        // $user->roles()->attach($request->role);
        $user->role = $request->role;

        $user->save();

        return [
            'success' => true,
            'message' => 'Utilisateur crée avec succès',
            'code' => 200
        ];
    }

    /**
     * Show users list
     */
    public function listUsers(Request $request)
    {
        $users = User::where('role', '!=', 0)->get();

        return response()->json([
            'users' => $users
        ]);
    }


    /**
     * Update user data
     */
    public function updateUser($id, Request $request)
    {
        $user = User::find($id);

        // do validation
        if($user->email == $request->email){
            $v = Validator::make($request->all(), [
                'username' => 'required|max:255',
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'email' => 'required|email|max:255',
                'role' => 'required',
            ]);
        }else{
            $v = Validator::make($request->all(), [
                'username' => 'required|max:255',
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'email' => 'required|email|unique:users|max:255',
                'role' => 'required',
            ]);
        }
        if ($v->fails())
        {
            return ['success' => false, 'message' => $v->errors()];
        }

        $user =  User::find($id);
        $user->username = $request->username;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        // $user->role()->save($request->role);
        $user->role = $request->role;
        if($user->password != ""){
            $user->password = bcrypt($request->password);
        }

        $user->update();
        return ['success' => true, 'message' => 'Les informations de l\'utilisateur sont à jour ...'];
    }

    public function removeUser($id)
    {
        User::find($id)->delete();
        return ['success' => true, 'message' => 'Deleted Successfully'];
    }

    public function enableUser()
    {

    }

    public function disableUser()
    {

    }

    public function updateAddressCustomer(Request $request)
    {
       $city = $request->city;
       $address1 = $request->address1;
       $address2 = $request->address2;
       $phone_number = $request->phone_number;

       if(Auth::user()){
        $user = User::find(Auth::user()->id);
        $user->city = $city;
        $user->address1 = $address1;
        $user->address2 = $address2;
        $user->phone_number = $phone_number;
        $user->save();
        return response()->json([
            'code' => 200,
            'status' => 'success',
            'message' => 'Les informations de l\'utilisateur ont été mise à jour'
        ]);
       }else{
        return response()->json([
            'code' => 401,
            'status' => 'failed',
            'message' => 'Veuillez vous connectez pour poursuivre!!!!'
        ]);
       }


    }

    public function updateAdminAccountInfo(Request $request)
    {
        $email = $request->email;
        $v = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            ]);
        if ($v->fails())
        {
            return [
                'success' => false,
                'message' => $v->errors(),
                'code' => 204
            ];
        }
           //check if email is unique and fill
           if($email != Auth::user()->email){
            $email_v = Validator::make($request->all(), [
                'email' => 'required|email|unique:users|max:255',
            ]);
            if ($email_v->fails())
            {
                return [
                    'success' => false,
                    'message' => $email_v->errors(),
                    'code' => 204
                ];
            }
        }
        if(Auth::user()){

            $user =  User::find(Auth::user()->id);
            $user->username = $request->username;
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->phone_number = $request->phone_number;
            $user->address1 = $request->address1;
            $user->email = $request->email;
            $user->save();
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' =>'Vos informations personnelles ont été mise à jour !!!'
            ]);
        }else{
            return response()->json([
                'code' => 401,
                'status' => 'failed',
                'message' =>'Veuillez-vous connecter pour continuer !!!'
            ]);
        }

    }

    public function updateAdminPass(Request $request)
    {
        $password_v = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'verification_password' => 'required|required_with:password|same:password'
        ]);
        if ($password_v->fails())
        {
            return [
                'code' => 204,
                'success' => false,
                'message' => $password_v->errors(),
            ];
        }
        if(Auth::user()){
            $user =  User::find(Auth::user()->id);
            if($request->password != ""){
                $user->password = bcrypt($request->password);
            }

            $user->save();
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' =>'Votre mot de passe a été mise à jour avec succès !!!'
            ]);
        }else{
            return response()->json([
                'code' => 401,
                'status' => 'failed',
                'message' =>'Veuillez vous connectez pour poursuivre!!!!'
            ]);
        }


    }

    /**
     * update image profile
     */
    public function updateProfileAvatar(Request $request)
    {
        if(Auth::user()){
            $user = User::find(Auth::user()->id);

            if ($request->file('avatar')) {
                $fileName =  "avatar-".time().'.'.$request->avatar->getClientOriginalExtension();
                $request->file('avatar')->move('../../frontend/src/assets_leden/img/users', $fileName);
                $user->avatar = $fileName;

                $user->save();
                return response()->json([
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Votre photo de profil a été mis à jour avec succès !!!'
                ]);
            }

        }else{
            return response()->json([
                'code' => 401,
                'status' => 'success',
                'message' => 'Veuillez vous connecter pour continuer !!!'
            ]);
        }

    }
    /**
     * update account infos
     */
    public function updateAccountInfo(Request $request)
    {
       if(Auth::user()){
           $email = $request->email;
           $password = $request->password;

           //check if email is unique and fill
           if($email != Auth::user()->email){
            $email_v = Validator::make($request->all(), [
                'email' => 'required|email|unique:users|max:255',
            ]);
            if ($email_v->fails())
            {
                return [
                    'success' => false,
                    'message' => $email_v->errors(),
                    'code' => 204
                ];
            }
           }
           if($password == ''){
            $v = Validator::make($request->all(), [
                'username' => 'required|max:255',
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'phone_number' => 'required',
                // 'password' => 'min:6',
                // 'verification_password' => 'required_with:password|same:password'
                ]);

           }else{
                $password_v = Validator::make($request->all(), [
                    'password' => 'min:6',
                    'verification_password' => 'required_with:password|same:password'
                ]);
                if ($password_v->fails())
                {
                    return [
                        'success' => false,
                        'message' => $password_v->errors(),
                        'code' => 204
                    ];
                }
                $password = bcrypt($request->password);

           }

            $user =  User::find(Auth::user()->id);
            $user->username = $request->username;
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            if($request->password != ""){
                $user->password = bcrypt($request->password);
            }

            $user->save();
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' =>'Les informations de l\'utilisateur ont été mise à jour'
            ]);
       }else{
            return response()->json([
                'code' => 401,
                'status' => 'failed',
                'message' =>'Veuillez vous connectez pour poursuivre!!!!'
            ]);
       }

    }
}
