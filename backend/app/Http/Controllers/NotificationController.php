<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{


    /**
     *  Get all notifications
     */
    public function getAllOrderNotifications()
    {
        $user = User::find(Auth::user()->id);
        $notifications = [];

        foreach ($user->notifications as $notification) {
            array_push($notifications, $notification);
        }
        return response()->json([
            'notifications' => $notifications
        ]);
    }

    public function getOrdersUpdateNotifications()
    {
        $user = User::find(Auth::user()->id);
        $notifications = [];

        foreach ($user->unreadNotifications->where('type', 'App\Notifications\OrderUpdateNotification') as $notification) {
            $date_gmt = Carbon::create($notification->created_at);
            if($date_gmt->isSameDay(Carbon::today())){
                array_push($notifications, $notification);
            }
        }

        return response()->json([
            'notifications' => $notifications
        ]);
    }

    /**
     * Get all unread notifications
     */
    public function getOrderUnreadNotifications()
    {
        $user = User::find(Auth::user()->id);
        $notifications = [];

        foreach ($user->unreadNotifications as $notification) {
            array_push($notifications, $notification);
        }

        return response()->json([
            'notifications' => $notifications
        ]);
    }

    /**
     * Make notification as read
     */
    public function markNotifAsRead($id_notif)
    {
        $notification = Auth::user()->unreadNotifications->where('id', $id_notif)->first();
        if ($notification) {
            $notification->markAsRead();
            return response()->json([
                'code' => 200,
                'message' => 'Notification lue !!!'
            ]);
        }
    }

    /**
     * Get customer unread notification
     */
    public function getCustomerUnreadNotif($id_customer)
    {
        $user = User::find($id_customer);
        $notifications = [];

        foreach ($user->unreadNotifications->where('type', 'App\Notifications\OrderUpdateNotification') as $notification) {
          array_push($notifications, $notification);
        }

        return response()->json([
            'notifications' => $notifications
        ]);
    }
    /**
     * Get customer read notification
     */
    public function getAllNotifForCustomer($id_customer)
    {
        $user = User::find($id_customer);
        $notifications = [];

        foreach ($user->notifications->where('type', 'App\Notifications\OrderUpdateNotification') as $notification) {
          array_push($notifications, $notification);
        }

        return response()->json([
            'notifications' => $notifications
        ]);
    }
}
