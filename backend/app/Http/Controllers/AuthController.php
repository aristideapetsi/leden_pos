<?php

namespace App\Http\Controllers;

use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'signup']]);
    }
    public function signup(Request $request)
    {
        $v = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'phone_number' => 'required',
            'password' => 'min:6',
            'verification_password' => 'required_with:password|same:password'
        ]);
        if ($v->fails())
        {
            return [
                'success' => false,
                'message' => $v->errors(),
                'code' => 204
            ];
        }

        $user = new User();
        $user->username = $request->username;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone_number = $request->phone_number;
        // $user->roles()->attach($request->role);
        $user->role = 4;

        $user->save();

        return [
            'success' => true,
            'message' => 'Inscription effectuée avec succès !!!',
            'code' => 200
        ];
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Connexion échouée'], 401);
        }
        return $this->createNewToken($token);
        // return response()->json(['access_token' => $token]);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => Auth::user(),
        ]);
    }

    public function sendPasswordResetLink(Request $request)
    {
        $email = $request->email;
        if(!$this->validateEmail($email)){
            return response()->json([
                'status' => 'failed',
                'code' => 204,
                'message' => 'Cette adresse email n\'existe pas dans notre base!!!'
            ]);
        }
        Mail::to($email)->send(new ResetPasswordMail);
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Email de réinitialisation de mot de passe envoyé avec succès!!!'
        ]);
    }

    public function validateEmail($email)
    {
       return !User::where('email', $email)->first();
    }
}
