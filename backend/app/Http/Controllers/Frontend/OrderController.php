<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\OrderItem;
use App\Models\OrderStat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    public function getOrderByCustomer()
    {
        $orders = [];
        if(Auth::user() ){
            $orders = OrderStat::where('customer_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'orders' => $orders,
                'message' => 'commandes recupérées avec succès'
            ]);
        }else{
            return response()->json([
                'code' => 401,
                'status' => 'Non autorisé',
                'orders' => $orders,
                'message' => 'Veuillez vous connecter avec votre compte!!!'
            ]);
        }
    }

    /**
     * Get order Detail
    */
    public function getorderDetail($orderId)
    {
        $order = OrderStat::where('id', $orderId)->with('orderProducts.product')->first();
        return response()->json([
            'code' => 200,
            'status' =>'success',
            'message' => 'order Detail retrieve successfully !!!',
            'order' => $order
        ]);
    }
}
