<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function addProductToCart($product_id, $product_qty)
    {
        // dd($product_id);
        if(Auth::check()){
            $prod_check = Product::where('id', $product_id)->first();
            // dd($prod_check);
            if($prod_check){
                if($prod_check->stock_qty <= 0){
                    return response()->json([
                        'message' => 'Stock de ' + $prod_check->product_name + ' est épuisé !!!',
                        'code' => 200
                    ]);
                }
                if(Cart::where('product_id', $product_id)->where('user_id', Auth::id())->exists()){
                    $cart = Cart::where('product_id', $product_id)->where('user_id', Auth::id())->first();

                        $cart->prod_qty = $product_qty;
                        $cart->save();
                        return response()->json([
                            'message' => 'Le nombre de '. $prod_check->product_name .' a été mis à jour dans le panier à '. $product_qty. ' !!!',
                            'code' => 200
                        ]);

                }else{
                    $cartItem = new Cart();
                    $cartItem->product_id = $product_id;
                    $cartItem->prod_qty = $product_qty;
                    $cartItem->user_id = Auth::user()->id;
                    $cartItem->save();

                    $cart = Cart::where('user_id', Auth::user()->id)->get();
                    return response()->json([
                        'message' =>  $product_qty .' '. $prod_check->product_name . ' ajouté au panier avec succès!!!',
                        'code' => 200,
                        'cart' => $cart
                    ]);
                }

            }
        }else{
            return response()->json([
                'message' => 'Veuillez-vous connecter pour continuer!!!',
                'code' => 401
            ]);
        }

    }
/**
 * VieW Cart
 */
    public function getCartContent()
    {
        $subTotal = 0;
        // $priceArray = [];
        if(Auth::user()){
            // $cart = DB::table('carts')
            //             ->leftJoin('products', 'products.id', '=', 'carts.product_id')
            //             ->select('carts.id as id','carts.prod_qty', 'products.price', 'products.feature_image', 'products.product_name')
            //             ->where('carts.user_id', '=', Auth::user()->id)
            //             ->get();
            $cart = Cart::where('user_id', Auth::user()->id)->with('products')->get();
            foreach($cart as $c){
                $subTotal += ($c->products->price * $c->prod_qty);
            }

            // foreach($priceArray as $price){
            //     $subTotal += $price;
            // }
        }else{
            $cart = [];
        }
        return response()->json([
            'message' => 'Cart information get successfully!!!',
            'cart' => $cart,
            'subTotal' => $subTotal
        ]);
    }
    /**
     * Remove item to cart
     */
    public function removeItemToCart(Request $request)
    {
        // dd($request->all());
       if(Auth::user()){
            $prod_id = $request->prod_id;
            if(Cart::where('product_id',  $prod_id)->where('user_id', Auth::user()->id)->exists()){
                $cartItem = Cart::where('product_id', $prod_id)->where('user_id', Auth::user()->id)->first();
                $cartItem->delete();
                $cart = Cart::where('user_id', Auth::user()->id)->get();
                return response()->json([
                    'status' => 200,
                    'message' => 'Product removed successfuly in cart!!!',
                    'cart' => $cart
                ]);
            }
       }else{
            return response()->json([
                'message' => 'Veuillez-vous connecter pour continuer!!!',
                'status' => 401
            ]);
       }
    }
    /**
     * Update Chart
     */
    public function updateChart(Request $request)
    {
        $prod_id = $request->prod_id;
        $product_qty = $request->prod_qty;

        if(Auth::check()){
            if(Cart::where('product_id', $prod_id)->where('user_id', Auth::user()->id)->exists()){
                $cart = Cart::where('product_id', $prod_id)->where('user_id', Auth::user()->id)->first();
                $cart->prod_qty = $product_qty;
                $cart->update();
                return response()->json([
                    'status' => 200,
                    'message' => 'La quantité du produit a été mise à jour!!!'
                ]);
            }

        }else{
            return response()->json([
                'status' => 401,
                'message' => 'Veuillez vous connecter!!!'
            ]);
        }
    }


}
