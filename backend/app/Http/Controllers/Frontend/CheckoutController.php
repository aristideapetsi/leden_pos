<?php

namespace App\Http\Controllers\Frontend;

use App\Events\NewTrade;
use App\Events\OrderUpdate;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\OrderItem;
use App\Models\OrderProduct;
use App\Models\OrderStat;
use App\Models\Product;
use App\Models\User;
use App\Notifications\OrderNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class CheckoutController extends Controller
{
    public function placeOrder(Request $request)
    {
        $order = new OrderStat();
        // $order->prod_qty = $request->prod_qty;
        $order->total_price = $request->total_price;
        $order->payment_channel = $request->payment_channel;
        $order->customer_id = Auth::user()->id;
        $order->shipping_total = $request->shipping_total;
        $order->tracking_no = $request->tracking_no;
        $order->split_payment_infos = $request->contacts;
        $order->notes = $request->notes;

        $cartitems = Cart::where('user_id', Auth::user()->id)->get();
        foreach ($cartitems as $item) {
            //create link between order and product
            $product = Product::find($item->product_id);
            if($product->stock_qty < $item->prod_qty){
                return response()->json([
                    'code' => 204,
                    'message' => 'Le stock de '. Product::find($item->product_id)->product_name . ' est épuisé!!! Mettez à jour votre panier'
                ]);
            }
            if($request->payment_channel == "1"){
                $order->status = "on-hold";
            }else{
                $order->status = "completed";
                event(new OrderUpdate('Commande N* '  . $order->id . ' en cours de prépa'));
            }
        $order->save();

        //

            OrderProduct ::create([
                'order_id' => $order->id,
                'product_id' => $item->product_id,
                'product_qty' => $item->prod_qty,
                'customer_id' => Auth::user()->id,
                'selling_price' => $request->selling_price
            ]);

            //Decrement product quantity
            // $product = Product::find($item->product_id);
            // $product->stock_qty -= $item->prod_qty;
            // $product->total_sales += $item->prod_qty;
            // if($product->stock_qty == 0){
            //     $product->stock_status = "out_of_stock";
            // }
            // $product->save();
        }

        if(Auth::user()->address1 == null){
            $user = User::where('id', Auth::user()->id)->first();
            $user->lastname = $request->firstname;
            $user->firstname = $request->lastname;
            $user->phone_number = $request->phone_number;
            $user->city = $request->city;
            $user->state= $request->state;
            $user->address2 = $request->address2;
            $user->update();
        }

        $cartitems = Cart::where('user_id', Auth::user()->id)->get();
        Cart::destroy($cartitems);
        $userSchema = User::find(Auth::user()->id);
        $orderData = [
            'order_id' => $order->id,
            'message' => 'Nouvelle commande recu N* ' . $order->id
        ];

        Notification::send($userSchema, new OrderNotification($orderData));
        event(new NewTrade('Nouvelle commande: N* ' . $order->id));

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Commande effectuée avec succès!!!'
        ]);
    }
}
