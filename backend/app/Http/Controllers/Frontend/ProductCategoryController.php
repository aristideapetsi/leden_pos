<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function getAllCategories()
    {
        $categories = CategoryProduct::all();

        return response()->json([
            'categories' => $categories,
            'status' => 200
        ]);
    }

    public function getCategoryInfo($id)
    {
        $category = CategoryProduct::find($id);
        return response()->json([
            'category' => $category,
            'status' => 'success',
            'code' => 200
        ]);
    }

}
