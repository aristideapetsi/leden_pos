<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Like;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    /**
     * Get All product | no filter
     */
    public function getAllProducts()
    {
       $products = Product::with('categories')->orderBy('created_at', 'desc')->get();

       $prodMinPrice = Product::min('price');
       $prodMaxPrice = Product::max('price');

       return response()->json([
           'products' => $products,
            'prodMaxPrice' => $prodMaxPrice,
            'prodMinPrice' => $prodMinPrice,
           'status' => 'Product list retrieve successfully!!!'
       ]);
    }

    /**
     * Get top 5 popular products/based on product total purchase
     */
    public function getMostPopularProducts()
    {
        $products = Product::orderBy('total_sales', 'DESC')->limit(5)->get();
        return response()->json([
            'products' => $products,
            'status' => 'Product list retrieve successfully!!!'
        ]);
    }
    /**
     * Get All popular products/ based on product total purchase
     */
    public function getPopularProducts()
    {
        $products = Product::orderBy('total_sales', 'DESC')->get();
        return response()->json([
            'products' => $products,
            'status' => 'Product list retrieve successfully!!!'
        ]);
    }
    /**
     * Filter product by price: low to high
     */
    public function getProductsFromLowToCost()
    {
        $products = Product::orderBy('price', 'ASC')->get();
        return response()->json([
            'products' => $products,
            'status' => 'Product list retrieve successfully!!!'
        ]);
    }
    /**
     * Filter product by price: High to low
     */
    public function getProductsFromCostToLow()
    {
        $products = Product::orderBy('price', 'DESC')->get();
        return response()->json([
            'products' => $products,
            'status' => 'Product list retrieve successfully!!!'
        ]);
    }

    public function getProductDetail($id)
    {
        $product = Product::find($id);
        return response()->json([
            'product' => $product
        ]);
    }
    /**
     * Get related products to a specific product
     */
    public function getRelatedProduct($idCat)
    {
       $products = Product::where('category_id', '=', $idCat)->orderBy('total_sales', 'DESC')
                ->limit(4)->get();

        return response()->json([
            'status' => 'related products get successfully !!!',
            'products' => $products
        ]);
    }

    /**
     * Filter Product By Category
     */
    public function filterProductByCategory(Request $request)
    {
        // dd($request->categories);
        if($request->categories != null){
            $products = DB::table('products')->whereIn('category_id', $request->categories)->get();
        }else{
            $products = Product::all();
        }
        // dd($products);
        return response()->json([
            'status' => 'products get successfully !!!',
            'products' => $products
        ]);
    }
    /**
     * Filter Product By price range
     */
    public function filterProductByPriceRange(Request $request)
    {
        $filter_min_price = $request->minPrice;
        $filter_max_price = $request->maxPrice;
        $range = [$filter_min_price, $filter_max_price];
        $products = Product::whereBetween('price', $range)->get();

        if($filter_min_price && $filter_max_price){
            if($filter_min_price > 0 && $filter_max_price > 0)
            {
                $products = Product::whereBetween('price', [$filter_min_price, $filter_max_price])->get();
            }
        } else {
            $products = Product::all();
        }

        return response()->json([
            'status' => 'products get successfully !!!',
            'products' => $products
        ]);
    }
    /**
     * Search products
     */
    public function searchProducts(Request $request)
    {
        // dd($request->all());
        if($request->products != null){
            $products = DB::table('products')->whereIn('id', $request->products)->get();
        }else{
            $products = Product::all();
        }
        // dd($products);
        return response()->json([
            'status' => 'products get successfully !!!',
            'products' => $products
        ]);
    }

    /**
     * Add like to Product function
     */
    public function addLikeToProduct($prod_id){
        $product_id = $prod_id;
        $product = Product::find($product_id);
        if(Auth::check()){
            if(Like::where('product_id', $product_id)->where('user_id', Auth::id())->exists()){
                $like = Like::where('product_id', $product_id)->where('user_id', Auth::id())->first();
                if( $like->status == "0"){
                    $like->status = 1;
                }else{
                    $like->status = 0;//product dislike with status 0
                }

                $like->save();

                return response()->json([
                    'message' => $like->status == 0 ? 'Vous n\'aimez plus ce produit!!!' : 'Vous aimé ce produit!!!' ,
                    'code' => 200,
                    'like_status' => $like->status
                ]);
            }else{
                $like = new Like();
                $like->user_id = Auth::user()->id;
                $like->product_id = $product_id;
                $like->status = 1;
                $like->save();

                return response()->json([
                    'message' => 'Vous aimé ce produit!!!',
                    'code' => 200,
                    'like_status' => $like->status
                ]);
            }
        }else{
            return response()->json([
                'message' => 'Veuillez-vous connecter pour continuer!!!',
                'code' => 401
            ]);
        }

    }
    /**
     * get liked products
     */
    public function getProductsLiked()
    {
        if(Auth::user()){
            $likedProds = Like::where('user_id', '=', Auth::user()->id)->where('status', 1)->pluck('product_id');
            return response()->json([
                'message' => 'Veuillez-vous connecter pour continuer!!!',
                'likedProducts' => $likedProds,
                'code' => 200
            ]);
        }else{
            return response()->json([
                'message' => 'Veuillez-vous connecter pour continuer!!!',
                'code' => 401
            ]);
        }
    }
}
