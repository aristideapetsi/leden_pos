<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Add product
     */
    function addProduct(Request $request){
        // dd($request->all());
        $v = Validator::make($request->all(), [
            'product_name' => 'required|max:255',
            'price' => 'required',
            'category_id' => 'required'
        ]);
        if ($v->fails())
        {
            return ['status' => 204, 'message' => $v->errors()];
        }

        $product = new Product();
        $product->product_name = $request->product_name;
        $product->stock_qty = $request->stock_qty;
        if($product->stock_qty == null || $product->stock_qty == 0){
            $product->stock_status = 'out_of_stock';
        }else{
            $product->Stock_status = 'in_stock';
        }
        if ($request->file('feature_image')) {

            $fileName =  "image-".time().'.'.$request->feature_image->getClientOriginalExtension();
            $request->file('feature_image')->move('../../frontend/src/assets_leden/img/products', $fileName);
            $product->feature_image = $fileName;
        }
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->save();

        return response()->json([
            'status' => 200,
            'message' => 'Product added successfully!!!'
        ]);
    }
    /**
     * Remove product
     */
    function removeProduct($id){
        $product = Product::find($id);
        $product->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Product deleted successfully !!!!'
        ]);
    }
    /**
     * Update product info
     */
    function updateProduct($id, Request $request){

    }
     /**
     * Get All products | list products
     */
    public function getProductsList()
    {
        $products = Product::orderBy('stock_qty', 'asc')->with('categories')->get();

        return response()->json([
            'products' => $products
        ]);
    }

    /**
     * Update Product Stock
     */
    public function updateStockProd($prodId, $stock)
    {
        $reponse = '';
        $prod = Product::find($prodId);
        // dd($prod);
        if($stock <= 0){
            $prod->stock_qty = 0;
            $prod->stock_status  = "out_of_stock";
        }else if($stock >= 0){
            $prod->stock_qty = $stock;
            $prod->stock_status  = "in_stock";
        }
        $prod->save();

        $response = 'stock updated';

        return response()->json([
            'response' => $response
        ]);
    }

    /**
     * Make products N/A
    */
    public function emptyProductStock(Request $request)
    {
        // dd($request->all());
        $products = explode(",", $request->prods);
        // dd($products);
        for($i = 0; $i < count($products); $i++)
        {
            $prod = Product::find($products[$i]);
            $prod->stock_qty = 0;
            $prod->stock_status = "out_of_stock";
            $prod->save();
        }
        return response()->json(['message' => 'Product stock updated !!!']);
    }

}
