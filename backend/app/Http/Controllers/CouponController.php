<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{
    /**
     * delete coupon | admin | super admin
     */
    public function deleteCoupon($coupon_id)
    {
        $coupon = Coupon::find($coupon_id);
        $coupon->delete();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Coupon supprimé avec succès !!!'
        ]);
    }
    /**
     * get all coupons | admin | superadmin
     */
    public function getAllCoupons()
    {
        $coupons = Coupon::all();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'coupons' => $coupons
        ]);
    }
    /**
     * Store discount coupon
     */
    public function storeCoupon(Request $request)
    {
        $v = Validator::make($request->all(), [
            'code' => 'required|unique:coupons',
            'type' => 'required',
            'value' => 'required|numeric',
            'cart_value' => 'required|numeric'

        ]);
        if ($v->fails())
        {
            return ['status' => 204, 'message' => $v->errors()];
        }

        $coupon = new Coupon();
        $coupon->code = $request->code;
        $coupon->type = $request->type;
        $coupon->value = $request->value;
        $coupon->cart_value = $request->cart_value;
        $coupon->save();

        return response()->json([
            'code' => 200,
            'status' => 'success',
            'message' => 'Le coupon a été enrégistré avec succès !!!'
        ]);

    }

    /**
     * Update coupon
     */
    public function updateCoupon(Request $request)
    {
        $v = Validator::make($request->all(), [
            'code' => 'required|unique:coupons',
            'type' => 'required',
            'value' => 'required|numeric',
            'cart_value' => 'required|numeric'

        ]);
        if ($v->fails())
        {
            return ['status' => 204, 'message' => $v->errors()];
        }

        $coupon = Coupon::find($request->coupon_id);
        $coupon->code = $request->code;
        $coupon->type = $request->type;
        $coupon->value = $request->value;
        $coupon->cart_value = $request->cart_value;
        $coupon->save();

        return response()->json([
            'code' => 200,
            'status' => 'success',
            'message' => 'Le coupon a été mise à jour avec succès !!!'
        ]);

    }

    /**
     * Apply coupon
     */
    public function applyCoupon(Request $request)
    {
        $coupon = Coupon::where('code', $request->coupon_code)->where('cart_value', '<=', $request->subTotal)->first();
        if(!$coupon){
            return response()->json([
                'status' => 'failed',
                'code' => '204',
                'message' => 'Coupon Invalide !!!'
            ]);
        }

        session()->put('coupon', [
            'code' => $coupon->code,
            'type' => $coupon->type,
            'value' => $coupon->value,
            'cart_value' => $coupon->cart_value
        ]);
    }


}
