<?php

namespace App\Http\Controllers;

use App\Models\CategoryProduct;
use App\Models\OrderStat;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
        /**
     * dashboard view
     */
    public function dashboard()
    {
        $orders = OrderStat::whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])->get();
        $total_incomes = 0;
        $total_sales = 0;
        $total_users = count(User::all());

        foreach($orders as $order){
            $total_incomes += $order->total_price;
            $total_sales += $order->total_sales;
        }
        if(count($orders) > 0){
            $average_order_price = $total_incomes / count($orders);
        }else{
            $average_order_price = "0.0";
        }

        $years = OrderStat::orderBy('created_at', 'desc')
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
        ->distinct()->pluck('created_at');
        // dd($years);
        $year_array = array();
        foreach($years as $year){
            array_push($year_array, (new \DateTime($year))->format('Y'));
        }
        // $unique_years = array_unique($year_array);

        //retrive total order done last week
        $current_week_orders_count = OrderStat::select("*")
            ->whereBetween('created_at',
                    [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]
                )
            ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
            ->get()->count();

        //retrieve total order done current week
        $last_week_orders_count = OrderStat::select('*')
        ->whereBetween('created_at',
            [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->subWeek()->endOfWeek()]
        )
        ->whereIn('status', ['completed', 'delivered', 'ready-kitchen', 'processing' ])
        ->get()->count();

        $increase = $last_week_orders_count - $current_week_orders_count;
        if($last_week_orders_count > 0 ){
            $increase_compare = round($increase / $last_week_orders_count, 2);
        }else{
            $increase_compare = 0;
        }
        // dd($current_week_orders_count);
        return response()->json([
            'total_incomes' => $total_incomes,
            'average_order_price' => $average_order_price,
            'total_users' => $total_users,
            'total_sales' => $total_sales,
            'unique_years' => $year_array,
            'increase_compare' => $increase_compare
        ]);
        // dd($increase);
        // return view('dashboard', compact('total_incomes', 'average_order_price', 'total_users', 'total_sales', 'unique_years', 'increase_compare'));
    }

    public function getOverviewStats()
    {
        $total_users = count(User::all());
        $total_product_category = count(CategoryProduct::all());
        $total_products = count(Product::all());
        $total_orders = count(OrderStat::all());

        return response()->json([
            'total_users' => $total_users,
            'total_product_category' => $total_product_category,
            'total_products' => $total_products,
            'total_orders' => $total_orders,
            'status'=> 'success',
            'code' => 200
        ]);
    }
}
