<?php

namespace App\Http\Controllers\Back_office;

use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryProdController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth:api', ['except' => ['login']]);
    // }
    /**
     * Add product category
     */
    function addCategory(Request $request){
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        // dd($request->all());

        if(Auth::user() && (Auth::user()->role == '0' || Auth::user()->role == '1')){
            $category = new CategoryProduct();
            $category->name = $request->name;
            $category->description = $request->description;
            if ($request->file('feature_image')) {

                $fileName =  "image-".time().'.'.$request->feature_image->getClientOriginalExtension();
                $request->file('feature_image')->move('../../frontend/src/assets_leden/img/categoriesProd', $fileName);
                $category->feature_image = $fileName;
            }
            $category->save();
            return response()->json([
                'message' => 'Catégorie ajoutée avec succès !!!',
                'code' => 200,
                'status' => 'success'
            ]);

        }else{
            return response()->json([
                'message' => 'Veuillez vous connecter pour continuer !!!',
                'code' => 401,
                'status' => 'success'
            ]);
        }
    }

    /**
     * Remove product category
     */
    function removeCategory($id){
        if(Auth::user() && (Auth::user()->role == '0' || Auth::user()->role == '1')){
            $category = CategoryProduct::find($id);
            $category->delete();
            return response()->json([
                'message' => 'categorie supprimée avec succès !!!',
                'code' => 200,
                'status' => "success"
            ]);
        }else{
            return response()->json([
                'message' => 'Veuillez vous connecter pour continuer !!!',
                'code' => 401,
                'status' => 'Unauthorized'
            ]);
        }
    }
    /**
     * List all product category
     */
    function listCategoryProds(){
        $categories = CategoryProduct::all();
        return response()->json([
            'categories' => $categories,
            'status' => 200
        ]);
    }

    /**
     * Update product category
     */
    function updateCategory($id, Request $request){
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if(Auth::user() && (Auth::user()->role == '0' || Auth::user()->role == '1')){
            $category = CategoryProduct::find($id);
            $category->name = $request->name;
            $category->description = $request->description;
            if ($request->file('feature_image')) {

                $fileName =  "image-".time().'.'.$request->feature_image->getClientOriginalExtension();
                $request->file('feature_image')->move('../../frontend/src/assets_leden/img/categoriesProd', $fileName);
                $category->feature_image = $fileName;
            }
            $category->save();

            return response()->json([
                'message' => 'Category updated successfully',
                'code' => 200,
                'status' => 'success'
            ]);
        }else{
            return response()->json([
                'message' => 'Veuillez vous connecter pour continuer !!!',
                'code' => 401,
                'status' => 'Unauthorized'
            ]);
        }
    }
}
