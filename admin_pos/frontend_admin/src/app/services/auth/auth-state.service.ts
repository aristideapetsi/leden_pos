import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthStateService {
  private loggedIn = new BehaviorSubject<boolean>(this.token.isLoggedIn()!);
  authStatus = this.loggedIn.asObservable();

  constructor(public token: TokenService) {}

  changeAuthStatus(value: boolean) {
    this.loggedIn.next(value);
  }

}
