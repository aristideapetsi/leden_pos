import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/User';
import { ConfigEnv } from 'src/app/configs/env';


@Injectable({
  providedIn: 'root'
})
// User interface
// export class User {
//   name!: String;
//   email!: String;
//   password!: String;
//   password_confirmation!: String;
// }
@Injectable({
  providedIn: 'root',
})
export class AuthService {

  env : ConfigEnv = new ConfigEnv();
  apiUrl = this.env.getApiUrl() ;
  constructor(private http: HttpClient,
    // public authService: AuthService,
    // private auth: AuthStateService,
    // public router: Router,
    // public token: TokenService
    ) {}
  // User registration
  register(user: User): Observable<any> {
    return this.http.post(this.apiUrl + 'register', user);
  }
  // Login
  signin(user: User): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'login', user);

  }

  // Access user profile
  profileUser(): Observable<any> {
    return this.http.get('http://127.0.0.1:8000/api/user-profile');
  }

  // Signout
  // signOut() {
  //   this.auth.setAuthState(false);
  //   this.token.removeToken();
  //   this.router.navigate(['login']);
  // }
}
