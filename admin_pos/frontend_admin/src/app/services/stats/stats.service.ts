import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  env = new ConfigEnv();

  private apiUrl = this.env.getApiUrl() + "statistics/";
  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(
    private http: HttpClient
    ) { }

  /**
   *
   * @returns list users
   */
   getAll(): Observable<any>{
    return this.http.get<any>(this.apiUrl + "dashboard")
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error: any){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
      errorMessage = error.error.message;
    else
      errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

  return throwError(errorMessage);
  }

  getOrdersStats():Observable<any>{
    return this.http.get<any>("http://localhost:8000/api/chart/products_by_order")
    .pipe(
      catchError(this.errorHandler)
    )
  }

}
