import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService implements CanActivate{

  constructor(
		private tokenService: TokenService,
		private router: Router) { }

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
		// var isAuthenticated = this.tokenService.isLoggedIn();
		// if (!isAuthenticated) {
		// 	this.router.navigate(['/login']);
		// }
    console.log(this.tokenService.isLoggedIn());
		return this.tokenService.isLoggedIn();
	}
}
