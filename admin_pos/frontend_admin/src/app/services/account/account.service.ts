import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { ConfigEnv } from 'src/app/configs/env';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  env : ConfigEnv = new ConfigEnv();
  apiUrl = this.env.getApiUrl() + "users/";
  constructor(
    private httpClient: HttpClient
  ) { }

  updateAddressCustomer(formData : any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.apiUrl + "updateAddressCustomer", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );;
  }

  updateAccountInfo(formData: any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.apiUrl + "updateAccountInfo", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );;
  }

  resetPassword(formData: any): Observable<any>{
    const headers = new HttpHeaders();

    return this.httpClient.post<any>(this.env.getApiUrl() + "sendPasswordResetLink", formData, {
      headers: headers
    }).pipe(
      catchError(this.errorHandler)
    );
  }
      /**
     *
     * @param error
     * @returns
     */
       errorHandler(error: any){
        let errorMessage = '';
        if(error.error instanceof ErrorEvent)
          errorMessage = error.error.message;
        else
          errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

      return throwError(errorMessage);
      }
}
