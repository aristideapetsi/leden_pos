import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigEnv } from 'src/app/configs/env';
import { AuthStateService } from '../auth/auth-state.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  env : ConfigEnv = new ConfigEnv();

  private iss = {
    login: this.env.getApiUrl() + 'login',
    register: this.env.getApiUrl() + 'register',
  }
  constructor(
  ) { }

  handleData(token: any) {
    this.setToken(token);
  }
  getToken() {
    return localStorage.getItem('auth_token');
  }

  setToken(token: any){
    localStorage.setItem('auth_token', token);
  }
 // Verify the token
 isValidToken(){
  const token = this.getToken();
  // console.log('token: ' + token)
  if (token) {
    const payload = this.payload(token);
    if (payload) {
      return Object.values(this.iss).indexOf(payload.iss) > -1 ? true: false;
    }
  }
    return false;
}
payload(token: any) {
  const jwtPayload = token.split('.')[1];
  return this.decode(jwtPayload);
}

decode(payload: any){
  return JSON.parse(atob(payload));
}
// User state based on valid token
isLoggedIn(): any {
  return this.isValidToken();
}
// Remove token
removeToken() {
  localStorage.removeItem('auth_token');
}
}
