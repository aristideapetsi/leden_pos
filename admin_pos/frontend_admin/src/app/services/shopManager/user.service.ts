import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = "http://localhost:8000/api/users/";

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private http: HttpClient) { }

  /**
   *
   * @returns list users
   */
  getAll(): Observable<any>{
    return this.http.get<any>(this.apiUrl + "getAllUsers")
    .pipe(
      catchError(this.errorHandler)
    )
  }
  /**
   *
   * @param user
   * @returns user
   */
  createUser(user: User): Observable<User>{
    return this.http.post<User>(this.apiUrl + "addUser", JSON.stringify(user), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
/**
 *
 * @param id
 * @returns user
 */
  find(id: any): Observable<User>{
    return this.http.get<User>(this.apiUrl + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   *
   * @param id
   * @param user
   * @returns
   */
  update(id: any, user: User): Observable<User>{
    return this.http.put<User>(this.apiUrl + "updateUser/" + id, JSON.stringify(user), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  /**
   *
   * @param id
   * @returns
   */
  delete(id: any): Observable<User>{
    return this.http.delete<User>(this.apiUrl + "removeUser/" + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error: any){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
      errorMessage = error.error.message;
    else
      errorMessage = `Error code ${error.status}\nMessage: ${error.message}`;

  return throwError(errorMessage);
  }

}
