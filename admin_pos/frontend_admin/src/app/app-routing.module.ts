import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AfterLoginService } from './services/login_back_office/after-login-service';
import { BeforeLoginService } from './services/login_back_office/before-login.service';

import { DashboardComponent } from './views/dashboard/dashboard.component';
import { LoginComponent } from './views/login/login.component';
import { UsersComponent } from './views/users/users.component';

const routes: Routes = [
  {path: 'lpos_admin', component: LoginComponent,
  canActivate: [BeforeLoginService] },
  {path: 'dashboard_admin', component: DashboardComponent,
  canActivate: [AfterLoginService]},
  {path: 'shop_management', component: UsersComponent,
  canActivate: [AfterLoginService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
