import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-leftbar',
  templateUrl: './leftbar.component.html',
  styleUrls: ['./leftbar.component.css']
})
export class LeftbarComponent implements OnInit {
  UserProfile! : User;
  isLoggedIn: boolean = false ;
  isAdmin: boolean = false;
  isSuperAdmin: boolean = false;
  isCashier: boolean= false;
  isPreparator: boolean = false;
  isDeliverman: boolean = false;

  contentLoaded : boolean = false;
  constructor(
    public authService: AuthService,
    private authStatus : AuthStateService
    ) {

  }

  ngOnInit(): void {
    this.contentLoaded = false;
    this.authStatus.authStatus.subscribe(value => this.isLoggedIn = value);
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;

      this.contentLoaded = true;

      if(this.UserProfile.role == 0){
        this.isSuperAdmin = true;
      }else if(this.UserProfile.role == 1){
        this.isAdmin = true;
      }else if(this.UserProfile.role == 2){
        this.isCashier = true;
      }else if(this.UserProfile.role == 3){
        this.isPreparator = true;
      }else if(this.UserProfile.role == 4){
        this.isDeliverman = true;
      }

    })

  }
}
