import { Component,OnDestroy, OnInit, VERSION } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


 // Variables
 form: FormGroup;
 loading: boolean = false;
 errors:any = null;

 constructor(
   fb: FormBuilder,
   private router: Router,
   private authService: AuthService,
   private token: TokenService,
   private authState: AuthStateService,
   private toast: ToastHelper

 ) {
   this.form = fb.group({
     email: [
       '',
       [Validators.required, Validators.email]
     ],
     password: [
       '',
       Validators.required
     ]
   });
 }

 ngOnInit(): void {

 }


 /**
  * Login the user based on the form values
  */
 login(): void {
   this.loading = true;
   this.errors = false;
   this.authService.signin(this.form.value).subscribe(
     (result) => {
       this.responseHandler(result);
       this.loading = false;
     },
     (error) => {
       this.errors = error.error;
       this.loading = false;
     },
     () => {
       this.authState.changeAuthStatus(true);
       this.form.reset();
       this.loading = false;
       this.router.navigate(['/dashboard_admin']);
     });
 }

 /**
  * Getter for the form controls
  */
 get controls() {
   return this.form.controls;
 }

 //handle response
responseHandler(data: any){
  console.log( data)
  let date = new Date().getTime();
  let expirationTime = new Date(data.expires_in).getTime();
 this.token.handleData(data.access_token);
 this.autoLogout(data.expires_in);
}

logout() {
 this.authState.changeAuthStatus(false);
 this.token.removeToken();
 this.router.navigate(['lposAdmin']);
 this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');

}

/**
*
* @param expirationTime
*/
autoLogout(expirationTime: number){
 console.log(expirationTime);
 setTimeout(()=>{
   this.logout();
   Swal.fire({
     title: 'Session Expiré',
     text: "Vous etes déconnecté suite à l'expiration de votre session!",
     icon: 'info',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: 'Me reconnecter!',
     cancelButtonText: 'ANNULER'
   }).then((result) => {
     if (result.isConfirmed) {
       this.router.navigate(['/lposAdmin']);
     }
   })

 }, expirationTime)
}
}
