import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastHelper } from 'src/app/helpers/ToastHelper';
import { User } from 'src/app/models/User';
import { AuthStateService } from 'src/app/services/auth/auth-state.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  UserProfile! : User;
  isLoggedIn: boolean = false;

  contentLoaded : boolean = false;

  constructor(
    public authService: AuthService,
    private auth: AuthStateService,
    public router: Router,
    public token: TokenService,
    private toast: ToastHelper
    ) {
   }

  ngOnInit(): void {
    this.contentLoaded = false;
    this.authService.profileUser().subscribe((data: any) =>{
      this.UserProfile = data;
      this.contentLoaded = true;
    })
    this.auth.authStatus.subscribe((val) => {
      this.isLoggedIn = val;

    });
  }

    // Signout
    signOut() {
      this.auth.changeAuthStatus(false);
      this.token.removeToken();
      this.router.navigate(['login']);
      this.toast.toast_success('Deconnexion', 'Vous etes déconnecté avec succès !!!');

    }

}
