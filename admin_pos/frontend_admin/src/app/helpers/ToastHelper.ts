import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: 'root'
 })

export class ToastHelper{

  constructor(
    private toast : ToastrService,
  ) {
  }

  /**
   * Toast success
   * @param title
   * @param message
   */
  toast_success(title: string, message: string, timeOutState : number = 3000 , closeBtn: boolean = false){
    this.toast.success(title, message, {
      progressBar: true,
      timeOut: timeOutState,
      closeButton: closeBtn
    });
  }

  /**
   * Toast info
   * @param title
   * @param message
   */
  toast_info(title: string, message: string, timeOutState : number = 3000 , closeBtn: boolean = false){
    this.toast.info(title, message,{
      progressBar: true,
      timeOut: timeOutState,
      closeButton: closeBtn
    });
  }

  /**
   * Toast error
   * @param title
   * @param message
   */
  toast_error(title: string, message: string, timeOutState : number = 3000 , closeBtn: boolean = false){
    this.toast.error(title, message,{
      progressBar: true,
      timeOut: timeOutState,
      closeButton: closeBtn
    });
  }

  /**
   * Toast warning
   * @param title
   * @param message
   */
  toast_warning(title: string, message: string, timeOutState : number = 3000 , closeBtn: boolean = false){
    this.toast.warning(title, message,{
      progressBar: true,
      timeOut: timeOutState,
      closeButton: closeBtn,
    });
  }
}
