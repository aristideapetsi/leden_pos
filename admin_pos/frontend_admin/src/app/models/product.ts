import { Category } from "./category";

export interface Product
{
  id: number;
  product_name: string;
  price: number;
  stock_qty: number;
  stock_status: string;
  total_sales: number;
  category_id: number;
  categories: Category;
  feature_image: string,
  description: string,
  created_at : string
}
