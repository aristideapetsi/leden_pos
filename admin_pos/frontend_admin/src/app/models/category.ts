export interface Category
{
  id: number;
  name: string;
  description: string;
  feature_image: string;
  created_at: string;
  // updated_at: string;
}
