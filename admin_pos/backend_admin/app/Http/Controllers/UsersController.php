<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    /**
     * User Management index page
     */
    public function index()
    {
        return view('users.index');
    }

        /**
     * Add User to database
     */
    public function addUser(Request $request)
    {

        // do validation
        $v = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'role' => 'required',
            'password' => 'min:6',
            'verification_password' => 'required_with:password|same:password|min:6'
        ]);
        // dd($request->all());
        if ($v->fails())
        {
            return [
                'success' => false,
                'message' => $v->errors(),
                'code' => 204
            ];
        }

        $user = new User();
        $user->username = $request->username;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        // $user->roles()->attach($request->role);
        $user->role = $request->role;

        $user->save();

        return [
            'success' => true,
            'message' => 'Utilisateur crée avec succès',
            'code' => 200
        ];
    }

    /**
     * Show users list
     */
    public function listUsers(Request $request)
    {
        $users = User::all();
        if ($request->ajax()) {
            return datatables()->of($users)
                ->addColumn('created_at', function($row){
                    return Carbon::create($row->created_at)->isoFormat('DD/MM/YYYY');
                })
                ->addColumn('role', function($row){
                    $html = "";
                    if($row->role == "1"){
                        $html = "Admin";
                    }else if($row->role == "2"){
                        $html = 'Caissier';
                    }else if($row->role == "3"){
                        $html = 'Préparateur';
                    }
                    return $html;
                })
                ->addColumn('action', function ($row) {
                    $html = '<a href="#" class="btn btn-xs btn-secondary btn-edit"><i class="fa fa-edit" ></i></a> ';
                    $html .= '<button data-rowid="' . $row->id . '" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-trash"></i></button>';
                    return $html;
                })->toJson();

        }
        // return view('users.index');
        return response()->json([
            'users' => $users
        ]);
    }


    /**
     * Update user data
     */
    public function updateUser($id, Request $request)
    {
        $user = User::find($id);

        // do validation
        if($user->email == $request->email){
            $v = Validator::make($request->all(), [
                'username' => 'required|max:255',
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'email' => 'required|email|max:255',
                'role' => 'required',
            ]);
        }else{
            $v = Validator::make($request->all(), [
                'username' => 'required|max:255',
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'email' => 'required|email|unique:users|max:255',
                'role' => 'required',
            ]);
        }
        if ($v->fails())
        {
            return ['success' => false, 'message' => $v->errors()];
        }

        $user =  User::find($id);
        $user->username = $request->username;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        // $user->role()->save($request->role);
        $user->role = $request->role;
        if($user->password != ""){
            $user->password = bcrypt($request->password);
        }

        $user->update();
        return ['success' => true, 'message' => 'Les informations de l\'utilisateur sont à jour ...'];
    }

    public function removeUser($id)
    {
        User::find($id)->delete();
        return ['success' => true, 'message' => 'Deleted Successfully'];
    }

    public function enableUser()
    {

    }

    public function disableUser()
    {

    }

    public function updateAddressCustomer(Request $request)
    {
       $city = $request->city;
       $address1 = $request->address1;
       $address2 = $request->address2;
       $phone_number = $request->phone_number;

       if(Auth::user()){
        $user = User::find(Auth::user()->id);
        $user->city = $city;
        $user->address1 = $address1;
        $user->address2 = $address2;
        $user->phone_number = $phone_number;
        $user->save();
        return response()->json([
            'code' => 200,
            'status' => 'success',
            'message' => 'Les informations de l\'utilisateur ont été mise à jour'
        ]);
       }else{
        return response()->json([
            'code' => 401,
            'status' => 'failed',
            'message' => 'Veuillez vous connectez pour poursuivre!!!!'
        ]);
       }


    }

    public function updateAccountInfo(Request $request)
    {
       if(Auth::user()){
            $user =  User::find(Auth::user()->id);
            $user->username = $request->username;
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            if($request->password != ""){
                $user->password = bcrypt($request->password);
            }

            $user->save();
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' =>'Les informations de l\'utilisateur ont été mise à jour'
            ]);
       }else{
            return response()->json([
                'code' => 401,
                'status' => 'failed',
                'message' =>'Veuillez vous connectez pour poursuivre!!!!'
            ]);
       }

    }
}
