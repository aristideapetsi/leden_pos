<?php

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    dd(User::all());
    return 'This is your multi-tenant application. The id of the current tenant is ' . tenant('id');
});

Route::group([
    'middleware' => 'api'

], function ($router) {
    Route::post('/login', [App\Http\Controllers\AuthController::class, 'login']);
    Route::post('/register', [App\Http\Controllers\AuthController::class, 'register']);
    Route::post('/logout', [App\Http\Controllers\AuthController::class, 'logout']);
    Route::get('/refresh', [App\Http\Controllers\AuthController::class, 'refresh'])->name('refresh');
    Route::get('/user-profile', [App\Http\Controllers\AuthController::class, 'userProfile'])->name('userProfile');
    Route::post('/sendPasswordResetLink', [AuthController::class, 'sendPasswordResetLink']);
});
